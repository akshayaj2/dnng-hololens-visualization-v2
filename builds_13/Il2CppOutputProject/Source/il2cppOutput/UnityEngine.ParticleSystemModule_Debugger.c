﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = 
{
	{ 22507, 0,  0 } /*tableIndex: 0 */,
};
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[1] = 
{
	"particle",
};
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[64] = 
{
	{ 0, 1 } /* 0x06000001 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32) */,
	{ 0, 0 } /* 0x06000002 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/Particle) */,
	{ 0, 0 } /* 0x06000003 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32) */,
	{ 0, 0 } /* 0x06000004 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32) */,
	{ 0, 0 } /* 0x06000005 System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32) */,
	{ 0, 0 } /* 0x06000006 System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32) */,
	{ 0, 0 } /* 0x06000007 System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[]) */,
	{ 0, 0 } /* 0x06000008 System.Void UnityEngine.ParticleSystem::Emit(System.Int32) */,
	{ 0, 0 } /* 0x06000009 System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32) */,
	{ 0, 0 } /* 0x0600000A System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/EmitParams,System.Int32) */,
	{ 0, 0 } /* 0x0600000B System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem/Particle&) */,
	{ 0, 0 } /* 0x0600000C UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main() */,
	{ 0, 0 } /* 0x0600000D UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission() */,
	{ 0, 0 } /* 0x0600000E UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape() */,
	{ 0, 0 } /* 0x0600000F UnityEngine.ParticleSystem/NoiseModule UnityEngine.ParticleSystem::get_noise() */,
	{ 0, 0 } /* 0x06000010 System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem/EmitParams&,System.Int32) */,
	{ 0, 0 } /* 0x06000011 System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem) */,
	{ 0, 0 } /* 0x06000012 System.Void UnityEngine.ParticleSystem/MainModule::set_loop(System.Boolean) */,
	{ 0, 0 } /* 0x06000013 System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace) */,
	{ 0, 0 } /* 0x06000014 System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake(System.Boolean) */,
	{ 0, 0 } /* 0x06000015 System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles(System.Int32) */,
	{ 0, 0 } /* 0x06000016 System.Void UnityEngine.ParticleSystem/MainModule::set_loop_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean) */,
	{ 0, 0 } /* 0x06000017 System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemSimulationSpace) */,
	{ 0, 0 } /* 0x06000018 System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean) */,
	{ 0, 0 } /* 0x06000019 System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&,System.Int32) */,
	{ 0, 0 } /* 0x0600001A System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem) */,
	{ 0, 0 } /* 0x0600001B System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean) */,
	{ 0, 0 } /* 0x0600001C System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0 } /* 0x0600001D System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverDistance(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0 } /* 0x0600001E System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&,System.Boolean) */,
	{ 0, 0 } /* 0x0600001F System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime_Injected(UnityEngine.ParticleSystem/EmissionModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0 } /* 0x06000020 System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverDistance_Injected(UnityEngine.ParticleSystem/EmissionModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0 } /* 0x06000021 System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem) */,
	{ 0, 0 } /* 0x06000022 System.Void UnityEngine.ParticleSystem/ShapeModule::set_enabled(System.Boolean) */,
	{ 0, 0 } /* 0x06000023 System.Void UnityEngine.ParticleSystem/ShapeModule::set_enabled_Injected(UnityEngine.ParticleSystem/ShapeModule&,System.Boolean) */,
	{ 0, 0 } /* 0x06000024 System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single) */,
	{ 0, 0 } /* 0x06000025 System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x06000026 System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x06000027 System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single) */,
	{ 0, 0 } /* 0x06000028 System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single) */,
	{ 0, 0 } /* 0x06000029 System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32) */,
	{ 0, 0 } /* 0x0600002A System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32) */,
	{ 0, 0 } /* 0x0600002B System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single) */,
	{ 0, 0 } /* 0x0600002C System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x0600002D System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity(System.Single) */,
	{ 0, 0 } /* 0x0600002E System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x0600002F System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single) */,
	{ 0, 0 } /* 0x06000030 UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single) */,
	{ 0, 0 } /* 0x06000031 System.Void UnityEngine.ParticleSystem/NoiseModule::.ctor(UnityEngine.ParticleSystem) */,
	{ 0, 0 } /* 0x06000032 System.Void UnityEngine.ParticleSystem/NoiseModule::set_enabled(System.Boolean) */,
	{ 0, 0 } /* 0x06000033 System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthX(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0 } /* 0x06000034 System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthY(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0 } /* 0x06000035 System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthZ(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0 } /* 0x06000036 System.Void UnityEngine.ParticleSystem/NoiseModule::set_frequency(System.Single) */,
	{ 0, 0 } /* 0x06000037 System.Void UnityEngine.ParticleSystem/NoiseModule::set_octaveCount(System.Int32) */,
	{ 0, 0 } /* 0x06000038 System.Void UnityEngine.ParticleSystem/NoiseModule::set_scrollSpeed(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0 } /* 0x06000039 System.Void UnityEngine.ParticleSystem/NoiseModule::set_enabled_Injected(UnityEngine.ParticleSystem/NoiseModule&,System.Boolean) */,
	{ 0, 0 } /* 0x0600003A System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthX_Injected(UnityEngine.ParticleSystem/NoiseModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0 } /* 0x0600003B System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthY_Injected(UnityEngine.ParticleSystem/NoiseModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0 } /* 0x0600003C System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthZ_Injected(UnityEngine.ParticleSystem/NoiseModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0 } /* 0x0600003D System.Void UnityEngine.ParticleSystem/NoiseModule::set_frequency_Injected(UnityEngine.ParticleSystem/NoiseModule&,System.Single) */,
	{ 0, 0 } /* 0x0600003E System.Void UnityEngine.ParticleSystem/NoiseModule::set_octaveCount_Injected(UnityEngine.ParticleSystem/NoiseModule&,System.Int32) */,
	{ 0, 0 } /* 0x0600003F System.Void UnityEngine.ParticleSystem/NoiseModule::set_scrollSpeed_Injected(UnityEngine.ParticleSystem/NoiseModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0 } /* 0x06000040 System.Int32 UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[]) */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
IL2CPP_EXTERN_C Il2CppSequencePoint g_sequencePointsUnityEngine_ParticleSystemModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_ParticleSystemModule[179] = 
{
	{ 26518, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 0 } /* seqPointIndex: 0 */,
	{ 26518, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 1 } /* seqPointIndex: 1 */,
	{ 26518, 1, 120, 120, 9, 10, 0, kSequencePointKind_Normal, 0, 2 } /* seqPointIndex: 2 */,
	{ 26518, 1, 121, 121, 13, 78, 1, kSequencePointKind_Normal, 0, 3 } /* seqPointIndex: 3 */,
	{ 26518, 1, 122, 122, 13, 42, 9, kSequencePointKind_Normal, 0, 4 } /* seqPointIndex: 4 */,
	{ 26518, 1, 123, 123, 13, 42, 18, kSequencePointKind_Normal, 0, 5 } /* seqPointIndex: 5 */,
	{ 26518, 1, 124, 124, 13, 42, 27, kSequencePointKind_Normal, 0, 6 } /* seqPointIndex: 6 */,
	{ 26518, 1, 125, 125, 13, 47, 37, kSequencePointKind_Normal, 0, 7 } /* seqPointIndex: 7 */,
	{ 26518, 1, 126, 126, 13, 39, 47, kSequencePointKind_Normal, 0, 8 } /* seqPointIndex: 8 */,
	{ 26518, 1, 127, 127, 13, 48, 56, kSequencePointKind_Normal, 0, 9 } /* seqPointIndex: 9 */,
	{ 26518, 1, 128, 128, 13, 55, 69, kSequencePointKind_Normal, 0, 10 } /* seqPointIndex: 10 */,
	{ 26518, 1, 129, 129, 13, 41, 82, kSequencePointKind_Normal, 0, 11 } /* seqPointIndex: 11 */,
	{ 26518, 1, 130, 130, 13, 37, 92, kSequencePointKind_Normal, 0, 12 } /* seqPointIndex: 12 */,
	{ 26518, 1, 131, 131, 13, 44, 101, kSequencePointKind_Normal, 0, 13 } /* seqPointIndex: 13 */,
	{ 26518, 1, 132, 132, 9, 10, 110, kSequencePointKind_Normal, 0, 14 } /* seqPointIndex: 14 */,
	{ 26518, 1, 122, 122, 13, 42, 12, kSequencePointKind_StepOut, 0, 15 } /* seqPointIndex: 15 */,
	{ 26518, 1, 123, 123, 13, 42, 21, kSequencePointKind_StepOut, 0, 16 } /* seqPointIndex: 16 */,
	{ 26518, 1, 124, 124, 13, 42, 31, kSequencePointKind_StepOut, 0, 17 } /* seqPointIndex: 17 */,
	{ 26518, 1, 125, 125, 13, 47, 41, kSequencePointKind_StepOut, 0, 18 } /* seqPointIndex: 18 */,
	{ 26518, 1, 126, 126, 13, 39, 50, kSequencePointKind_StepOut, 0, 19 } /* seqPointIndex: 19 */,
	{ 26518, 1, 127, 127, 13, 48, 58, kSequencePointKind_StepOut, 0, 20 } /* seqPointIndex: 20 */,
	{ 26518, 1, 127, 127, 13, 48, 63, kSequencePointKind_StepOut, 0, 21 } /* seqPointIndex: 21 */,
	{ 26518, 1, 128, 128, 13, 55, 71, kSequencePointKind_StepOut, 0, 22 } /* seqPointIndex: 22 */,
	{ 26518, 1, 128, 128, 13, 55, 76, kSequencePointKind_StepOut, 0, 23 } /* seqPointIndex: 23 */,
	{ 26518, 1, 129, 129, 13, 41, 86, kSequencePointKind_StepOut, 0, 24 } /* seqPointIndex: 24 */,
	{ 26518, 1, 130, 130, 13, 37, 95, kSequencePointKind_StepOut, 0, 25 } /* seqPointIndex: 25 */,
	{ 26518, 1, 131, 131, 13, 44, 104, kSequencePointKind_StepOut, 0, 26 } /* seqPointIndex: 26 */,
	{ 26519, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 27 } /* seqPointIndex: 27 */,
	{ 26519, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 28 } /* seqPointIndex: 28 */,
	{ 26519, 1, 136, 136, 9, 10, 0, kSequencePointKind_Normal, 0, 29 } /* seqPointIndex: 29 */,
	{ 26519, 1, 137, 137, 13, 44, 1, kSequencePointKind_Normal, 0, 30 } /* seqPointIndex: 30 */,
	{ 26519, 1, 138, 138, 9, 10, 10, kSequencePointKind_Normal, 0, 31 } /* seqPointIndex: 31 */,
	{ 26519, 1, 137, 137, 13, 44, 4, kSequencePointKind_StepOut, 0, 32 } /* seqPointIndex: 32 */,
	{ 26521, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 33 } /* seqPointIndex: 33 */,
	{ 26521, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 34 } /* seqPointIndex: 34 */,
	{ 26521, 2, 95, 95, 72, 73, 0, kSequencePointKind_Normal, 0, 35 } /* seqPointIndex: 35 */,
	{ 26521, 2, 95, 95, 74, 107, 1, kSequencePointKind_Normal, 0, 36 } /* seqPointIndex: 36 */,
	{ 26521, 2, 95, 95, 108, 109, 11, kSequencePointKind_Normal, 0, 37 } /* seqPointIndex: 37 */,
	{ 26521, 2, 95, 95, 74, 107, 5, kSequencePointKind_StepOut, 0, 38 } /* seqPointIndex: 38 */,
	{ 26523, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 39 } /* seqPointIndex: 39 */,
	{ 26523, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 40 } /* seqPointIndex: 40 */,
	{ 26523, 2, 106, 106, 71, 72, 0, kSequencePointKind_Normal, 0, 41 } /* seqPointIndex: 41 */,
	{ 26523, 2, 106, 106, 73, 113, 1, kSequencePointKind_Normal, 0, 42 } /* seqPointIndex: 42 */,
	{ 26523, 2, 106, 106, 114, 115, 13, kSequencePointKind_Normal, 0, 43 } /* seqPointIndex: 43 */,
	{ 26523, 2, 106, 106, 73, 113, 5, kSequencePointKind_StepOut, 0, 44 } /* seqPointIndex: 44 */,
	{ 26524, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 45 } /* seqPointIndex: 45 */,
	{ 26524, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 46 } /* seqPointIndex: 46 */,
	{ 26524, 2, 107, 107, 61, 62, 0, kSequencePointKind_Normal, 0, 47 } /* seqPointIndex: 47 */,
	{ 26524, 2, 107, 107, 63, 98, 1, kSequencePointKind_Normal, 0, 48 } /* seqPointIndex: 48 */,
	{ 26524, 2, 107, 107, 99, 100, 12, kSequencePointKind_Normal, 0, 49 } /* seqPointIndex: 49 */,
	{ 26524, 2, 107, 107, 63, 98, 4, kSequencePointKind_StepOut, 0, 50 } /* seqPointIndex: 50 */,
	{ 26525, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 51 } /* seqPointIndex: 51 */,
	{ 26525, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 52 } /* seqPointIndex: 52 */,
	{ 26525, 2, 176, 176, 37, 38, 0, kSequencePointKind_Normal, 0, 53 } /* seqPointIndex: 53 */,
	{ 26525, 2, 176, 176, 39, 60, 1, kSequencePointKind_Normal, 0, 54 } /* seqPointIndex: 54 */,
	{ 26525, 2, 176, 176, 61, 62, 9, kSequencePointKind_Normal, 0, 55 } /* seqPointIndex: 55 */,
	{ 26525, 2, 176, 176, 39, 60, 3, kSequencePointKind_StepOut, 0, 56 } /* seqPointIndex: 56 */,
	{ 26529, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 57 } /* seqPointIndex: 57 */,
	{ 26529, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 58 } /* seqPointIndex: 58 */,
	{ 26529, 3, 634, 634, 38, 39, 0, kSequencePointKind_Normal, 0, 59 } /* seqPointIndex: 59 */,
	{ 26529, 3, 634, 634, 40, 68, 1, kSequencePointKind_Normal, 0, 60 } /* seqPointIndex: 60 */,
	{ 26529, 3, 634, 634, 69, 70, 10, kSequencePointKind_Normal, 0, 61 } /* seqPointIndex: 61 */,
	{ 26529, 3, 634, 634, 40, 68, 2, kSequencePointKind_StepOut, 0, 62 } /* seqPointIndex: 62 */,
	{ 26530, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 63 } /* seqPointIndex: 63 */,
	{ 26530, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 64 } /* seqPointIndex: 64 */,
	{ 26530, 3, 635, 635, 46, 47, 0, kSequencePointKind_Normal, 0, 65 } /* seqPointIndex: 65 */,
	{ 26530, 3, 635, 635, 48, 80, 1, kSequencePointKind_Normal, 0, 66 } /* seqPointIndex: 66 */,
	{ 26530, 3, 635, 635, 81, 82, 10, kSequencePointKind_Normal, 0, 67 } /* seqPointIndex: 67 */,
	{ 26530, 3, 635, 635, 48, 80, 2, kSequencePointKind_StepOut, 0, 68 } /* seqPointIndex: 68 */,
	{ 26531, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 69 } /* seqPointIndex: 69 */,
	{ 26531, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 70 } /* seqPointIndex: 70 */,
	{ 26531, 3, 636, 636, 40, 41, 0, kSequencePointKind_Normal, 0, 71 } /* seqPointIndex: 71 */,
	{ 26531, 3, 636, 636, 42, 71, 1, kSequencePointKind_Normal, 0, 72 } /* seqPointIndex: 72 */,
	{ 26531, 3, 636, 636, 72, 73, 10, kSequencePointKind_Normal, 0, 73 } /* seqPointIndex: 73 */,
	{ 26531, 3, 636, 636, 42, 71, 2, kSequencePointKind_StepOut, 0, 74 } /* seqPointIndex: 74 */,
	{ 26532, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 75 } /* seqPointIndex: 75 */,
	{ 26532, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 76 } /* seqPointIndex: 76 */,
	{ 26532, 3, 649, 649, 40, 41, 0, kSequencePointKind_Normal, 0, 77 } /* seqPointIndex: 77 */,
	{ 26532, 3, 649, 649, 42, 71, 1, kSequencePointKind_Normal, 0, 78 } /* seqPointIndex: 78 */,
	{ 26532, 3, 649, 649, 72, 73, 10, kSequencePointKind_Normal, 0, 79 } /* seqPointIndex: 79 */,
	{ 26532, 3, 649, 649, 42, 71, 2, kSequencePointKind_StepOut, 0, 80 } /* seqPointIndex: 80 */,
	{ 26534, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 81 } /* seqPointIndex: 81 */,
	{ 26534, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 82 } /* seqPointIndex: 82 */,
	{ 26534, 3, 20, 20, 64, 65, 0, kSequencePointKind_Normal, 0, 83 } /* seqPointIndex: 83 */,
	{ 26534, 3, 20, 20, 66, 100, 1, kSequencePointKind_Normal, 0, 84 } /* seqPointIndex: 84 */,
	{ 26534, 3, 20, 20, 101, 102, 8, kSequencePointKind_Normal, 0, 85 } /* seqPointIndex: 85 */,
	{ 26543, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 86 } /* seqPointIndex: 86 */,
	{ 26543, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 87 } /* seqPointIndex: 87 */,
	{ 26543, 3, 74, 74, 68, 69, 0, kSequencePointKind_Normal, 0, 88 } /* seqPointIndex: 88 */,
	{ 26543, 3, 74, 74, 70, 104, 1, kSequencePointKind_Normal, 0, 89 } /* seqPointIndex: 89 */,
	{ 26543, 3, 74, 74, 105, 106, 8, kSequencePointKind_Normal, 0, 90 } /* seqPointIndex: 90 */,
	{ 26550, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 91 } /* seqPointIndex: 91 */,
	{ 26550, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 92 } /* seqPointIndex: 92 */,
	{ 26550, 3, 112, 112, 65, 66, 0, kSequencePointKind_Normal, 0, 93 } /* seqPointIndex: 93 */,
	{ 26550, 3, 112, 112, 67, 101, 1, kSequencePointKind_Normal, 0, 94 } /* seqPointIndex: 94 */,
	{ 26550, 3, 112, 112, 102, 103, 8, kSequencePointKind_Normal, 0, 95 } /* seqPointIndex: 95 */,
	{ 26553, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 96 } /* seqPointIndex: 96 */,
	{ 26553, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 97 } /* seqPointIndex: 97 */,
	{ 26553, 1, 94, 94, 75, 76, 0, kSequencePointKind_Normal, 0, 98 } /* seqPointIndex: 98 */,
	{ 26553, 1, 94, 94, 77, 103, 1, kSequencePointKind_Normal, 0, 99 } /* seqPointIndex: 99 */,
	{ 26553, 1, 94, 94, 104, 105, 9, kSequencePointKind_Normal, 0, 100 } /* seqPointIndex: 100 */,
	{ 26553, 1, 94, 94, 77, 103, 3, kSequencePointKind_StepOut, 0, 101 } /* seqPointIndex: 101 */,
	{ 26554, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 102 } /* seqPointIndex: 102 */,
	{ 26554, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 103 } /* seqPointIndex: 103 */,
	{ 26554, 4, 190, 190, 70, 71, 0, kSequencePointKind_Normal, 0, 104 } /* seqPointIndex: 104 */,
	{ 26554, 4, 190, 190, 72, 91, 1, kSequencePointKind_Normal, 0, 105 } /* seqPointIndex: 105 */,
	{ 26554, 4, 190, 190, 92, 93, 8, kSequencePointKind_Normal, 0, 106 } /* seqPointIndex: 106 */,
	{ 26555, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 107 } /* seqPointIndex: 107 */,
	{ 26555, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 108 } /* seqPointIndex: 108 */,
	{ 26555, 4, 191, 191, 70, 71, 0, kSequencePointKind_Normal, 0, 109 } /* seqPointIndex: 109 */,
	{ 26555, 4, 191, 191, 72, 91, 1, kSequencePointKind_Normal, 0, 110 } /* seqPointIndex: 110 */,
	{ 26555, 4, 191, 191, 92, 93, 8, kSequencePointKind_Normal, 0, 111 } /* seqPointIndex: 111 */,
	{ 26556, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 112 } /* seqPointIndex: 112 */,
	{ 26556, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 113 } /* seqPointIndex: 113 */,
	{ 26556, 4, 194, 194, 77, 78, 0, kSequencePointKind_Normal, 0, 114 } /* seqPointIndex: 114 */,
	{ 26556, 4, 194, 194, 79, 98, 1, kSequencePointKind_Normal, 0, 115 } /* seqPointIndex: 115 */,
	{ 26556, 4, 194, 194, 99, 100, 8, kSequencePointKind_Normal, 0, 116 } /* seqPointIndex: 116 */,
	{ 26557, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 117 } /* seqPointIndex: 117 */,
	{ 26557, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 118 } /* seqPointIndex: 118 */,
	{ 26557, 4, 195, 195, 78, 79, 0, kSequencePointKind_Normal, 0, 119 } /* seqPointIndex: 119 */,
	{ 26557, 4, 195, 195, 80, 104, 1, kSequencePointKind_Normal, 0, 120 } /* seqPointIndex: 120 */,
	{ 26557, 4, 195, 195, 105, 106, 8, kSequencePointKind_Normal, 0, 121 } /* seqPointIndex: 121 */,
	{ 26558, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 122 } /* seqPointIndex: 122 */,
	{ 26558, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 123 } /* seqPointIndex: 123 */,
	{ 26558, 4, 196, 196, 74, 75, 0, kSequencePointKind_Normal, 0, 124 } /* seqPointIndex: 124 */,
	{ 26558, 4, 196, 196, 76, 97, 1, kSequencePointKind_Normal, 0, 125 } /* seqPointIndex: 125 */,
	{ 26558, 4, 196, 196, 98, 99, 8, kSequencePointKind_Normal, 0, 126 } /* seqPointIndex: 126 */,
	{ 26559, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 127 } /* seqPointIndex: 127 */,
	{ 26559, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 128 } /* seqPointIndex: 128 */,
	{ 26559, 4, 197, 197, 73, 74, 0, kSequencePointKind_Normal, 0, 129 } /* seqPointIndex: 129 */,
	{ 26559, 4, 197, 197, 75, 96, 1, kSequencePointKind_Normal, 0, 130 } /* seqPointIndex: 130 */,
	{ 26559, 4, 197, 197, 97, 98, 8, kSequencePointKind_Normal, 0, 131 } /* seqPointIndex: 131 */,
	{ 26560, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 132 } /* seqPointIndex: 132 */,
	{ 26560, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 133 } /* seqPointIndex: 133 */,
	{ 26560, 4, 200, 200, 72, 73, 0, kSequencePointKind_Normal, 0, 134 } /* seqPointIndex: 134 */,
	{ 26560, 4, 200, 200, 74, 121, 1, kSequencePointKind_Normal, 0, 135 } /* seqPointIndex: 135 */,
	{ 26560, 4, 200, 200, 122, 123, 15, kSequencePointKind_Normal, 0, 136 } /* seqPointIndex: 136 */,
	{ 26560, 4, 200, 200, 74, 121, 5, kSequencePointKind_StepOut, 0, 137 } /* seqPointIndex: 137 */,
	{ 26561, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 138 } /* seqPointIndex: 138 */,
	{ 26561, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 139 } /* seqPointIndex: 139 */,
	{ 26561, 4, 204, 204, 88, 89, 0, kSequencePointKind_Normal, 0, 140 } /* seqPointIndex: 140 */,
	{ 26561, 4, 204, 204, 90, 125, 1, kSequencePointKind_Normal, 0, 141 } /* seqPointIndex: 141 */,
	{ 26561, 4, 204, 204, 126, 162, 18, kSequencePointKind_Normal, 0, 142 } /* seqPointIndex: 142 */,
	{ 26561, 4, 204, 204, 163, 164, 32, kSequencePointKind_Normal, 0, 143 } /* seqPointIndex: 143 */,
	{ 26561, 4, 204, 204, 90, 125, 8, kSequencePointKind_StepOut, 0, 144 } /* seqPointIndex: 144 */,
	{ 26562, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 145 } /* seqPointIndex: 145 */,
	{ 26562, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 146 } /* seqPointIndex: 146 */,
	{ 26562, 4, 206, 206, 100, 101, 0, kSequencePointKind_Normal, 0, 147 } /* seqPointIndex: 147 */,
	{ 26562, 4, 206, 206, 102, 169, 1, kSequencePointKind_Normal, 0, 148 } /* seqPointIndex: 148 */,
	{ 26562, 4, 206, 206, 170, 171, 29, kSequencePointKind_Normal, 0, 149 } /* seqPointIndex: 149 */,
	{ 26562, 4, 206, 206, 102, 169, 19, kSequencePointKind_StepOut, 0, 150 } /* seqPointIndex: 150 */,
	{ 26563, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 151 } /* seqPointIndex: 151 */,
	{ 26563, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 152 } /* seqPointIndex: 152 */,
	{ 26563, 4, 207, 207, 102, 103, 0, kSequencePointKind_Normal, 0, 153 } /* seqPointIndex: 153 */,
	{ 26563, 4, 207, 207, 104, 146, 1, kSequencePointKind_Normal, 0, 154 } /* seqPointIndex: 154 */,
	{ 26563, 4, 207, 207, 147, 183, 18, kSequencePointKind_Normal, 0, 155 } /* seqPointIndex: 155 */,
	{ 26563, 4, 207, 207, 184, 185, 32, kSequencePointKind_Normal, 0, 156 } /* seqPointIndex: 156 */,
	{ 26563, 4, 207, 207, 104, 146, 8, kSequencePointKind_StepOut, 0, 157 } /* seqPointIndex: 157 */,
	{ 26564, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 158 } /* seqPointIndex: 158 */,
	{ 26564, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 159 } /* seqPointIndex: 159 */,
	{ 26564, 4, 80, 80, 48, 49, 0, kSequencePointKind_Normal, 0, 160 } /* seqPointIndex: 160 */,
	{ 26564, 4, 80, 80, 50, 92, 1, kSequencePointKind_Normal, 0, 161 } /* seqPointIndex: 161 */,
	{ 26564, 4, 80, 80, 93, 118, 8, kSequencePointKind_Normal, 0, 162 } /* seqPointIndex: 162 */,
	{ 26564, 4, 80, 80, 119, 137, 19, kSequencePointKind_Normal, 0, 163 } /* seqPointIndex: 163 */,
	{ 26564, 4, 80, 80, 138, 156, 26, kSequencePointKind_Normal, 0, 164 } /* seqPointIndex: 164 */,
	{ 26564, 4, 80, 80, 157, 178, 33, kSequencePointKind_Normal, 0, 165 } /* seqPointIndex: 165 */,
	{ 26564, 4, 80, 80, 179, 204, 44, kSequencePointKind_Normal, 0, 166 } /* seqPointIndex: 166 */,
	{ 26564, 4, 80, 80, 205, 206, 51, kSequencePointKind_Normal, 0, 167 } /* seqPointIndex: 167 */,
	{ 26565, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 168 } /* seqPointIndex: 168 */,
	{ 26565, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 169 } /* seqPointIndex: 169 */,
	{ 26565, 4, 113, 113, 13, 14, 0, kSequencePointKind_Normal, 0, 170 } /* seqPointIndex: 170 */,
	{ 26565, 4, 114, 114, 17, 50, 1, kSequencePointKind_Normal, 0, 171 } /* seqPointIndex: 171 */,
	{ 26565, 4, 115, 115, 13, 14, 10, kSequencePointKind_Normal, 0, 172 } /* seqPointIndex: 172 */,
	{ 26565, 4, 114, 114, 17, 50, 2, kSequencePointKind_StepOut, 0, 173 } /* seqPointIndex: 173 */,
	{ 26566, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 174 } /* seqPointIndex: 174 */,
	{ 26566, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 175 } /* seqPointIndex: 175 */,
	{ 26566, 3, 381, 381, 65, 66, 0, kSequencePointKind_Normal, 0, 176 } /* seqPointIndex: 176 */,
	{ 26566, 3, 381, 381, 67, 101, 1, kSequencePointKind_Normal, 0, 177 } /* seqPointIndex: 177 */,
	{ 26566, 3, 381, 381, 102, 103, 8, kSequencePointKind_Normal, 0, 178 } /* seqPointIndex: 178 */,
};
#else
extern Il2CppSequencePoint g_sequencePointsUnityEngine_ParticleSystemModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_ParticleSystemModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[] = {
{ "", { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} }, //0 
{ "C:\\buildslave\\unity\\build\\Modules\\ParticleSystem\\Managed\\ParticleSystem.deprecated.cs", { 254, 108, 111, 128, 100, 7, 222, 4, 156, 244, 245, 164, 175, 217, 189, 197} }, //1 
{ "C:\\buildslave\\unity\\build\\Modules\\ParticleSystem\\ScriptBindings\\ParticleSystem.bindings.cs", { 236, 65, 203, 223, 251, 100, 230, 5, 113, 251, 2, 165, 182, 52, 78, 2} }, //2 
{ "C:\\buildslave\\unity\\build\\Modules\\ParticleSystem\\ScriptBindings\\ParticleSystemModules.bindings.cs", { 124, 10, 231, 224, 179, 116, 145, 254, 251, 117, 255, 106, 204, 191, 69, 14} }, //3 
{ "C:\\buildslave\\unity\\build\\Modules\\ParticleSystem\\Managed\\ParticleSystemStructs.cs", { 220, 239, 182, 144, 220, 43, 200, 140, 248, 250, 235, 227, 210, 7, 223, 37} }, //4 
};
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[10] = 
{
	{ 4098, 1 },
	{ 4098, 2 },
	{ 4098, 3 },
	{ 4091, 3 },
	{ 4092, 3 },
	{ 4093, 3 },
	{ 4094, 1 },
	{ 4094, 4 },
	{ 4095, 4 },
	{ 4097, 3 },
};
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[8] = 
{
	{ 0, 111 },
	{ 0, 15 },
	{ 0, 14 },
	{ 0, 12 },
	{ 0, 12 },
	{ 0, 12 },
	{ 0, 12 },
	{ 0, 12 },
};
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[64] = 
{
	{ 111, 0, 1 } /* System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/Particle) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32) */,
	{ 15, 1, 1 } /* System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32) */,
	{ 14, 2, 1 } /* System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[]) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem::Emit(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/EmitParams,System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem/Particle&) */,
	{ 12, 3, 1 } /* UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main() */,
	{ 12, 4, 1 } /* UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission() */,
	{ 12, 5, 1 } /* UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape() */,
	{ 12, 6, 1 } /* UnityEngine.ParticleSystem/NoiseModule UnityEngine.ParticleSystem::get_noise() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem/EmitParams&,System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::set_loop(System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake(System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::set_loop_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemSimulationSpace) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&,System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverDistance(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&,System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime_Injected(UnityEngine.ParticleSystem/EmissionModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverDistance_Injected(UnityEngine.ParticleSystem/EmissionModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/ShapeModule::set_enabled(System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/ShapeModule::set_enabled_Injected(UnityEngine.ParticleSystem/ShapeModule&,System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity(System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single) */,
	{ 12, 7, 1 } /* UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::.ctor(UnityEngine.ParticleSystem) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_enabled(System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthX(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthY(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthZ(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_frequency(System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_octaveCount(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_scrollSpeed(UnityEngine.ParticleSystem/MinMaxCurve) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_enabled_Injected(UnityEngine.ParticleSystem/NoiseModule&,System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthX_Injected(UnityEngine.ParticleSystem/NoiseModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthY_Injected(UnityEngine.ParticleSystem/NoiseModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_strengthZ_Injected(UnityEngine.ParticleSystem/NoiseModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_frequency_Injected(UnityEngine.ParticleSystem/NoiseModule&,System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_octaveCount_Injected(UnityEngine.ParticleSystem/NoiseModule&,System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.ParticleSystem/NoiseModule::set_scrollSpeed_Injected(UnityEngine.ParticleSystem/NoiseModule&,UnityEngine.ParticleSystem/MinMaxCurve&) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[]) */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
IL2CPP_EXTERN_C const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_ParticleSystemModule;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_ParticleSystemModule = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	179,
	(Il2CppSequencePoint*)g_sequencePointsUnityEngine_ParticleSystemModule,
	0,
	(Il2CppCatchPoint*)g_catchPoints,
	10,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
