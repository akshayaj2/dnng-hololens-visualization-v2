﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[22] = 
{
	{ 0, 0 } /* 0x06000001 System.Void UnityEngine.Yoga.BaselineFunction::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0 } /* 0x06000002 System.Single UnityEngine.Yoga.BaselineFunction::Invoke(UnityEngine.Yoga.YogaNode,System.Single,System.Single) */,
	{ 0, 0 } /* 0x06000003 System.IAsyncResult UnityEngine.Yoga.BaselineFunction::BeginInvoke(UnityEngine.Yoga.YogaNode,System.Single,System.Single,System.AsyncCallback,System.Object) */,
	{ 0, 0 } /* 0x06000004 System.Single UnityEngine.Yoga.BaselineFunction::EndInvoke(System.IAsyncResult) */,
	{ 0, 0 } /* 0x06000005 System.Void UnityEngine.Yoga.MeasureFunction::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0 } /* 0x06000006 UnityEngine.Yoga.YogaSize UnityEngine.Yoga.MeasureFunction::Invoke(UnityEngine.Yoga.YogaNode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.Single,UnityEngine.Yoga.YogaMeasureMode) */,
	{ 0, 0 } /* 0x06000007 System.IAsyncResult UnityEngine.Yoga.MeasureFunction::BeginInvoke(UnityEngine.Yoga.YogaNode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.AsyncCallback,System.Object) */,
	{ 0, 0 } /* 0x06000008 UnityEngine.Yoga.YogaSize UnityEngine.Yoga.MeasureFunction::EndInvoke(System.IAsyncResult) */,
	{ 0, 0 } /* 0x06000009 System.Void UnityEngine.Yoga.Native::YGNodeMeasureInvoke(UnityEngine.Yoga.YogaNode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.IntPtr) */,
	{ 0, 0 } /* 0x0600000A System.Void UnityEngine.Yoga.Native::YGNodeBaselineInvoke(UnityEngine.Yoga.YogaNode,System.Single,System.Single,System.IntPtr) */,
	{ 0, 0 } /* 0x0600000B UnityEngine.Yoga.YogaSize UnityEngine.Yoga.YogaNode::MeasureInternal(UnityEngine.Yoga.YogaNode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.Single,UnityEngine.Yoga.YogaMeasureMode) */,
	{ 0, 0 } /* 0x0600000C System.Single UnityEngine.Yoga.YogaNode::BaselineInternal(UnityEngine.Yoga.YogaNode,System.Single,System.Single) */,
	{ 0, 0 } /* 0x0600000D System.Void UnityEngine.UIElements.UIElementsRuntimeUtilityNative::RepaintOverlayPanels() */,
	{ 0, 0 } /* 0x0600000E System.Void UnityEngine.UIElements.UIElementsRuntimeUtilityNative::UpdateRuntimePanels() */,
	{ 0, 0 } /* 0x0600000F System.Void UnityEngine.UIElements.UIR.Utility::RaiseGraphicsResourcesRecreate(System.Boolean) */,
	{ 0, 0 } /* 0x06000010 System.Void UnityEngine.UIElements.UIR.Utility::RaiseEngineUpdate() */,
	{ 0, 0 } /* 0x06000011 System.Void UnityEngine.UIElements.UIR.Utility::RaiseFlushPendingResources() */,
	{ 0, 0 } /* 0x06000012 System.Void UnityEngine.UIElements.UIR.Utility::RaiseRegisterIntermediateRenderers(UnityEngine.Camera) */,
	{ 0, 0 } /* 0x06000013 System.Void UnityEngine.UIElements.UIR.Utility::RaiseRenderNodeAdd(System.IntPtr) */,
	{ 0, 0 } /* 0x06000014 System.Void UnityEngine.UIElements.UIR.Utility::RaiseRenderNodeExecute(System.IntPtr) */,
	{ 0, 0 } /* 0x06000015 System.Void UnityEngine.UIElements.UIR.Utility::RaiseRenderNodeCleanup(System.IntPtr) */,
	{ 0, 0 } /* 0x06000016 System.Void UnityEngine.UIElements.UIR.Utility::.cctor() */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
IL2CPP_EXTERN_C Il2CppSequencePoint g_sequencePointsUnityEngine_UIElementsNativeModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_UIElementsNativeModule[98] = 
{
	{ 18864, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 0 } /* seqPointIndex: 0 */,
	{ 18864, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 1 } /* seqPointIndex: 1 */,
	{ 18864, 1, 145, 145, 9, 10, 0, kSequencePointKind_Normal, 0, 2 } /* seqPointIndex: 2 */,
	{ 18864, 1, 146, 146, 13, 117, 1, kSequencePointKind_Normal, 0, 3 } /* seqPointIndex: 3 */,
	{ 18864, 1, 147, 147, 9, 10, 24, kSequencePointKind_Normal, 0, 4 } /* seqPointIndex: 4 */,
	{ 18864, 1, 146, 146, 13, 117, 3, kSequencePointKind_StepOut, 0, 5 } /* seqPointIndex: 5 */,
	{ 18864, 1, 146, 146, 13, 117, 14, kSequencePointKind_StepOut, 0, 6 } /* seqPointIndex: 6 */,
	{ 18865, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 7 } /* seqPointIndex: 7 */,
	{ 18865, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 8 } /* seqPointIndex: 8 */,
	{ 18865, 1, 157, 157, 9, 10, 0, kSequencePointKind_Normal, 0, 9 } /* seqPointIndex: 9 */,
	{ 18865, 1, 158, 158, 13, 92, 1, kSequencePointKind_Normal, 0, 10 } /* seqPointIndex: 10 */,
	{ 18865, 1, 159, 159, 9, 10, 16, kSequencePointKind_Normal, 0, 11 } /* seqPointIndex: 11 */,
	{ 18865, 1, 158, 158, 13, 92, 2, kSequencePointKind_StepOut, 0, 12 } /* seqPointIndex: 12 */,
	{ 18865, 1, 158, 158, 13, 92, 10, kSequencePointKind_StepOut, 0, 13 } /* seqPointIndex: 13 */,
	{ 18866, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 14 } /* seqPointIndex: 14 */,
	{ 18866, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 15 } /* seqPointIndex: 15 */,
	{ 18866, 2, 684, 684, 9, 10, 0, kSequencePointKind_Normal, 0, 16 } /* seqPointIndex: 16 */,
	{ 18866, 2, 685, 685, 13, 63, 1, kSequencePointKind_Normal, 0, 17 } /* seqPointIndex: 17 */,
	{ 18866, 2, 685, 685, 0, 0, 17, kSequencePointKind_Normal, 0, 18 } /* seqPointIndex: 18 */,
	{ 18866, 2, 686, 686, 13, 14, 20, kSequencePointKind_Normal, 0, 19 } /* seqPointIndex: 19 */,
	{ 18866, 2, 687, 687, 17, 89, 21, kSequencePointKind_Normal, 0, 20 } /* seqPointIndex: 20 */,
	{ 18866, 2, 689, 689, 13, 86, 32, kSequencePointKind_Normal, 0, 21 } /* seqPointIndex: 21 */,
	{ 18866, 2, 690, 690, 9, 10, 52, kSequencePointKind_Normal, 0, 22 } /* seqPointIndex: 22 */,
	{ 18866, 2, 687, 687, 17, 89, 26, kSequencePointKind_StepOut, 0, 23 } /* seqPointIndex: 23 */,
	{ 18866, 2, 689, 689, 13, 86, 44, kSequencePointKind_StepOut, 0, 24 } /* seqPointIndex: 24 */,
	{ 18867, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 25 } /* seqPointIndex: 25 */,
	{ 18867, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 26 } /* seqPointIndex: 26 */,
	{ 18867, 2, 703, 703, 9, 10, 0, kSequencePointKind_Normal, 0, 27 } /* seqPointIndex: 27 */,
	{ 18867, 2, 704, 704, 13, 64, 1, kSequencePointKind_Normal, 0, 28 } /* seqPointIndex: 28 */,
	{ 18867, 2, 704, 704, 0, 0, 17, kSequencePointKind_Normal, 0, 29 } /* seqPointIndex: 29 */,
	{ 18867, 2, 705, 705, 13, 14, 20, kSequencePointKind_Normal, 0, 30 } /* seqPointIndex: 30 */,
	{ 18867, 2, 706, 706, 17, 90, 21, kSequencePointKind_Normal, 0, 31 } /* seqPointIndex: 31 */,
	{ 18867, 2, 708, 708, 13, 64, 32, kSequencePointKind_Normal, 0, 32 } /* seqPointIndex: 32 */,
	{ 18867, 2, 709, 709, 9, 10, 49, kSequencePointKind_Normal, 0, 33 } /* seqPointIndex: 33 */,
	{ 18867, 2, 706, 706, 17, 90, 26, kSequencePointKind_StepOut, 0, 34 } /* seqPointIndex: 34 */,
	{ 18867, 2, 708, 708, 13, 64, 41, kSequencePointKind_StepOut, 0, 35 } /* seqPointIndex: 35 */,
	{ 18868, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 36 } /* seqPointIndex: 36 */,
	{ 18868, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 37 } /* seqPointIndex: 37 */,
	{ 18868, 3, 19, 19, 9, 10, 0, kSequencePointKind_Normal, 0, 38 } /* seqPointIndex: 38 */,
	{ 18868, 3, 20, 20, 13, 52, 1, kSequencePointKind_Normal, 0, 39 } /* seqPointIndex: 39 */,
	{ 18868, 3, 21, 21, 9, 10, 18, kSequencePointKind_Normal, 0, 40 } /* seqPointIndex: 40 */,
	{ 18868, 3, 20, 20, 13, 52, 12, kSequencePointKind_StepOut, 0, 41 } /* seqPointIndex: 41 */,
	{ 18869, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 42 } /* seqPointIndex: 42 */,
	{ 18869, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 43 } /* seqPointIndex: 43 */,
	{ 18869, 3, 25, 25, 9, 10, 0, kSequencePointKind_Normal, 0, 44 } /* seqPointIndex: 44 */,
	{ 18869, 3, 26, 26, 13, 51, 1, kSequencePointKind_Normal, 0, 45 } /* seqPointIndex: 45 */,
	{ 18869, 3, 27, 27, 9, 10, 18, kSequencePointKind_Normal, 0, 46 } /* seqPointIndex: 46 */,
	{ 18869, 3, 26, 26, 13, 51, 12, kSequencePointKind_StepOut, 0, 47 } /* seqPointIndex: 47 */,
	{ 18870, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 48 } /* seqPointIndex: 48 */,
	{ 18870, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 49 } /* seqPointIndex: 49 */,
	{ 18870, 4, 77, 77, 9, 10, 0, kSequencePointKind_Normal, 0, 50 } /* seqPointIndex: 50 */,
	{ 18870, 4, 78, 78, 13, 57, 1, kSequencePointKind_Normal, 0, 51 } /* seqPointIndex: 51 */,
	{ 18870, 4, 79, 79, 9, 10, 19, kSequencePointKind_Normal, 0, 52 } /* seqPointIndex: 52 */,
	{ 18870, 4, 78, 78, 13, 57, 13, kSequencePointKind_StepOut, 0, 53 } /* seqPointIndex: 53 */,
	{ 18871, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 54 } /* seqPointIndex: 54 */,
	{ 18871, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 55 } /* seqPointIndex: 55 */,
	{ 18871, 4, 85, 85, 9, 10, 0, kSequencePointKind_Normal, 0, 56 } /* seqPointIndex: 56 */,
	{ 18871, 4, 86, 86, 13, 38, 1, kSequencePointKind_Normal, 0, 57 } /* seqPointIndex: 57 */,
	{ 18871, 4, 86, 86, 0, 0, 10, kSequencePointKind_Normal, 0, 58 } /* seqPointIndex: 58 */,
	{ 18871, 4, 87, 87, 13, 14, 13, kSequencePointKind_Normal, 0, 59 } /* seqPointIndex: 59 */,
	{ 18871, 4, 89, 89, 17, 39, 14, kSequencePointKind_Normal, 0, 60 } /* seqPointIndex: 60 */,
	{ 18871, 4, 91, 91, 13, 14, 25, kSequencePointKind_Normal, 0, 61 } /* seqPointIndex: 61 */,
	{ 18871, 4, 92, 92, 9, 10, 26, kSequencePointKind_Normal, 0, 62 } /* seqPointIndex: 62 */,
	{ 18871, 4, 89, 89, 17, 39, 19, kSequencePointKind_StepOut, 0, 63 } /* seqPointIndex: 63 */,
	{ 18872, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 64 } /* seqPointIndex: 64 */,
	{ 18872, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 65 } /* seqPointIndex: 65 */,
	{ 18872, 4, 96, 96, 9, 10, 0, kSequencePointKind_Normal, 0, 66 } /* seqPointIndex: 66 */,
	{ 18872, 4, 97, 97, 13, 45, 1, kSequencePointKind_Normal, 0, 67 } /* seqPointIndex: 67 */,
	{ 18872, 4, 98, 98, 9, 10, 18, kSequencePointKind_Normal, 0, 68 } /* seqPointIndex: 68 */,
	{ 18872, 4, 97, 97, 13, 45, 12, kSequencePointKind_StepOut, 0, 69 } /* seqPointIndex: 69 */,
	{ 18873, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 70 } /* seqPointIndex: 70 */,
	{ 18873, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 71 } /* seqPointIndex: 71 */,
	{ 18873, 4, 102, 102, 9, 10, 0, kSequencePointKind_Normal, 0, 72 } /* seqPointIndex: 72 */,
	{ 18873, 4, 103, 103, 13, 59, 1, kSequencePointKind_Normal, 0, 73 } /* seqPointIndex: 73 */,
	{ 18873, 4, 104, 104, 9, 10, 19, kSequencePointKind_Normal, 0, 74 } /* seqPointIndex: 74 */,
	{ 18873, 4, 103, 103, 13, 59, 13, kSequencePointKind_StepOut, 0, 75 } /* seqPointIndex: 75 */,
	{ 18874, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 76 } /* seqPointIndex: 76 */,
	{ 18874, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 77 } /* seqPointIndex: 77 */,
	{ 18874, 4, 108, 108, 9, 10, 0, kSequencePointKind_Normal, 0, 78 } /* seqPointIndex: 78 */,
	{ 18874, 4, 109, 109, 13, 45, 1, kSequencePointKind_Normal, 0, 79 } /* seqPointIndex: 79 */,
	{ 18874, 4, 110, 110, 9, 10, 19, kSequencePointKind_Normal, 0, 80 } /* seqPointIndex: 80 */,
	{ 18874, 4, 109, 109, 13, 45, 13, kSequencePointKind_StepOut, 0, 81 } /* seqPointIndex: 81 */,
	{ 18875, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 82 } /* seqPointIndex: 82 */,
	{ 18875, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 83 } /* seqPointIndex: 83 */,
	{ 18875, 4, 114, 114, 9, 10, 0, kSequencePointKind_Normal, 0, 84 } /* seqPointIndex: 84 */,
	{ 18875, 4, 115, 115, 13, 49, 1, kSequencePointKind_Normal, 0, 85 } /* seqPointIndex: 85 */,
	{ 18875, 4, 116, 116, 9, 10, 19, kSequencePointKind_Normal, 0, 86 } /* seqPointIndex: 86 */,
	{ 18875, 4, 115, 115, 13, 49, 13, kSequencePointKind_StepOut, 0, 87 } /* seqPointIndex: 87 */,
	{ 18876, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 88 } /* seqPointIndex: 88 */,
	{ 18876, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 89 } /* seqPointIndex: 89 */,
	{ 18876, 4, 120, 120, 9, 10, 0, kSequencePointKind_Normal, 0, 90 } /* seqPointIndex: 90 */,
	{ 18876, 4, 121, 121, 13, 49, 1, kSequencePointKind_Normal, 0, 91 } /* seqPointIndex: 91 */,
	{ 18876, 4, 122, 122, 9, 10, 19, kSequencePointKind_Normal, 0, 92 } /* seqPointIndex: 92 */,
	{ 18876, 4, 121, 121, 13, 49, 13, kSequencePointKind_StepOut, 0, 93 } /* seqPointIndex: 93 */,
	{ 18877, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 94 } /* seqPointIndex: 94 */,
	{ 18877, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 95 } /* seqPointIndex: 95 */,
	{ 18877, 4, 81, 81, 9, 103, 0, kSequencePointKind_Normal, 0, 96 } /* seqPointIndex: 96 */,
	{ 18877, 4, 81, 81, 9, 103, 5, kSequencePointKind_StepOut, 0, 97 } /* seqPointIndex: 97 */,
};
#else
extern Il2CppSequencePoint g_sequencePointsUnityEngine_UIElementsNativeModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_UIElementsNativeModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[] = {
{ "", { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} }, //0 
{ "C:\\buildslave\\unity\\build\\External\\Yoga\\csharp\\Facebook.Yoga\\YogaNative.bindings.cs", { 109, 71, 63, 204, 40, 219, 212, 174, 56, 228, 126, 71, 91, 5, 149, 50} }, //1 
{ "C:\\buildslave\\unity\\build\\External\\Yoga\\csharp\\Facebook.Yoga\\YogaNode.cs", { 176, 175, 36, 90, 33, 158, 126, 48, 195, 55, 95, 218, 223, 85, 146, 224} }, //2 
{ "C:\\buildslave\\unity\\build\\Modules\\UIElementsNative\\UIElementsUtility.bindings.cs", { 202, 151, 33, 198, 223, 127, 255, 225, 25, 215, 218, 33, 130, 14, 64, 32} }, //3 
{ "C:\\buildslave\\unity\\build\\Modules\\UIElementsNative\\Renderer\\UIRenderer\\UIRenderer.bindings.cs", { 56, 78, 86, 130, 170, 98, 215, 90, 58, 169, 89, 112, 230, 232, 83, 75} }, //4 
};
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[4] = 
{
	{ 3169, 1 },
	{ 3170, 2 },
	{ 3172, 3 },
	{ 3174, 4 },
};
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[3] = 
{
	{ 0, 54 },
	{ 0, 51 },
	{ 0, 27 },
};
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[22] = 
{
	{ 0, 0, 0 } /* System.Void UnityEngine.Yoga.BaselineFunction::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0, 0 } /* System.Single UnityEngine.Yoga.BaselineFunction::Invoke(UnityEngine.Yoga.YogaNode,System.Single,System.Single) */,
	{ 0, 0, 0 } /* System.IAsyncResult UnityEngine.Yoga.BaselineFunction::BeginInvoke(UnityEngine.Yoga.YogaNode,System.Single,System.Single,System.AsyncCallback,System.Object) */,
	{ 0, 0, 0 } /* System.Single UnityEngine.Yoga.BaselineFunction::EndInvoke(System.IAsyncResult) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Yoga.MeasureFunction::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0, 0 } /* UnityEngine.Yoga.YogaSize UnityEngine.Yoga.MeasureFunction::Invoke(UnityEngine.Yoga.YogaNode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.Single,UnityEngine.Yoga.YogaMeasureMode) */,
	{ 0, 0, 0 } /* System.IAsyncResult UnityEngine.Yoga.MeasureFunction::BeginInvoke(UnityEngine.Yoga.YogaNode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.AsyncCallback,System.Object) */,
	{ 0, 0, 0 } /* UnityEngine.Yoga.YogaSize UnityEngine.Yoga.MeasureFunction::EndInvoke(System.IAsyncResult) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Yoga.Native::YGNodeMeasureInvoke(UnityEngine.Yoga.YogaNode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.IntPtr) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Yoga.Native::YGNodeBaselineInvoke(UnityEngine.Yoga.YogaNode,System.Single,System.Single,System.IntPtr) */,
	{ 54, 0, 1 } /* UnityEngine.Yoga.YogaSize UnityEngine.Yoga.YogaNode::MeasureInternal(UnityEngine.Yoga.YogaNode,System.Single,UnityEngine.Yoga.YogaMeasureMode,System.Single,UnityEngine.Yoga.YogaMeasureMode) */,
	{ 51, 1, 1 } /* System.Single UnityEngine.Yoga.YogaNode::BaselineInternal(UnityEngine.Yoga.YogaNode,System.Single,System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIElementsRuntimeUtilityNative::RepaintOverlayPanels() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIElementsRuntimeUtilityNative::UpdateRuntimePanels() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIR.Utility::RaiseGraphicsResourcesRecreate(System.Boolean) */,
	{ 27, 2, 1 } /* System.Void UnityEngine.UIElements.UIR.Utility::RaiseEngineUpdate() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIR.Utility::RaiseFlushPendingResources() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIR.Utility::RaiseRegisterIntermediateRenderers(UnityEngine.Camera) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIR.Utility::RaiseRenderNodeAdd(System.IntPtr) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIR.Utility::RaiseRenderNodeExecute(System.IntPtr) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIR.Utility::RaiseRenderNodeCleanup(System.IntPtr) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.UIElements.UIR.Utility::.cctor() */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
IL2CPP_EXTERN_C const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_UIElementsNativeModule;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_UIElementsNativeModule = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	98,
	(Il2CppSequencePoint*)g_sequencePointsUnityEngine_UIElementsNativeModule,
	0,
	(Il2CppCatchPoint*)g_catchPoints,
	4,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
