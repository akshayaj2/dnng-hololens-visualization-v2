use std::fmt;
use std::cmp;

use bytemuck::{Pod, Zeroable}; // Data
use serde::{Serialize};

use nalgebra::{Vector3}; // Math
use find_peaks::PeakFinder;

use crate::constants::*;
use crate::uncertainty;

// Exact structure of data in binary files
#[derive(Copy, Clone, Pod, Zeroable)]
#[repr(C, packed)]
pub struct FileEvent {
    pub board: i16,
    pub channel: i16,
    pub timestamp_ps: i64,
    pub energy: i16,
    pub short_gate: i16,
    pub flags: i32,
    pub num_wave: i32,
    pub samples: [u16; WAVE_SAMPLES],
}

// A pulse is either incomplete, a single co-incidence, or a CeBr pulse
#[derive(Clone, Debug)]
pub enum PulseType {
    File(Pulse),
    Single { bottom: Pulse, top: Pulse },
    CeBr(Pulse),
}

#[derive(Clone, Debug)]
pub struct Event {
    pub board: i16,
    pub channel: i16,
    pub bar: i16,
    pub timestamp_ps: i64,

    pub pulse: PulseType,
}

impl Event {
    pub fn total_peak(&self) -> f64 {
        match &self.pulse {
            PulseType::File(_) => panic!("Attempted to calculate with file-type pulse."),
            PulseType::Single { bottom, top } => bottom.peak + top.peak,
            PulseType::CeBr(pulse) => pulse.peak,
        }
    }
    pub fn total_integral(&self) -> f64 {
        match &self.pulse {
            PulseType::File(_) => panic!("Attempted to calculate with file-type pulse."),
            PulseType::Single { bottom, top } => bottom.integral + top.integral,
            PulseType::CeBr(pulse) => pulse.integral,
        }
    }
    pub fn total_tail_integral(&self) -> f64 {
        match &self.pulse {
            PulseType::File(_) => panic!("Attempted to calculate with file-type pulse."),
            PulseType::Single { bottom, top } => (bottom.tail_integral.powf(2.0) + top.tail_integral.powf(2.0)).sqrt(),
            PulseType::CeBr(pulse) => pulse.tail_integral,
        }
    }
    pub fn total_total_integral(&self) -> f64 {
        match &self.pulse {
            PulseType::File(_) => panic!("Attempted to calculate with file-type pulse."),
            PulseType::Single { bottom, top } => (bottom.total_integral.powf(2.0) + top.total_integral.powf(2.0)).sqrt(),
            PulseType::CeBr(pulse) => pulse.total_integral,
        }
    }
    pub fn total_dcfd_ns(&self) -> f64 {
        match &self.pulse {
            PulseType::File(_) => panic!("Attempted to calculate with file-type pulse."),
            PulseType::Single { bottom, top } => (bottom.dcfd_ns + top.dcfd_ns)/2.0,
            PulseType::CeBr(pulse) => pulse.dcfd_ns,
        }
    }

    pub fn z_ratio(&self) -> f64 {
        match &self.pulse {
            PulseType::Single { bottom, .. } => bottom.integral/self.total_integral(),
            _ => panic!("Attempted to calculate z-ratio non-single pulse."),
        }
    }
    pub fn z_pos(&self) -> f64 {
        let x = self.z_ratio();
        let (a, b, c, d) = Z_POSITION_CALIBRATION[self.bar as usize];

        return a + x * (b + x * (c + x * (d))); // Horner's method
    }
    pub fn pos(&self) -> Vector3<f64> {
        match &self.pulse {
            PulseType::Single { .. } => {
                let mut bar_pos = BAR_POSITIONS[self.bar as usize].clone();
                bar_pos.z = self.z_pos();

                return bar_pos;
            },
            PulseType::CeBr(_) => CEBR_POSITIONS[(self.channel - NON_COUPLES[0]) as usize].clone(),
            _ => panic!("Attempted to calculate pos on incomplete pulse."),
        }
    }

    pub fn neutron(&self, lo: f64) -> bool {
        match &self.pulse {
            PulseType::Single { .. } => {
                const PSD_LIGHT_OUTPUT_ENDPOINT: f64 = 112.5;

                let x = if self.light_output_kevee() < PSD_LIGHT_OUTPUT_ENDPOINT {
                    lo
                } else {
                    PSD_LIGHT_OUTPUT_ENDPOINT
                };

                let (a, b, c, d) = PSD_CUT[self.bar as usize];
                let ratio_line = a + x * (b + x * (c + x * (d)));

                let psd = self.total_tail_integral()/self.total_total_integral();

                return psd > ratio_line;
            },
            PulseType::CeBr(_) => false,
            _ => panic!("Attempted to classify incomplete pulse!"),
        }
    }

    pub fn light_output_kevee(&self) -> f64 {
        match &self.pulse {
            PulseType::Single { .. } => {
                let calibration_factor = LIGHT_OUTPUT_CALIBRATION[self.bar as usize];

                return self.total_integral() * calibration_factor;
            },
            PulseType::CeBr(_) => {
                let (a, b, c) = CEBR_LIGHT_OUTPUT_CALIBRATION[(self.channel - NON_COUPLES[0]) as usize];
                let x = self.total_integral();

                return c + x * (b + x * (a));
            },
            _ => panic!("Attempted to calculate light output for incomplete event!"),
        }
    }
    pub fn light_output_mev(&self) -> f64 {
        let (energy_deposited, table) = &*BIRKS;

        let LO = self.light_output_kevee();

        for light_out in 0..(table.len()-1) {
            if LO < table[light_out] {
                let y_1 = energy_deposited[light_out];
                let y_2 = energy_deposited[light_out + 1];
                let x_1 = table[light_out];
                let x_2 = table[light_out + 1];

                return y_1 + (LO - x_1) * (y_2-y_1)/(x_2-x_1);
            }
        }

        0.0
    }
}

#[derive(Serialize, Debug)]
pub struct Cone {
    gamma: bool,

    lever_arm: Vector3<f64>, // Lever arm

    x_1: Vector3<f64>,
    x_2: Vector3<f64>,

    x_1_uncertainty: Vector3<f64>,
    x_2_uncertainty: Vector3<f64>,

    pub alpha: f64, // Scattering angle: cos^2 \theta = (E_TOF)/E
    alpha_uncertainty: f64, // Propagated uncertainty

    energy: f64
}

#[derive(Debug)]
pub struct DoubleEvent {
    pub first: Event,
    pub second: Event,

    pub gamma: bool,
}

impl DoubleEvent {
    pub fn new(first: Event, second: Event, gamma: bool) -> Self {
        DoubleEvent {
            first: first,
            second: second,

            gamma: gamma,
        }
    }

    pub fn time_delta_ns(&self) -> f64 {
        (self.second.timestamp_ps - self.first.timestamp_ps) as f64/1000.0
    }
    pub fn dcfd_delta_ns(&self) -> f64 {
        self.second.total_dcfd_ns() - self.first.total_dcfd_ns()
    }

    pub fn direction(&self) -> Vector3<f64> {
        self.first.pos() - self.second.pos()
    }
    pub fn lever_arm(&self) -> Vector3<f64> {
        self.direction().normalize()
    }
    pub fn velocity_cm_ns(&self) -> f64 {
        self.direction().magnitude()/(self.time_delta_ns() + self.dcfd_delta_ns())
    }
    pub fn energy_tof(&self) -> f64 {
        0.5*939.566*(self.velocity_cm_ns()/29.97925).powf(2.0) // MeV
    }

    pub fn cone(&self) -> Cone {
        let (energy, alpha) = match self.gamma {
            false => {
                let E = self.first.light_output_mev() + self.energy_tof();

                (E, self.energy_tof()/E)
            },
            true => {
                let E: f64 = self.first.light_output_kevee()/1000.0 + self.second.light_output_kevee()/1000.0;
                let E_2: f64 = self.second.light_output_kevee()/1000.0;
                let m_e: f64 = 0.511; // MeV*c**2

                (E, (1.0 + m_e * ((1.0/E) - (1.0/E_2))).powf(2.0))
            }
        };
        let alpha_uncertainty = match self.gamma {
            false => uncertainty::alpha_uncertainty(&self),
            true => uncertainty::alpha_uncertainty_gamma(&self),
        };

        // FIXME: Add energy field
        Cone {
            gamma: self.gamma,

            lever_arm: self.lever_arm(),

            x_1: self.first.pos(),
            x_2: self.second.pos(),

            x_1_uncertainty: Vector3::new(0.3, 0.3, uncertainty::z_uncertainty(&self.first)),
            x_2_uncertainty: Vector3::new(0.3, 0.3, uncertainty::z_uncertainty(&self.second)),

            alpha: alpha,
            alpha_uncertainty: alpha_uncertainty,

            energy: energy
        }
    }
}

#[derive(Clone, Debug)]
pub struct Pulse {
    pub samples: Vec<f64>,

    pub peak: f64,
    pub peak_index: usize,

    pub dcfd_ns: f64,

    pub integral: f64,
    pub total_integral: f64,
    pub tail_integral: f64,
}

impl Pulse {
    pub fn new(samples: [u16; WAVE_SAMPLES]) -> Option<Self> {
        let mut pulse = Pulse {
            samples: samples.into_iter().map(|x| x as f64).collect(),

            peak: 0.0,
            peak_index: 0,

            dcfd_ns: 0.0,

            integral: 0.0,
            total_integral: 0.0,
            tail_integral: 0.0,
        };
        pulse.baseline_subtract();

        // Simple find peak with max
        let (peak_index, peak) =
            pulse.samples.iter().map(|x| *x).enumerate()
            .max_by(|x, y| x.1.partial_cmp(&y.1).unwrap()).unwrap_or((0, 0.0));
        pulse.peak_index = peak_index;
        if pulse.peak_index <= 45 || pulse.peak_index >= 80 { return None; }
        pulse.peak = peak * V_PER_LSB;

        // Find peak using PeakFinder (i.e. like scipy find_peaks)
        // let peaks = PeakFinder::new(&pulse.normalized())
        //     .with_min_prominence(0.15)
        //     .with_min_distance(10)
        //     .find_peaks();
        // if peaks.len() != 1 { return None; }
        // pulse.peak_index = peaks[0].middle_position();
        // if pulse.peak_index <= 45 || pulse.peak_index >= 80 { return None; }
        // pulse.peak = pulse.samples[pulse.peak_index] * V_PER_LSB;

        pulse.integral = pulse.calculate_integral();

        pulse.total_integral =
            pulse.samples[(pulse.peak_index + TOTAL_INTEGRAL_OFFSET)..cmp::min(pulse.peak_index + END_INTEGRAL_OFFSET, WAVE_SAMPLES)]
            .into_iter().sum::<f64>() * NS_PER_SAMPLE;

        pulse.tail_integral =
            pulse.samples[(pulse.peak_index + TAIL_INTEGRAL_OFFSET)..cmp::min(pulse.peak_index + END_INTEGRAL_OFFSET, WAVE_SAMPLES)]
            .into_iter().sum::<f64>() * NS_PER_SAMPLE;

        pulse.dcfd_ns = pulse.calculate_dcfd(0.2, pulse.peak_index);

        return Some(pulse);
    }

    pub fn baseline_subtract(&mut self) {
        let baseline_slice = &mut self.samples[0..BASELINE_SAMPLES];
        let sum: f64 = baseline_slice.into_iter().map(|x| *x).sum();
        let average = sum/BASELINE_SAMPLES as f64;
        //self.samples.iter_mut().for_each(|x| *x = f64::max(*x - average, 0.0));
        self.samples.iter_mut().for_each(|x| *x = *x - average);
    }
    pub fn normalized(&mut self) -> Vec<f64> {
        let max = self.samples.iter().map(|x| *x)
            .max_by(|x, y| x.partial_cmp(y).unwrap()).unwrap_or(0.0);
        return self.samples.iter().map(|x| *x/max).collect();
    }

    fn calculate_integral(&self) -> f64 {
        let begin_fraction = 0.05 * self.peak;
        let end_fraction = 0.01 * self.peak;

        let mut counter = self.peak_index;
        while self.samples[counter] > begin_fraction && counter > 1 { counter -= 1; }
        let begin_integral = counter - 1;

        let mut counter = self.peak_index;
        while self.samples[counter] > end_fraction && counter < WAVE_SAMPLES - 1 { counter += 1; }
        let end_integral = counter + 1;

        return self.samples[begin_integral..end_integral].into_iter().sum::<f64>() * V_PER_LSB * NS_PER_SAMPLE;
    }
    fn calculate_dcfd(&self, f: f64, max_idx: usize) -> f64 {
        let y = f * self.samples[max_idx];

        let mut counter = max_idx;
        while self.samples[counter] > y { counter -= 1; }

        let y_1 = self.samples[counter];
        let y_2 = self.samples[counter+1];
        let x_1 = (counter * 2) as f64;
        let x_2 = ((counter + 1) * 2) as f64;

        return x_1 + (y-y_1)*(x_2-x_1)/(y_2-y_1);
    }
}
