#![allow(non_snake_case)]

use std::env;
use std::fs;
use std::path;
use std::fs::File;
use std::error::Error;
use std::time::{Instant};
use std::sync::atomic::{AtomicUsize, Ordering};
use rayon::prelude::*;

mod uncertainty;
mod constants;
mod events;
mod parse;

use events::*;
use parse::FileResult;

fn main() -> Result<(), Box<dyn Error>> {
    // Initialize debug event counters
    let now = Instant::now();
    let total_events = AtomicUsize::new(0);
    let total_single_events = AtomicUsize::new(0);
    let total_double_events = AtomicUsize::new(0);

    let mut load_path: path::PathBuf = "./data".into();

    // A command line argument was provided
    let mut print_json: bool = false;
    if env::args().len() == 2 {
        let arg_path: &str = &env::args().nth(1).unwrap();
        load_path = arg_path.into();
    } else if env::args().len() > 2 {
        panic!("Incorrect command line arguments");
    }

    let mut entries: Vec<path::PathBuf> = if load_path.is_dir() {
        fs::read_dir(load_path)?.filter_map(|p| p.ok()).map(|e| e.path()).collect()
    } else {
        print_json = true;
        vec![load_path]
    };
    entries.sort();

    // Iterate through all data files, call parse_file on them (with counters)
    rayon::ThreadPoolBuilder::new().num_threads(12).build_global().unwrap();
    let results: Vec<FileResult> = entries.iter().filter_map(|path| {
        match path.extension() {
            Some(ext) if ext != "bin" => { return None; },
            None => { return None; },
            _ => {},
        }

        if !print_json {
            println!("Loading: {:?}", path);
        }

        return Some(
            parse::parse_file(&path, !print_json,
                              &total_events, &total_single_events, &total_double_events)
                .expect("Failed to parse file")
        );
    }).collect();

    if !print_json {
        println!("Took {} ms parsing files.", now.elapsed().as_millis());
    }

    // Retrieve counter values
    let total_events = total_events.into_inner();
    let total_single_events = total_single_events.into_inner();
    let total_double_events = total_double_events.into_inner();

    if !print_json {
        /*println!("\n --- \n");
        println!("Events: {}, Singles: {} ({:.3}%), Doubles: {} ({:.3}%), Cones: {} ({:.3}%)",
                total_events,
                total_single_events, (total_single_events as f64/total_events as f64) * 100.0,
                total_double_events, (total_double_events as f64/total_single_events as f64) * 100.0,
                cones.len(), (cones.len() as f64/total_double_events as f64) * 100.0);
        println!("");

        // Write cones to JSON
        let cones_output = File::create("cones.json")?;
        serde_json::to_writer(cones_output, &cones)?;*/
    } else {
        println!("{}", serde_json::to_string(&results[0])?);
    }

    Ok(())
}
