use crate::*;

use std::fs;
use once_cell::sync::Lazy;

pub const GEB_FACTORS: [(f64, f64, f64); 20] = [
    (0.0,    0.075419184, 0.0),
    (0.0,    0.083430745, 0.0),
    (0.0,    0.075281053, 0.0),
    (0.0,    0.079770291, 0.0),
    (0.0,    0.080253747, 0.0),
    (0.0,    0.075419184, 0.0),
    (0.0,    0.083430745, 0.0),
    (0.0,    0.075281053, 0.0),
    (0.0,    0.079770291, 0.0),
    (0.0,    0.080253747, 0.0),
    (0.0,    0.083430745, 0.0),
    (0.0,    0.075281053, 0.0),
    (-6.56E-05, 0.050731, -0.239498), 
    (-5.02E-05, 0.046818, -0.15571),  
    (-6.56E-05, 0.050731, -0.239498), 
    (-5.02E-05, 0.046818, -0.15571),  
    (-6.56E-05, 0.050731, -0.239498), 
    (-5.02E-05, 0.046818, -0.15571),  
    (-6.56E-05, 0.050731, -0.239498), 
    (-5.02E-05, 0.046818, -0.15571)
];


macro_rules! include_str_vec {
    ( $( $x: expr ),+ ) => {{
        let mut str_vec = Vec::new();
        $(
            str_vec.push(include_str!($x));
        )+
        str_vec
    }};
}

static Z_UNCERTAINTY_DATA: Lazy<[Vec<Vec<f64>>; 12]> = Lazy::new(|| {
    let mut z_uncertainty_data: Vec<&str> = include_str_vec!(
        "./Z_Data/Bar_0_un.txt", "./Z_Data/Bar_1_un.txt", "./Z_Data/Bar_2_un.txt", "./Z_Data/Bar_3_un.txt",
        "./Z_Data/Bar_4_un.txt", "./Z_Data/Bar_5_un.txt", "./Z_Data/Bar_6_un.txt", "./Z_Data/Bar_7_un.txt",
        "./Z_Data/Bar_8_un.txt", "./Z_Data/Bar_9_un.txt", "./Z_Data/Bar_10_un.txt", "./Z_Data/Bar_11_un.txt"
    );
    let mut z_uncertainty_per_bar: [Vec<Vec<f64>>; 12] = Default::default();

    for i in 0..12 {
        z_uncertainty_per_bar[i] =
            z_uncertainty_data[i].split('\n')
            .map(|line| {
                line.split_whitespace()
                    .filter_map(|s| s.parse::<f64>().ok()).collect()
            }).filter(|a: &Vec<f64>| a.len() > 0).collect();
    }

    return z_uncertainty_per_bar;
});

pub fn z_uncertainty(event: &Event) -> f64 {
    if matches!(event.pulse, PulseType::CeBr(_)) {
        return 0.3/(12.0f64.sqrt());
    }
    
    let z_pos = event.z_pos();
    let bar = event.bar;
    let light_output = event.light_output_kevee(); // FIXME: Use kevee here?

    let data_set = &Z_UNCERTAINTY_DATA[bar as usize];
    let min_lo: Vec<f64> = vec![25.0,50.0,75.0,100.0,125.0,150.0,175.0,200.0,225.0,250.0,275.0,300.0,325.0,350.0,375.0];

    let column = min_lo.into_iter().filter(|x| *x < light_output).count();
    let column_data: Vec<f64> = data_set.iter().map(|row| row[column]).collect();
    let x_set: Vec<f64> = data_set.iter().map(|row| row[0]).collect();

    if z_pos < x_set[0] {
        return column_data[0];
    } else if z_pos > x_set[x_set.len() - 1] {
        return column_data[column_data.len() - 1];
    } else {
        for (i, v) in x_set.iter().enumerate() {
            if *v > z_pos {
                let x = z_pos;
                let x_1 = x_set[i - 1];
                let x_2 = x_set[i];
                let y_1 = column_data[i - 1];
                let y_2 = column_data[i];

                return y_1 + (x - x_1)*(y_2-y_1)/(x_2-x_1);
            }
        }
    }

    return 0.3;
}

pub fn alpha_uncertainty(double_event: &DoubleEvent) -> f64 {
    let m_n: f64 = 939.56563; // MeV*c**2
    let speed_light: f64 = 29.97925; // cm/ns

    // ---

    let x_1: f64 = double_event.first.pos().x;
    let y_1: f64 = double_event.first.pos().y;
    let z_1: f64 = double_event.first.pos().z;

    let un_X_1: f64 = 0.3;
    let un_Y_1: f64 = 0.3;
    let un_Z_1: f64 = z_uncertainty(&double_event.first); // FIXME: Use Z_Uncertainty formula

    // ---

    let x_2: f64 = double_event.second.pos().x;
    let y_2: f64 = double_event.second.pos().y;
    let z_2: f64 = double_event.second.pos().z;

    let un_X_2: f64 = 0.3;
    let un_Y_2: f64 = 0.3;
    let un_Z_2: f64 = z_uncertainty(&double_event.second); // FIXME: Use Z_Uncertainty formula

    // ---

    let t_diff: f64 = double_event.time_delta_ns() + double_event.dcfd_delta_ns();
    let uncer_t_diff: f64 = 0.217; // ns

    let E_dep: f64 = double_event.first.light_output_mev();
    let uncer_E_dep: f64 = 0.1; // Event[17]?

    (E_dep.powf(2.0)*(0.25*m_n.powf(2.0)*un_X_1.powf(2.0)*(2.0*x_1 - 2.0*x_2).powf(2.0)/(speed_light.powf(4.0)*t_diff.powf(4.0)) + 
                      0.25*m_n.powf(2.0)*un_X_2.powf(2.0)*(-2.0*x_1 + 2.0*x_2).powf(2.0)/(speed_light.powf(4.0)*t_diff.powf(4.0)) + 
                      0.25*m_n.powf(2.0)*un_Y_1.powf(2.0)*(2.0*y_1 - 2.0*y_2).powf(2.0)/(speed_light.powf(4.0)*t_diff.powf(4.0)) + 
                      0.25*m_n.powf(2.0)*un_Y_2.powf(2.0)*(-2.0*y_1 + 2.0*y_2).powf(2.0)/(speed_light.powf(4.0)*t_diff.powf(4.0)) + 
                      0.25*m_n.powf(2.0)*un_Z_1.powf(2.0)*(2.0*z_1 - 2.0*z_2).powf(2.0)/(speed_light.powf(4.0)*t_diff.powf(4.0)) + 
                      0.25*m_n.powf(2.0)*un_Z_2.powf(2.0)*(-2.0*z_1 + 2.0*z_2).powf(2.0)/(speed_light.powf(4.0)*t_diff.powf(4.0)) + 
                      1.0*m_n.powf(2.0)*uncer_t_diff.powf(2.0)*((-x_1 + x_2).powf(2.0) + (-y_1 + y_2).powf(2.0) + 
                      (-z_1 + z_2).powf(2.0)).powf(2.0)/(speed_light.powf(4.0)*t_diff.powf(6.0))).powf(1.0)/(E_dep + 0.5*m_n*((-x_1 + x_2).powf(2.0) + 
                      (-y_1 + y_2).powf(2.0) + (-z_1 + z_2).powf(2.0))/(speed_light.powf(2.0)*t_diff.powf(2.0))).powf(4.0) + 
                      0.25*m_n.powf(2.0)*uncer_E_dep.powf(2.0)*((-x_1 + x_2).powf(2.0) + (-y_1 + y_2).powf(2.0) + 
                      (-z_1 + z_2).powf(2.0)).powf(2.0)/(speed_light.powf(4.0)*t_diff.powf(4.0)*(E_dep + 0.5*m_n*((-x_1 + x_2).powf(2.0) + 
                      (-y_1 + y_2).powf(2.0) + (-z_1 + z_2).powf(2.0))/(speed_light.powf(2.0)*t_diff.powf(2.0))).powf(4.0))).powf(0.5)
}

pub fn e_dep_uncertainty(double_event: &DoubleEvent) -> (f64, f64) {
    let E_1 = double_event.first.light_output_kevee()/1000.0;
    let E_2 = double_event.second.light_output_kevee()/1000.0;

    let bar_1 = if matches!(double_event.first.pulse, PulseType::CeBr(_)) {
        (double_event.first.channel - constants::NON_COUPLES[0]) as usize
    } else {
        double_event.first.bar as usize
    };
    let bar_2 = if matches!(double_event.second.pulse, PulseType::CeBr(_)) {
        (double_event.second.channel - constants::NON_COUPLES[0]) as usize
    } else {
        double_event.second.bar as usize
    };

    let (a_1, b_1, c_1) = GEB_FACTORS[bar_1];
    let (a_2, b_2, c_2) = GEB_FACTORS[bar_2];

    let un_E_1 = (a_1 + b_1*(E_1 + c_1*E_1.powf(2.0)).sqrt())/2.355;
    let un_E_2 = (a_2 + b_2*(E_2 + c_2*E_2.powf(2.0)).sqrt())/2.355;

    return (un_E_1, un_E_2);
}

pub fn alpha_uncertainty_gamma(double_event: &DoubleEvent) -> f64 {
    let m_e: f64 = 0.511; // MeV*c**2

    // ---

    let un_Z_1: f64 = z_uncertainty(&double_event.first);

    let E_1 = double_event.first.light_output_kevee()/1000.0;

    // ---

    let un_Z_2: f64 = z_uncertainty(&double_event.second);

    let E_2 = double_event.second.light_output_kevee()/1000.0;

    // ---

    let (un_E_1, un_E_2) = e_dep_uncertainty(double_event);
    //println!("Energy: {:.4}, {:.4}, Uncertainty: {:.4}, {:.4}", E_1, E_2, un_E_1, un_E_2);

    (4.0*m_e.powf(2.0)*un_E_1.powf(2.0)*(m_e*(1.0/(E_1 + E_2) - 1.0/E_2) + 1.0).powf(2.0)/(E_1 + E_2).powf(4.0) + 
                  4.0*m_e.powf(2.0)*un_E_2.powf(2.0)*(m_e*(1.0/(E_1 + E_2) - 1.0/E_2) + 1.0).powf(2.0)*(-1.0/(E_1 + E_2).powf(2.0) + 
                  E_2.powf(-2.0)).powf(2.0)).powf(0.5)
}
