use std::fs;
use std::fs::File;
use std::path;
use std::io::{BufReader, Read};

use std::error::Error;

use std::sync::atomic::{AtomicUsize, Ordering};
use rayon::prelude::*;
use serde::{Serialize};

use crate::uncertainty;
use crate::constants::*;
use crate::events::*;

type CountsPerBar = [f64; 20];

pub fn parse_events(file: &mut fs::File) -> Result<Vec<Event>, Box<dyn Error>> {
    // Load events
    let mut buffer: Vec<u8> = Vec::with_capacity(file.metadata()?.len() as usize);
    BufReader::new(file).read_to_end(&mut buffer)?;
    let file_events: &[FileEvent] = bytemuck::cast_slice(buffer.as_slice());

    let mut events: Vec<Event> = file_events.par_iter().filter_map(|&fe| {
        let channel = fe.channel + if fe.board == 1 { 16 } else { 0 };
        let cebr: bool = NON_COUPLES.contains(&channel);
        // if cebr { return None };
        let bar = if cebr { channel } else { channel/2 };

        let event = Event {
            board: fe.board,
            channel: channel,
            bar: bar,

            timestamp_ps: fe.timestamp_ps + match cebr {
                false => TIME_OFFSETS_NS[bar as usize] * 1000,
                true => CEBR_TIME_OFFSETS_NS[(channel - NON_COUPLES[0]) as usize] * 1000,
            },

            pulse: match cebr {
                false => PulseType::File(Pulse::new(fe.samples)?),
                true => PulseType::CeBr(Pulse::new(fe.samples)?),
            },
        };
        return Some(event);
    }).collect();
    events.sort_unstable_by(|a, b| a.timestamp_ps.cmp(&b.timestamp_ps));

    Ok(events)
}

pub fn parse_single(events: &Vec<Event>, debug: bool) -> Result<(Vec<Event>, CountsPerBar), Box<dyn Error>> {
    // Identify single co-incidences
    let mut single_events: Vec<Event> = vec![];
    let mut counts_per_bar: CountsPerBar = [0.0; 20];

    let mut total_identified = 0;
    let mut light_output_filtered = 0;
    let mut z_pos_filtered = 0;

    for current in 0..events.len() {
        let event = &events[current];

        if matches!(event.pulse, PulseType::CeBr(_)) {
            if event.total_integral() > 25.0 && event.total_integral() < 1500.0 {
                counts_per_bar[(12 + (event.channel - NON_COUPLES[0])) as usize] += 1.0;
                single_events.push(event.clone());
            }
            continue;
        }

        for next in 1..SLIDING_WINDOW_LENGTH {
            if current + next >= events.len() { break; }

            let next_event = &events[current + next];

            if event.bar == next_event.bar && event.channel != next_event.channel {
                let time_delta_ns = (next_event.timestamp_ps - event.timestamp_ps) as f64/1000.0;
                if time_delta_ns > SINGLE_WINDOW_NS { break; }

                let bottom_event = if event.channel % 2 == 0 { event } else { next_event };
                let top_event = if event.channel % 2 == 0 { next_event } else { event };

                // Create combined event, average timestamps.
                //  - Use bottom event as base, copy top event's pulse
                let mut combined_event = bottom_event.clone();
                combined_event.timestamp_ps = (top_event.timestamp_ps + bottom_event.timestamp_ps)/2;
                combined_event.pulse = PulseType::Single {
                    bottom: match combined_event.pulse {
                        PulseType::File(p) => p,
                        _ => panic!("Attempting to combine non-file pulses!"),
                    },
                    top: match &top_event.pulse {
                        PulseType::File(p) => p.clone(),
                        _ => panic!("Attempting to combine non-file pulses!"),
                    },
                };

                total_identified += 1;
                // Apply Light Output cuts
                if combined_event.light_output_kevee() > LIGHT_OUTPUT_MIN && combined_event.light_output_kevee() < LIGHT_OUTPUT_MAX {
                    // Apply Z-Position cuts
                    let z_pos = combined_event.z_pos();
                    let z_pos_uncertainty = uncertainty::z_uncertainty(&combined_event);
                    if z_pos.abs() - z_pos_uncertainty < Z_POSITION_RANGE {
                        counts_per_bar[combined_event.bar as usize] += 1.0;
                        single_events.push(combined_event);
                    } else {
                        z_pos_filtered += 1;
                    }
                } else {
                    light_output_filtered += 1;
                }
            }
        }
    }

    if debug {
        println!("Out of {} identified: Light output filtered: {}, Z-Pos filtered: {}",
                total_identified, light_output_filtered, z_pos_filtered);
    }

    // Sort by timestamp + DCFD
    single_events.sort_unstable_by(|a, b| {
        let a_start_time_ns = (a.timestamp_ps as f64/1000.0) + a.total_dcfd_ns();
        let b_start_time_ns = (b.timestamp_ps as f64/1000.0) + b.total_dcfd_ns();
        a_start_time_ns.partial_cmp(&b_start_time_ns).unwrap()
    });

    // Not normalizing for now...
    // let max_counts = counts_per_bar.iter().max_by(|a, b| a.partial_cmp(b).unwrap()).unwrap();
    // counts_per_bar = counts_per_bar.map(|x| x/max_counts);

    Ok((single_events, counts_per_bar))
}

pub fn parse_double(single_events: &Vec<Event>, debug: bool) -> Result<(Vec<DoubleEvent>, usize), Box<dyn Error>> {
    let mut double_events: Vec<DoubleEvent> = vec![];

    // Filter counters
    let mut total_identified = 0;
    let mut nn_identified = 0;
    let mut gamma_identified = 0;

    let mut nn_filtered = 0;
    let mut tof_filtered = 0;
    let mut time_cut_filtered = 0;

    let mut i = 0;
    // FIXME: Don't run if single_events.len() < 4
    while i < single_events.len()-3 {
        let first = &single_events[i];
        let second = &single_events[i+1];
        let third = &single_events[i+2];

        let diff = (second.timestamp_ps - first.timestamp_ps) as f64/1000.0;
        let second_diff = (third.timestamp_ps - first.timestamp_ps) as f64/1000.0;

        if diff < DOUBLE_WINDOW_NS {
            total_identified += 1;

            match (&first.pulse, &second.pulse) {
                (PulseType::Single { .. }, PulseType::Single { .. }) => { // Two Non-CeBr pulses
                    // Both must be neutrons
                    if first.neutron(first.light_output_kevee()) && second.neutron(first.light_output_kevee()) {
                        let double_event = DoubleEvent::new(first.clone(), second.clone(), false);

                        if double_event.second.light_output_mev() < double_event.energy_tof() {
                            double_events.push(double_event);
                            nn_identified += 1;
                        } else {
                            tof_filtered += 1;
                        }
                    } else {
                        nn_filtered += 1;
                    }

                    i += 2;
                },
                (PulseType::Single { .. }, PulseType::CeBr(_)) => { // A Non-CeBr, CeBr pulse in that order
                    // The first must be a gamma
                    // And not a triple, (so the second diff should be greater than the window)
                    if !(second_diff < DOUBLE_WINDOW_NS) && !first.neutron(first.light_output_kevee()) {
                        let double_event = DoubleEvent::new(first.clone(), second.clone(), true);
                        let cone = double_event.cone();
                        let angle = cone.alpha.acos();

                        double_events.push(double_event);
                        gamma_identified += 1;
                    } else if second_diff < DOUBLE_WINDOW_NS {
                        i += 1;
                    }
                    i += 2;
                },
                _ => {i += 2;}, // Any other order we don't care about (CeBr, Cebr), (CeBr, Single)
            }
        } else {
            i += 1;
        }
    }

    if debug {
        println!("Out of {}/{}/{} (Total/NN/Gamma) identified: NN filtered: {}, E_2 > E_TOF filtered: {}, DCFD Time Cut filtered: {}",
                 total_identified, nn_identified, gamma_identified,
                 nn_filtered, tof_filtered, time_cut_filtered);
    }

    Ok((double_events, total_identified))
}

#[derive(Serialize, Debug)]
pub struct FileResult {
    pub cones: Vec<Cone>,
    pub counts_per_bar: CountsPerBar,
    pub measurement_time: f64,
}

pub fn parse_file(path: &path::PathBuf, debug: bool,
              total_events: &AtomicUsize, total_single_events: &AtomicUsize, total_double_events: &AtomicUsize) -> Result<FileResult, Box<dyn Error>> {
    //let mut file = BufReader::new(File::open(path)?);
    let mut file = File::open(path)?;

    // Load events from file, then:
    //  - Parse single co-incidences
    //  - Parse double co-incidences
    let events: Vec<Event> = parse_events(&mut file)?;
    let (single_events, counts_per_bar) = parse_single(&events, debug)?;
    let (double_events, identified_double_events) = parse_double(&single_events, debug)?;

    if debug {
        println!("[{:?}] {} Events, {} Singles, {} Doubles, {} Cones",
                    path, events.len(), single_events.len(), identified_double_events, double_events.len());
        println!("Counts per bar: {:?}", counts_per_bar);
        println!("");
    }

    // Increment counters
    total_events.fetch_add(events.len(), Ordering::SeqCst);
    total_single_events.fetch_add(single_events.len(), Ordering::SeqCst);
    total_double_events.fetch_add(identified_double_events, Ordering::SeqCst);

    Ok(FileResult {
        cones: double_events.iter().map(|e| e.cone()).collect(),
        counts_per_bar,
        measurement_time: (match single_events.len() {
            0 => 0,
            _ => single_events.last().unwrap().timestamp_ps - single_events[0].timestamp_ps
        } as f64)/1000.0
    })
}
