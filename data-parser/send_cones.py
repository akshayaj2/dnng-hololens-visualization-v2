import os
import time
import requests
import subprocess
import json
import numpy as np

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
print('Loading Tensorflow...')
#import tensorflow

np.set_printoptions(suppress=True)

# endpoint = 'http://localhost:10082'
endpoint = 'http://35.3.201.144:10082'

model_name = 'aug_a_model'
# data_name = 'RL63'
# data_path = f'C:/Users/Z640/Desktop/H2DPI_Project/Hololens/DAQ/{data_name}/RAW'
data_path = './RL52'
data_files = []

print(f'Loading model [{model_name}]...')
model = tensorflow.keras.models.load_model(f'./{model_name}')
model.summary()

try:
    requests.post(endpoint + '/clearCones', timeout=2)
except:
    print('Failed to clear cones... Network error!')

def pretty_print_counts(counts):
    output = (
        f'[12|{counts[12]}]                        [14|{counts[14]}]\n'
        f'[13|{counts[13]}]  00|{counts[0]}  01|{counts[1]}  [15|{counts[15]}]\n'
        f'  02|{counts[2]}  03|{counts[3]}  04|{counts[4]}  05|{counts[5]}\n'
        f'  06|{counts[6]}  07|{counts[7]}  08|{counts[8]}  09|{counts[9]}\n'
        f'[16|{counts[16]}]  10|{counts[10]}  11|{counts[11]}  [18|{counts[18]}]\n'
        f'[17|{counts[17]}]                        [19|{counts[19]}]'
    )
    print(output)

def process_path(path):
    file_json = subprocess.run(['./data_parser.exe', path],
                            stdout=subprocess.PIPE)
    try:
        file_result = json.loads(file_json.stdout.strip())
    except:
        print("Error while parsing bin!\n")
        return

    cones = file_result['cones']
    counts_per_bar = [int(i) for i in file_result['counts_per_bar']]
    normalized_counts_per_bar = [float(i)/max(counts_per_bar) for i in counts_per_bar]
    measurement_time_ns = file_result['measurement_time']
    measurement_time_s = measurement_time_ns/1000000000
    measurement_time_m = measurement_time_s/60

    gamma_count = len(list(filter(lambda c: c['gamma'], cones)))
    neutron_count = len(cones) - gamma_count

    model_result = model.predict(np.array([normalized_counts_per_bar]))
    angle_certainties = [0] * 360
    if len(model_result) > 0:
        angle_certainties = model_result[0].tolist()
        for i in range(90): # Rotate 90 clockwise
            angle_certainties.append(angle_certainties.pop(0))
        angle_certainties.reverse() # Reflect counterclockwise -> clockwise
    angle = angle_certainties.index(max(angle_certainties))

    print('Single co-incidence/CeBr counts:')
    pretty_print_counts(counts_per_bar)
    print('---')
    print('Normalized counts:', [f'{i:.2f}' for i in normalized_counts_per_bar])
    print(f'Predicted angle: {angle} degrees')
    # print(f'Predicted angle: {angle} degrees | Result: {model_result}')
    print(f'Measurement time: {measurement_time_s:.2f} s')
    print(
        f'Gamma cones: {gamma_count} [{gamma_count/measurement_time_m:.3f} cones/minute]', ' | ',
        f'Neutron cones: {neutron_count} [{neutron_count/measurement_time_m:.3f} cones/minute]'
    )
    print('')

    try:
        requests.post(endpoint + "/uploadCones", json=cones, timeout=2)
        requests.post(endpoint + "/direction", json=angle_certainties, timeout=2)
    except Exception as e:
        print(f'Failed to send file information... {e}\n')
        return

while True:
    if not os.path.isdir(data_path):
        time.sleep(1)
        print('Waiting for data path to exist...')
        continue

    new_data_files = [(data_path + '/' + f) for f in os.listdir(data_path) if (data_path + '/' + f) not in data_files]
    new_data_files.sort(key=os.path.getmtime)

    if len(new_data_files) > 1:
        path = new_data_files[0]
        print(path)

        data_files.append(path)
        process_path(path)

    time.sleep(0.1)
