use std::fs::File;
use std::io::prelude::*;
use std::error::Error;

use std::env;

use std::io::Cursor;
use image::{GenericImage, GenericImageView, ImageBuffer, Rgb32FImage, RgbImage};

fn main() -> Result<(), Box<dyn Error>> {
    let in_path = env::args().nth(1).unwrap();
    let out_path = env::args().nth(2).unwrap();

    let mut data = File::open(in_path)?;
    let mut data_contents = String::new();
    data.read_to_string(&mut data_contents);

    let mut image: RgbImage = ImageBuffer::new(360, 360);

    for line in data_contents.split('\n').enumerate() {
        let (y, line) = line;
        for number in line.split(',').enumerate() {
            let (x, number) = number;

            if x >= 360 || y >= 360 { continue; }
            let value: f32 = number.parse().unwrap_or(0f32);
            let value255: u8 = ((value/6f32)*255f32) as u8;
            image.put_pixel(x as u32, y as u32, image::Rgb([value255, value255, value255]));
        }
    }

    image.save(out_path)?;

    //println!("{}", data_contents);
    Ok(())
}
