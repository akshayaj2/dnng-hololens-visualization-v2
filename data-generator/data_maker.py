import math
import random
import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.rcsetup as rcsetup

"""
Created on Mon May 16 09:17:10 2022
@author: pakari
"""
N = 1000
X = np.linspace(-180, 180, N) # Azimuth
Y = np.linspace(-180, 180, N) # Altitude

X, Y = np.meshgrid(X, Y)
# Mean vector and covariance matrix
mu = np.array([50., -30.])
Sigma = np.array([[ 50. , -0.5], [-0.5,  50.]])
# Pack X and Y into a single 3-dimensional array
pos = np.empty(X.shape + (2,))
pos[:, :, 0] = X
pos[:, :, 1] = Y
def multivariate_gaussian(pos, mu, Sigma):
    n = mu.shape[0]
    Sigma_det = np.linalg.det(Sigma)
    Sigma_inv = np.linalg.inv(Sigma)
    N = np.sqrt((2*np.pi)**n * Sigma_det)
    # This einsum call calculates (x-mu)T.Sigma-1.(x-mu) in a vectorized
    # way across all the input variables.
    fac = np.einsum("...k,kl,...l->...", pos-mu, Sigma_inv, pos-mu)
    return np.exp(-fac / 2) / N

# The distribution on the variables X, Y packed into pos.
Z = multivariate_gaussian(pos, mu, Sigma)
Z2 = multivariate_gaussian(pos, np.array([-50., 30.]), Sigma)
# plot using subplots
fig = plt.figure()
plt.contourf(X, Y , Z + Z2)
#plt.xlabel("Azimuthal Angle (θ)")
#plt.ylabel("Altitude Angle (φ)")
plt.axis('off')
plt.gray()
plt.savefig("pic.png", format = "png", bbox_inches='tight', pad_inches = 0)
plt.show()
