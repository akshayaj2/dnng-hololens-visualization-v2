import math
import random
import numpy as np
import matplotlib.pyplot as plt
import base64
from PIL import Image

im = Image.open("pic.png")
im = im.resize((360, 360))
im.save("test.png")
pix = im.load()
result = ""
for i in range(360):
    for j in range(360):
        result += str(pix[j,i][0]) + "e1,"
    result += "\n"
#print(result)
f = open("data.txt", "w")
f.write(result)
