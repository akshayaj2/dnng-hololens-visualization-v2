﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// WinRT.Interop.DelegateProperty`1<WinRT.Interop.IAsyncOperation/CompletedHandler>
struct DelegateProperty_1_t252391BD875C0B5B4BF231D801CE1B88C4E5A9A8;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct Dictionary_2_tDE3993BD3BBC5AA17021AEB6C4928B55CF0AA730;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Boolean>
struct Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,Cone>
struct Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<System.Int32Enum>>
struct Func_2_tED7578086478CDE88DBC2FFFDCB01FEA9F167392;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<System.Object>>
struct Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD;
// System.Func`2<Cone,System.Boolean>
struct Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>
struct Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>
struct Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>
struct Func_2_t578287205B88E28024F821D907D73582B53344F1;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>
struct Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct Func_2_tDEA980C7282507293B34B1074F07AFA084A38186;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>
struct Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>
struct Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Func`2<System.Object,Cone>
struct Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F;
// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7;
// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>
struct Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,Cone>
struct Func_2_tE0F030B2CA607826B25983C128370C60E0480620;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>
struct Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>
struct Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct IEnumerable_1_tE638BB72F39262C88C3BBC55F1F3EC6253CE14C6;
// System.Collections.Generic.IEnumerable`1<Cone>
struct IEnumerable_1_t4B9E2A964A5B4A50967399D195576490AAA26392;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerable_1_t8013B5EA149B327D6B08316A0ADE34E776668D5F;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct IEnumerable_1_tDC7510525C42BA1DA3ED8C3DE6E534F0FB5F658A;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct IEnumerable_1_t1B9DD6A9441FE3201F74A06FCC2D009059BAAF60;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>
struct IEnumerable_1_t8082C167A55EAC0A388D4FDD25627B3F596FE228;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct IEnumerable_1_tB66D096E5149D7BE3B0DD7BC2C5E975719B3F731;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct IEnumerator_1_t145EADEA08BF9F918D8DE9AA029E22ED3DB88ED1;
// System.Collections.Generic.IEnumerator`1<Cone>
struct IEnumerator_1_t0E2BBC2DFB9910F194BA8CC6B902C76AA11A2051;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerator_1_tF8232127F167F8B87BB459DF2426805233968856;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct IEnumerator_1_t014BD502DF265772AC3676786404E5E010AD9C49;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct IEnumerator_1_t2A5DE76525F155DB72F6085E476DF35BDF3718EF;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2DC97C7D486BF9E077C2BC2E517E434F393AA76E;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>
struct IEnumerator_1_tEAF1A760CE940EC4925B4301D962FB748D82D084;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct IEnumerator_1_t02E20491457E5A188A81B5D394BFC2E748BDCBF0;
// System.Linq.Enumerable/Iterator`1<Cone>
struct Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E;
// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507;
// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>
struct List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>
struct List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2;
// WinRT.ObjectReference`1<WinRT.Interop.IAsyncInfo/Vftbl>
struct ObjectReference_1_t41A1DD8B8CAFFA34FCBA13CED053F85524BF0A2B;
// WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>
struct ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD;
// System.Threading.SparselyPopulatedArrayFragment`1<System.Threading.CancellationCallbackInfo>
struct SparselyPopulatedArrayFragment_1_t93197EF47D6A025755987003D5D62F3AED371C21;
// System.Threading.Tasks.TaskCompletionSource`1<System.Int32Enum>
struct TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354;
// System.Threading.Tasks.TaskCompletionSource`1<System.Object>
struct TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903;
// System.Threading.Tasks.TaskFactory`1<System.Int32Enum>
struct TaskFactory_1_tB258CD0C610C65E36B1AC1470D310CC8290BDA75;
// System.Threading.Tasks.TaskFactory`1<System.Object>
struct TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55;
// System.Threading.Tasks.Task`1<System.Int32Enum>
struct Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>
struct WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>
struct WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>
struct WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,Cone>
struct WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>
struct WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>
struct WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>
struct WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,Cone>
struct WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>
struct WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>
struct WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>
struct WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,Cone>
struct WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>
struct WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>
struct WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC;
// WinRT.Interop._IAsyncOperation`2<System.Object,System.Int32Enum>
struct _IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42;
// WinRT.Interop._IAsyncOperation`2<System.Object,System.Object>
struct _IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>[]
struct KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD;
// UnityEngine.InputSystem.Utilities.NameAndParameters[]
struct NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09;
// UnityEngine.InputSystem.Utilities.NamedValue[]
struct NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.InputSystem.Utilities.Substring[]
struct SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1;
// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue[]
struct JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Threading.CancellationCallbackInfo
struct CancellationCallbackInfo_t7FC8CF6DB4845FCB0138771E86AE058710B1117B;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3;
// System.Threading.ContextCallback
struct ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// WinRT.Interop.IAsyncInfo
struct IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9;
// WinRT.Interop.IAsyncOperation
struct IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// WinRT.Interop.IDelegate2_Obj_Enum
struct IDelegate2_Obj_Enum_t1B6381C6004ABDE20A651D52B90592E07CD9FF1E;
// WinRT.IObjectReference
struct IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444;
// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// System.Threading.Tasks.StackGuard
struct StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D;
// System.String
struct String_t;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// WinRT.Interop._get_PropertyAsObject
struct _get_PropertyAsObject_tB6DABCBEE76A0489F547E29DCD7309085C4B32D3;
// WinRT.Interop._put_PropertyAsObject
struct _put_PropertyAsObject_t65BE7D6576FA6A84A0205925C7420D5CF3239980;
// WinRT.Interop.IAsyncOperation/CompletedHandler
struct CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E;
// WinRT.Interop.IInspectableVftbl/_GetIids
struct _GetIids_tDC148CE7B602F998B5A9C63C47FD75466894A52F;
// WinRT.Interop.IInspectableVftbl/_GetRuntimeClassName
struct _GetRuntimeClassName_t1FD45F08090CED02A6328E976D1B846CBDB7133C;
// WinRT.Interop.IInspectableVftbl/_GetTrustLevel
struct _GetTrustLevel_t41789FA2A2D2CC0D11B6128565B223154D91C803;
// WinRT.Interop.IUnknownVftbl/_AddRef
struct _AddRef_t2BAF9ED563D7BE7BBAA3ECB1E966712BC891E4F6;
// WinRT.Interop.IUnknownVftbl/_QueryInterface
struct _QueryInterface_t095A9E6C5C1A299ED61A65C48DA4A1E05FEBD861;
// WinRT.Interop.IUnknownVftbl/_Release
struct _Release_tAEF27E0E04B097DCD22D6E226BA893BA116B99CB;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0;

IL2CPP_EXTERN_C RuntimeClass* Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0<System.Object,System.Int32Enum>
struct U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124  : public RuntimeObject
{
public:
	// WinRT.Interop._IAsyncOperation`2<TTask,TResult> WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::<>4__this
	_IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<TResult> WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::source
	TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 * ___source_1;
	// WinRT.Interop.IAsyncInfo WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::info
	IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * ___info_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124, ___U3CU3E4__this_0)); }
	inline _IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline _IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(_IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124, ___source_1)); }
	inline TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 * get_source_1() const { return ___source_1; }
	inline TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 ** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 * value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_1), (void*)value);
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124, ___info_2)); }
	inline IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * get_info_2() const { return ___info_2; }
	inline IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 ** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___info_2), (void*)value);
	}
};


// WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0<System.Object,System.Object>
struct U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC  : public RuntimeObject
{
public:
	// WinRT.Interop._IAsyncOperation`2<TTask,TResult> WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::<>4__this
	_IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<TResult> WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::source
	TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 * ___source_1;
	// WinRT.Interop.IAsyncInfo WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::info
	IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * ___info_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC, ___U3CU3E4__this_0)); }
	inline _IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline _IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(_IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC, ___source_1)); }
	inline TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 * get_source_1() const { return ___source_1; }
	inline TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 ** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 * value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_1), (void*)value);
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC, ___info_2)); }
	inline IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * get_info_2() const { return ___info_2; }
	inline IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 ** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___info_2), (void*)value);
	}
};


// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	RuntimeObject * ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___current_2)); }
	inline RuntimeObject * get_current_2() const { return ___current_2; }
	inline RuntimeObject ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(RuntimeObject * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_2), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6_StaticFields, ____emptyArray_5)); }
	inline KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyValuePair_2U5BU5D_t8D5FACE7F1DCBAAE2A952321D653C75FEE777FAD* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>
struct List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B, ____items_1)); }
	inline InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD* get__items_1() const { return ____items_1; }
	inline InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B_StaticFields, ____emptyArray_5)); }
	inline InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD* get__emptyArray_5() const { return ____emptyArray_5; }
	inline InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(InternedStringU5BU5D_t3459ADC2129EA8E4FC276351C72461F983ACEABD* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1, ____items_1)); }
	inline NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09* get__items_1() const { return ____items_1; }
	inline NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1_StaticFields, ____emptyArray_5)); }
	inline NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NameAndParametersU5BU5D_tF09FB028C00DC20AA023D6176724A8963ABA2A09* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937, ____items_1)); }
	inline NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* get__items_1() const { return ____items_1; }
	inline NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937_StaticFields, ____emptyArray_5)); }
	inline NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>
struct List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776, ____items_1)); }
	inline SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1* get__items_1() const { return ____items_1; }
	inline SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776_StaticFields, ____emptyArray_5)); }
	inline SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SubstringU5BU5D_t89755ADD55DB4C854BC0C1A369D27FFD53893DE1* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2, ____items_1)); }
	inline JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB* get__items_1() const { return ____items_1; }
	inline JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2_StaticFields, ____emptyArray_5)); }
	inline JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB* get__emptyArray_5() const { return ____emptyArray_5; }
	inline JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(JsonValueU5BU5D_t5810C029BAEB05A32E230A2C5AED1D6DEA1D88DB* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Threading.Tasks.TaskCompletionSource`1<System.Int32Enum>
struct TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354  : public RuntimeObject
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskCompletionSource`1::m_task
	Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354, ___m_task_0)); }
	inline Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}
};


// System.Threading.Tasks.TaskCompletionSource`1<System.Object>
struct TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903  : public RuntimeObject
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskCompletionSource`1::m_task
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903, ___m_task_0)); }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// WinRT.Interop.IAsyncInfo
struct IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9  : public RuntimeObject
{
public:
	// WinRT.ObjectReference`1<WinRT.Interop.IAsyncInfo/Vftbl> WinRT.Interop.IAsyncInfo::_obj
	ObjectReference_1_t41A1DD8B8CAFFA34FCBA13CED053F85524BF0A2B * ____obj_0;

public:
	inline static int32_t get_offset_of__obj_0() { return static_cast<int32_t>(offsetof(IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9, ____obj_0)); }
	inline ObjectReference_1_t41A1DD8B8CAFFA34FCBA13CED053F85524BF0A2B * get__obj_0() const { return ____obj_0; }
	inline ObjectReference_1_t41A1DD8B8CAFFA34FCBA13CED053F85524BF0A2B ** get_address_of__obj_0() { return &____obj_0; }
	inline void set__obj_0(ObjectReference_1_t41A1DD8B8CAFFA34FCBA13CED053F85524BF0A2B * value)
	{
		____obj_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____obj_0), (void*)value);
	}
};


// WinRT.Interop.IAsyncOperation
struct IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85  : public RuntimeObject
{
public:
	// WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl> WinRT.Interop.IAsyncOperation::_obj
	ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * ____obj_0;
	// WinRT.Interop.DelegateProperty`1<WinRT.Interop.IAsyncOperation/CompletedHandler> WinRT.Interop.IAsyncOperation::_Completed
	DelegateProperty_1_t252391BD875C0B5B4BF231D801CE1B88C4E5A9A8 * ____Completed_2;

public:
	inline static int32_t get_offset_of__obj_0() { return static_cast<int32_t>(offsetof(IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85, ____obj_0)); }
	inline ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * get__obj_0() const { return ____obj_0; }
	inline ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A ** get_address_of__obj_0() { return &____obj_0; }
	inline void set__obj_0(ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * value)
	{
		____obj_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____obj_0), (void*)value);
	}

	inline static int32_t get_offset_of__Completed_2() { return static_cast<int32_t>(offsetof(IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85, ____Completed_2)); }
	inline DelegateProperty_1_t252391BD875C0B5B4BF231D801CE1B88C4E5A9A8 * get__Completed_2() const { return ____Completed_2; }
	inline DelegateProperty_1_t252391BD875C0B5B4BF231D801CE1B88C4E5A9A8 ** get_address_of__Completed_2() { return &____Completed_2; }
	inline void set__Completed_2(DelegateProperty_1_t252391BD875C0B5B4BF231D801CE1B88C4E5A9A8 * value)
	{
		____Completed_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____Completed_2), (void*)value);
	}
};

struct IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85_StaticFields
{
public:
	// WinRT.Interop.IDelegate2_Obj_Enum WinRT.Interop.IAsyncOperation::_OnCompletedDelegate
	IDelegate2_Obj_Enum_t1B6381C6004ABDE20A651D52B90592E07CD9FF1E * ____OnCompletedDelegate_1;

public:
	inline static int32_t get_offset_of__OnCompletedDelegate_1() { return static_cast<int32_t>(offsetof(IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85_StaticFields, ____OnCompletedDelegate_1)); }
	inline IDelegate2_Obj_Enum_t1B6381C6004ABDE20A651D52B90592E07CD9FF1E * get__OnCompletedDelegate_1() const { return ____OnCompletedDelegate_1; }
	inline IDelegate2_Obj_Enum_t1B6381C6004ABDE20A651D52B90592E07CD9FF1E ** get_address_of__OnCompletedDelegate_1() { return &____OnCompletedDelegate_1; }
	inline void set__OnCompletedDelegate_1(IDelegate2_Obj_Enum_t1B6381C6004ABDE20A651D52B90592E07CD9FF1E * value)
	{
		____OnCompletedDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnCompletedDelegate_1), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3, ___m_Array_0)); }
	inline NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* get_m_Array_0() const { return ___m_Array_0; }
	inline NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NamedValueU5BU5D_t83EA79812217F6947D1DE2277639F6BDF693B87A* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// System.Threading.SparselyPopulatedArrayAddInfo`1<System.Threading.CancellationCallbackInfo>
struct SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0 
{
public:
	// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArrayAddInfo`1::m_source
	SparselyPopulatedArrayFragment_1_t93197EF47D6A025755987003D5D62F3AED371C21 * ___m_source_0;
	// System.Int32 System.Threading.SparselyPopulatedArrayAddInfo`1::m_index
	int32_t ___m_index_1;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0, ___m_source_0)); }
	inline SparselyPopulatedArrayFragment_1_t93197EF47D6A025755987003D5D62F3AED371C21 * get_m_source_0() const { return ___m_source_0; }
	inline SparselyPopulatedArrayFragment_1_t93197EF47D6A025755987003D5D62F3AED371C21 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(SparselyPopulatedArrayFragment_1_t93197EF47D6A025755987003D5D62F3AED371C21 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_source_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_index_1() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0, ___m_index_1)); }
	inline int32_t get_m_index_1() const { return ___m_index_1; }
	inline int32_t* get_address_of_m_index_1() { return &___m_index_1; }
	inline void set_m_index_1(int32_t value)
	{
		___m_index_1 = value;
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>
struct WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B, ___predicate_4)); }
	inline Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B, ___selector_5)); }
	inline Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C, ___predicate_4)); }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C, ___selector_5)); }
	inline Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1, ___predicate_4)); }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t578287205B88E28024F821D907D73582B53344F1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1, ___selector_5)); }
	inline Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208, ___predicate_4)); }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208, ___selector_5)); }
	inline Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>
struct WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA, ___predicate_4)); }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA, ___selector_5)); }
	inline Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A, ___predicate_4)); }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A, ___selector_5)); }
	inline Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// WinRT.Interop._IAsyncOperation`2<System.Object,System.Int32Enum>
struct _IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42  : public IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85
{
public:

public:
};


// WinRT.Interop._IAsyncOperation`2<System.Object,System.Object>
struct _IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0  : public IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85
{
public:

public:
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::m_source
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD, ___m_source_0)); }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * get_m_source_0() const { return ___m_source_0; }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_source_0), (void*)value);
	}
};

struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_ActionToActionObjShunt
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_ActionToActionObjShunt_1;

public:
	inline static int32_t get_offset_of_s_ActionToActionObjShunt_1() { return static_cast<int32_t>(offsetof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_StaticFields, ___s_ActionToActionObjShunt_1)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_ActionToActionObjShunt_1() const { return ___s_ActionToActionObjShunt_1; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_ActionToActionObjShunt_1() { return &___s_ActionToActionObjShunt_1; }
	inline void set_s_ActionToActionObjShunt_1(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_ActionToActionObjShunt_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ActionToActionObjShunt_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_marshaled_pinvoke
{
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_marshaled_com
{
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;
};

// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Guid
struct Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// WinRT.Interop.IUnknownVftbl
struct IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486 
{
public:
	static const Il2CppGuid CLSID;

public:
	// WinRT.Interop.IUnknownVftbl/_QueryInterface WinRT.Interop.IUnknownVftbl::QueryInterface
	_QueryInterface_t095A9E6C5C1A299ED61A65C48DA4A1E05FEBD861 * ___QueryInterface_0;
	// WinRT.Interop.IUnknownVftbl/_AddRef WinRT.Interop.IUnknownVftbl::AddRef
	_AddRef_t2BAF9ED563D7BE7BBAA3ECB1E966712BC891E4F6 * ___AddRef_1;
	// WinRT.Interop.IUnknownVftbl/_Release WinRT.Interop.IUnknownVftbl::Release
	_Release_tAEF27E0E04B097DCD22D6E226BA893BA116B99CB * ___Release_2;

public:
	inline static int32_t get_offset_of_QueryInterface_0() { return static_cast<int32_t>(offsetof(IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486, ___QueryInterface_0)); }
	inline _QueryInterface_t095A9E6C5C1A299ED61A65C48DA4A1E05FEBD861 * get_QueryInterface_0() const { return ___QueryInterface_0; }
	inline _QueryInterface_t095A9E6C5C1A299ED61A65C48DA4A1E05FEBD861 ** get_address_of_QueryInterface_0() { return &___QueryInterface_0; }
	inline void set_QueryInterface_0(_QueryInterface_t095A9E6C5C1A299ED61A65C48DA4A1E05FEBD861 * value)
	{
		___QueryInterface_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___QueryInterface_0), (void*)value);
	}

	inline static int32_t get_offset_of_AddRef_1() { return static_cast<int32_t>(offsetof(IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486, ___AddRef_1)); }
	inline _AddRef_t2BAF9ED563D7BE7BBAA3ECB1E966712BC891E4F6 * get_AddRef_1() const { return ___AddRef_1; }
	inline _AddRef_t2BAF9ED563D7BE7BBAA3ECB1E966712BC891E4F6 ** get_address_of_AddRef_1() { return &___AddRef_1; }
	inline void set_AddRef_1(_AddRef_t2BAF9ED563D7BE7BBAA3ECB1E966712BC891E4F6 * value)
	{
		___AddRef_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AddRef_1), (void*)value);
	}

	inline static int32_t get_offset_of_Release_2() { return static_cast<int32_t>(offsetof(IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486, ___Release_2)); }
	inline _Release_tAEF27E0E04B097DCD22D6E226BA893BA116B99CB * get_Release_2() const { return ___Release_2; }
	inline _Release_tAEF27E0E04B097DCD22D6E226BA893BA116B99CB ** get_address_of_Release_2() { return &___Release_2; }
	inline void set_Release_2(_Release_tAEF27E0E04B097DCD22D6E226BA893BA116B99CB * value)
	{
		___Release_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Release_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of WinRT.Interop.IUnknownVftbl
struct IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486_marshaled_pinvoke
{
	Il2CppMethodPointer ___QueryInterface_0;
	Il2CppMethodPointer ___AddRef_1;
	Il2CppMethodPointer ___Release_2;
};
// Native definition for COM marshalling of WinRT.Interop.IUnknownVftbl
struct IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486_marshaled_com
{
	Il2CppMethodPointer ___QueryInterface_0;
	Il2CppMethodPointer ___AddRef_1;
	Il2CppMethodPointer ___Release_2;
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// UnityEngine.InputSystem.Utilities.Substring
struct Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.Substring::m_String
	String_t* ___m_String_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.Substring::m_Index
	int32_t ___m_Index_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.Substring::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_String_0() { return static_cast<int32_t>(offsetof(Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02, ___m_String_0)); }
	inline String_t* get_m_String_0() const { return ___m_String_0; }
	inline String_t** get_address_of_m_String_0() { return &___m_String_0; }
	inline void set_m_String_0(String_t* value)
	{
		___m_String_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_String_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.Substring
struct Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02_marshaled_pinvoke
{
	char* ___m_String_0;
	int32_t ___m_Index_1;
	int32_t ___m_Length_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.Substring
struct Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02_marshaled_com
{
	Il2CppChar* ___m_String_0;
	int32_t ___m_Index_1;
	int32_t ___m_Length_2;
};

// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>
struct Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163, ___list_0)); }
	inline List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * get_list_0() const { return ___list_0; }
	inline List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163, ___current_3)); }
	inline InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  get_current_3() const { return ___current_3; }
	inline InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>
struct Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3, ___list_0)); }
	inline List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * get_list_0() const { return ___list_0; }
	inline List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3, ___current_3)); }
	inline Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  get_current_3() const { return ___current_3; }
	inline Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_String_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507, ___current_2)); }
	inline InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  get_current_2() const { return ___current_2; }
	inline InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// WinRT.AsyncStatus
struct AsyncStatus_t5FF4D2BE1F46AB6DD474FC3578D172933CCBA52D 
{
public:
	// System.Int32 WinRT.AsyncStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsyncStatus_t5FF4D2BE1F46AB6DD474FC3578D172933CCBA52D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_t407059AA0E00ABE74F43C533E7D035C4BA451F6A 
{
public:
	// System.Threading.CancellationCallbackInfo System.Threading.CancellationTokenRegistration::m_callbackInfo
	CancellationCallbackInfo_t7FC8CF6DB4845FCB0138771E86AE058710B1117B * ___m_callbackInfo_0;
	// System.Threading.SparselyPopulatedArrayAddInfo`1<System.Threading.CancellationCallbackInfo> System.Threading.CancellationTokenRegistration::m_registrationInfo
	SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0  ___m_registrationInfo_1;

public:
	inline static int32_t get_offset_of_m_callbackInfo_0() { return static_cast<int32_t>(offsetof(CancellationTokenRegistration_t407059AA0E00ABE74F43C533E7D035C4BA451F6A, ___m_callbackInfo_0)); }
	inline CancellationCallbackInfo_t7FC8CF6DB4845FCB0138771E86AE058710B1117B * get_m_callbackInfo_0() const { return ___m_callbackInfo_0; }
	inline CancellationCallbackInfo_t7FC8CF6DB4845FCB0138771E86AE058710B1117B ** get_address_of_m_callbackInfo_0() { return &___m_callbackInfo_0; }
	inline void set_m_callbackInfo_0(CancellationCallbackInfo_t7FC8CF6DB4845FCB0138771E86AE058710B1117B * value)
	{
		___m_callbackInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_callbackInfo_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_registrationInfo_1() { return static_cast<int32_t>(offsetof(CancellationTokenRegistration_t407059AA0E00ABE74F43C533E7D035C4BA451F6A, ___m_registrationInfo_1)); }
	inline SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0  get_m_registrationInfo_1() const { return ___m_registrationInfo_1; }
	inline SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0 * get_address_of_m_registrationInfo_1() { return &___m_registrationInfo_1; }
	inline void set_m_registrationInfo_1(SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0  value)
	{
		___m_registrationInfo_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_registrationInfo_1))->___m_source_0), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_t407059AA0E00ABE74F43C533E7D035C4BA451F6A_marshaled_pinvoke
{
	CancellationCallbackInfo_t7FC8CF6DB4845FCB0138771E86AE058710B1117B * ___m_callbackInfo_0;
	SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0  ___m_registrationInfo_1;
};
// Native definition for COM marshalling of System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_t407059AA0E00ABE74F43C533E7D035C4BA451F6A_marshaled_com
{
	CancellationCallbackInfo_t7FC8CF6DB4845FCB0138771E86AE058710B1117B * ___m_callbackInfo_0;
	SparselyPopulatedArrayAddInfo_1_t6EE25E0D720E03DE7A660791DB554CADCD247FC0  ___m_registrationInfo_1;
};

// Cone
struct Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002 
{
public:
	// UnityEngine.Vector3 Cone::lever_arm
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lever_arm_0;
	// UnityEngine.Vector3 Cone::x_1
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___x_1_1;
	// UnityEngine.Vector3 Cone::x_2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___x_2_2;
	// UnityEngine.Vector3 Cone::x_1_uncertainty
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___x_1_uncertainty_3;
	// UnityEngine.Vector3 Cone::x_2_uncertainty
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___x_2_uncertainty_4;
	// System.Single Cone::alpha
	float ___alpha_5;
	// System.Single Cone::alpha_uncertainty
	float ___alpha_uncertainty_6;
	// System.Single Cone::energy
	float ___energy_7;
	// System.UInt32 Cone::gamma
	uint32_t ___gamma_8;

public:
	inline static int32_t get_offset_of_lever_arm_0() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___lever_arm_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lever_arm_0() const { return ___lever_arm_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lever_arm_0() { return &___lever_arm_0; }
	inline void set_lever_arm_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lever_arm_0 = value;
	}

	inline static int32_t get_offset_of_x_1_1() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___x_1_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_x_1_1() const { return ___x_1_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_x_1_1() { return &___x_1_1; }
	inline void set_x_1_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___x_1_1 = value;
	}

	inline static int32_t get_offset_of_x_2_2() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___x_2_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_x_2_2() const { return ___x_2_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_x_2_2() { return &___x_2_2; }
	inline void set_x_2_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___x_2_2 = value;
	}

	inline static int32_t get_offset_of_x_1_uncertainty_3() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___x_1_uncertainty_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_x_1_uncertainty_3() const { return ___x_1_uncertainty_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_x_1_uncertainty_3() { return &___x_1_uncertainty_3; }
	inline void set_x_1_uncertainty_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___x_1_uncertainty_3 = value;
	}

	inline static int32_t get_offset_of_x_2_uncertainty_4() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___x_2_uncertainty_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_x_2_uncertainty_4() const { return ___x_2_uncertainty_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_x_2_uncertainty_4() { return &___x_2_uncertainty_4; }
	inline void set_x_2_uncertainty_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___x_2_uncertainty_4 = value;
	}

	inline static int32_t get_offset_of_alpha_5() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___alpha_5)); }
	inline float get_alpha_5() const { return ___alpha_5; }
	inline float* get_address_of_alpha_5() { return &___alpha_5; }
	inline void set_alpha_5(float value)
	{
		___alpha_5 = value;
	}

	inline static int32_t get_offset_of_alpha_uncertainty_6() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___alpha_uncertainty_6)); }
	inline float get_alpha_uncertainty_6() const { return ___alpha_uncertainty_6; }
	inline float* get_address_of_alpha_uncertainty_6() { return &___alpha_uncertainty_6; }
	inline void set_alpha_uncertainty_6(float value)
	{
		___alpha_uncertainty_6 = value;
	}

	inline static int32_t get_offset_of_energy_7() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___energy_7)); }
	inline float get_energy_7() const { return ___energy_7; }
	inline float* get_address_of_energy_7() { return &___energy_7; }
	inline void set_energy_7(float value)
	{
		___energy_7 = value;
	}

	inline static int32_t get_offset_of_gamma_8() { return static_cast<int32_t>(offsetof(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002, ___gamma_8)); }
	inline uint32_t get_gamma_8() const { return ___gamma_8; }
	inline uint32_t* get_address_of_gamma_8() { return &___gamma_8; }
	inline void set_gamma_8(uint32_t value)
	{
		___gamma_8 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// WinRT.Interop.IInspectableVftbl
struct IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D 
{
public:
	static const Il2CppGuid CLSID;

public:
	// WinRT.Interop.IUnknownVftbl WinRT.Interop.IInspectableVftbl::IUnknownVftbl
	IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  ___IUnknownVftbl_0;
	// WinRT.Interop.IInspectableVftbl/_GetIids WinRT.Interop.IInspectableVftbl::GetIids
	_GetIids_tDC148CE7B602F998B5A9C63C47FD75466894A52F * ___GetIids_1;
	// WinRT.Interop.IInspectableVftbl/_GetRuntimeClassName WinRT.Interop.IInspectableVftbl::GetRuntimeClassName
	_GetRuntimeClassName_t1FD45F08090CED02A6328E976D1B846CBDB7133C * ___GetRuntimeClassName_2;
	// WinRT.Interop.IInspectableVftbl/_GetTrustLevel WinRT.Interop.IInspectableVftbl::GetTrustLevel
	_GetTrustLevel_t41789FA2A2D2CC0D11B6128565B223154D91C803 * ___GetTrustLevel_3;

public:
	inline static int32_t get_offset_of_IUnknownVftbl_0() { return static_cast<int32_t>(offsetof(IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D, ___IUnknownVftbl_0)); }
	inline IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  get_IUnknownVftbl_0() const { return ___IUnknownVftbl_0; }
	inline IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486 * get_address_of_IUnknownVftbl_0() { return &___IUnknownVftbl_0; }
	inline void set_IUnknownVftbl_0(IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  value)
	{
		___IUnknownVftbl_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___IUnknownVftbl_0))->___QueryInterface_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___IUnknownVftbl_0))->___AddRef_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___IUnknownVftbl_0))->___Release_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_GetIids_1() { return static_cast<int32_t>(offsetof(IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D, ___GetIids_1)); }
	inline _GetIids_tDC148CE7B602F998B5A9C63C47FD75466894A52F * get_GetIids_1() const { return ___GetIids_1; }
	inline _GetIids_tDC148CE7B602F998B5A9C63C47FD75466894A52F ** get_address_of_GetIids_1() { return &___GetIids_1; }
	inline void set_GetIids_1(_GetIids_tDC148CE7B602F998B5A9C63C47FD75466894A52F * value)
	{
		___GetIids_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GetIids_1), (void*)value);
	}

	inline static int32_t get_offset_of_GetRuntimeClassName_2() { return static_cast<int32_t>(offsetof(IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D, ___GetRuntimeClassName_2)); }
	inline _GetRuntimeClassName_t1FD45F08090CED02A6328E976D1B846CBDB7133C * get_GetRuntimeClassName_2() const { return ___GetRuntimeClassName_2; }
	inline _GetRuntimeClassName_t1FD45F08090CED02A6328E976D1B846CBDB7133C ** get_address_of_GetRuntimeClassName_2() { return &___GetRuntimeClassName_2; }
	inline void set_GetRuntimeClassName_2(_GetRuntimeClassName_t1FD45F08090CED02A6328E976D1B846CBDB7133C * value)
	{
		___GetRuntimeClassName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GetRuntimeClassName_2), (void*)value);
	}

	inline static int32_t get_offset_of_GetTrustLevel_3() { return static_cast<int32_t>(offsetof(IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D, ___GetTrustLevel_3)); }
	inline _GetTrustLevel_t41789FA2A2D2CC0D11B6128565B223154D91C803 * get_GetTrustLevel_3() const { return ___GetTrustLevel_3; }
	inline _GetTrustLevel_t41789FA2A2D2CC0D11B6128565B223154D91C803 ** get_address_of_GetTrustLevel_3() { return &___GetTrustLevel_3; }
	inline void set_GetTrustLevel_3(_GetTrustLevel_t41789FA2A2D2CC0D11B6128565B223154D91C803 * value)
	{
		___GetTrustLevel_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GetTrustLevel_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of WinRT.Interop.IInspectableVftbl
struct IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D_marshaled_pinvoke
{
	IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486_marshaled_pinvoke ___IUnknownVftbl_0;
	Il2CppMethodPointer ___GetIids_1;
	Il2CppMethodPointer ___GetRuntimeClassName_2;
	Il2CppMethodPointer ___GetTrustLevel_3;
};
// Native definition for COM marshalling of WinRT.Interop.IInspectableVftbl
struct IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D_marshaled_com
{
	IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486_marshaled_com ___IUnknownVftbl_0;
	Il2CppMethodPointer ___GetIids_1;
	Il2CppMethodPointer ___GetRuntimeClassName_2;
	Il2CppMethodPointer ___GetTrustLevel_3;
};

// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// WinRT.ModuleReference
struct ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1 
{
public:
	// System.Object WinRT.ModuleReference::_module
	RuntimeObject * ____module_0;
	// System.Int32* WinRT.ModuleReference::_refCountPtr
	int32_t* ____refCountPtr_1;
	// System.IntPtr WinRT.ModuleReference::_nativeHandle
	intptr_t ____nativeHandle_2;

public:
	inline static int32_t get_offset_of__module_0() { return static_cast<int32_t>(offsetof(ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1, ____module_0)); }
	inline RuntimeObject * get__module_0() const { return ____module_0; }
	inline RuntimeObject ** get_address_of__module_0() { return &____module_0; }
	inline void set__module_0(RuntimeObject * value)
	{
		____module_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____module_0), (void*)value);
	}

	inline static int32_t get_offset_of__refCountPtr_1() { return static_cast<int32_t>(offsetof(ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1, ____refCountPtr_1)); }
	inline int32_t* get__refCountPtr_1() const { return ____refCountPtr_1; }
	inline int32_t** get_address_of__refCountPtr_1() { return &____refCountPtr_1; }
	inline void set__refCountPtr_1(int32_t* value)
	{
		____refCountPtr_1 = value;
	}

	inline static int32_t get_offset_of__nativeHandle_2() { return static_cast<int32_t>(offsetof(ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1, ____nativeHandle_2)); }
	inline intptr_t get__nativeHandle_2() const { return ____nativeHandle_2; }
	inline intptr_t* get_address_of__nativeHandle_2() { return &____nativeHandle_2; }
	inline void set__nativeHandle_2(intptr_t value)
	{
		____nativeHandle_2 = value;
	}
};

// Native definition for P/Invoke marshalling of WinRT.ModuleReference
struct ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1_marshaled_pinvoke
{
	Il2CppIUnknown* ____module_0;
	int32_t* ____refCountPtr_1;
	intptr_t ____nativeHandle_2;
};
// Native definition for COM marshalling of WinRT.ModuleReference
struct ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1_marshaled_com
{
	Il2CppIUnknown* ____module_0;
	int32_t* ____refCountPtr_1;
	intptr_t ____nativeHandle_2;
};

// UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NameAndParameters::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue> UnityEngine.InputSystem.Utilities.NameAndParameters::<parameters>k__BackingField
	ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3  ___U3CparametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97, ___U3CparametersU3Ek__BackingField_1)); }
	inline ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3  get_U3CparametersU3Ek__BackingField_1() const { return ___U3CparametersU3Ek__BackingField_1; }
	inline ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3 * get_address_of_U3CparametersU3Ek__BackingField_1() { return &___U3CparametersU3Ek__BackingField_1; }
	inline void set_U3CparametersU3Ek__BackingField_1(ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3  value)
	{
		___U3CparametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3  ___U3CparametersU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t7362A44A1250AE238B47D6C7C6D4A13EDC5E7DC3  ___U3CparametersU3Ek__BackingField_1;
};

// System.Threading.Tasks.Task
struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	RuntimeObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	RuntimeObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	RuntimeObject * ___m_continuationObject_10;
	// System.Threading.Tasks.Task/ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * ___m_contingentProperties_15;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_action_5)); }
	inline RuntimeObject * get_m_action_5() const { return ___m_action_5; }
	inline RuntimeObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(RuntimeObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_action_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateObject_6)); }
	inline RuntimeObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline RuntimeObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(RuntimeObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskScheduler_7)); }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_taskScheduler_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_parent_8)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parent_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_10() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_continuationObject_10)); }
	inline RuntimeObject * get_m_continuationObject_10() const { return ___m_continuationObject_10; }
	inline RuntimeObject ** get_address_of_m_continuationObject_10() { return &___m_continuationObject_10; }
	inline void set_m_continuationObject_10(RuntimeObject * value)
	{
		___m_continuationObject_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuationObject_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_15() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_contingentProperties_15)); }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * get_m_contingentProperties_15() const { return ___m_contingentProperties_15; }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 ** get_address_of_m_contingentProperties_15() { return &___m_contingentProperties_15; }
	inline void set_m_contingentProperties_15(ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * value)
	{
		___m_contingentProperties_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_contingentProperties_15), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	RuntimeObject * ___s_taskCompletionSentinel_11;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * ___s_currentActiveTasks_13;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	RuntimeObject * ___s_activeTasksLock_14;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_taskCancelCallback_16;
	// System.Func`1<System.Threading.Tasks.Task/ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * ___s_createContingentProperties_17;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___s_completedTask_18;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * ___s_IsExceptionObservedByParentPredicate_19;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * ___s_ecCallback_20;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___s_IsTaskContinuationNullPredicate_21;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_factory_3)); }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_factory_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_11() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCompletionSentinel_11)); }
	inline RuntimeObject * get_s_taskCompletionSentinel_11() const { return ___s_taskCompletionSentinel_11; }
	inline RuntimeObject ** get_address_of_s_taskCompletionSentinel_11() { return &___s_taskCompletionSentinel_11; }
	inline void set_s_taskCompletionSentinel_11(RuntimeObject * value)
	{
		___s_taskCompletionSentinel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCompletionSentinel_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_12() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_asyncDebuggingEnabled_12)); }
	inline bool get_s_asyncDebuggingEnabled_12() const { return ___s_asyncDebuggingEnabled_12; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_12() { return &___s_asyncDebuggingEnabled_12; }
	inline void set_s_asyncDebuggingEnabled_12(bool value)
	{
		___s_asyncDebuggingEnabled_12 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_13() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_currentActiveTasks_13)); }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * get_s_currentActiveTasks_13() const { return ___s_currentActiveTasks_13; }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 ** get_address_of_s_currentActiveTasks_13() { return &___s_currentActiveTasks_13; }
	inline void set_s_currentActiveTasks_13(Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * value)
	{
		___s_currentActiveTasks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_currentActiveTasks_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_14() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_activeTasksLock_14)); }
	inline RuntimeObject * get_s_activeTasksLock_14() const { return ___s_activeTasksLock_14; }
	inline RuntimeObject ** get_address_of_s_activeTasksLock_14() { return &___s_activeTasksLock_14; }
	inline void set_s_activeTasksLock_14(RuntimeObject * value)
	{
		___s_activeTasksLock_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_activeTasksLock_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_16() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCancelCallback_16)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_taskCancelCallback_16() const { return ___s_taskCancelCallback_16; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_taskCancelCallback_16() { return &___s_taskCancelCallback_16; }
	inline void set_s_taskCancelCallback_16(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_taskCancelCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCancelCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_17() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_createContingentProperties_17)); }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * get_s_createContingentProperties_17() const { return ___s_createContingentProperties_17; }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B ** get_address_of_s_createContingentProperties_17() { return &___s_createContingentProperties_17; }
	inline void set_s_createContingentProperties_17(Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * value)
	{
		___s_createContingentProperties_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_createContingentProperties_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_completedTask_18() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_completedTask_18)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_s_completedTask_18() const { return ___s_completedTask_18; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_s_completedTask_18() { return &___s_completedTask_18; }
	inline void set_s_completedTask_18(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___s_completedTask_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_completedTask_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_19() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsExceptionObservedByParentPredicate_19)); }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * get_s_IsExceptionObservedByParentPredicate_19() const { return ___s_IsExceptionObservedByParentPredicate_19; }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD ** get_address_of_s_IsExceptionObservedByParentPredicate_19() { return &___s_IsExceptionObservedByParentPredicate_19; }
	inline void set_s_IsExceptionObservedByParentPredicate_19(Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * value)
	{
		___s_IsExceptionObservedByParentPredicate_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsExceptionObservedByParentPredicate_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_ecCallback_20() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_ecCallback_20)); }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * get_s_ecCallback_20() const { return ___s_ecCallback_20; }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B ** get_address_of_s_ecCallback_20() { return &___s_ecCallback_20; }
	inline void set_s_ecCallback_20(ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * value)
	{
		___s_ecCallback_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ecCallback_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_21() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsTaskContinuationNullPredicate_21)); }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * get_s_IsTaskContinuationNullPredicate_21() const { return ___s_IsTaskContinuationNullPredicate_21; }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB ** get_address_of_s_IsTaskContinuationNullPredicate_21() { return &___s_IsTaskContinuationNullPredicate_21; }
	inline void set_s_IsTaskContinuationNullPredicate_21(Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * value)
	{
		___s_IsTaskContinuationNullPredicate_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsTaskContinuationNullPredicate_21), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_currentTask_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_stackGuard_1), (void*)value);
	}
};


// System.TypeCode
struct TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E 
{
public:
	// UnityEngine.InputSystem.Utilities.Substring UnityEngine.InputSystem.Utilities.JsonParser/JsonString::text
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  ___text_0;
	// System.Boolean UnityEngine.InputSystem.Utilities.JsonParser/JsonString::hasEscapes
	bool ___hasEscapes_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E, ___text_0)); }
	inline Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  get_text_0() const { return ___text_0; }
	inline Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 * get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___text_0))->___m_String_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_hasEscapes_1() { return static_cast<int32_t>(offsetof(JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E, ___hasEscapes_1)); }
	inline bool get_hasEscapes_1() const { return ___hasEscapes_1; }
	inline bool* get_address_of_hasEscapes_1() { return &___hasEscapes_1; }
	inline void set_hasEscapes_1(bool value)
	{
		___hasEscapes_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E_marshaled_pinvoke
{
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02_marshaled_pinvoke ___text_0;
	int32_t ___hasEscapes_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E_marshaled_com
{
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02_marshaled_com ___text_0;
	int32_t ___hasEscapes_1;
};

// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType
struct JsonValueType_t8AE335508103709430FFE1B9813ACA224E55AF4D 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonValueType_t8AE335508103709430FFE1B9813ACA224E55AF4D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695, ___list_0)); }
	inline List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * get_list_0() const { return ___list_0; }
	inline List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695, ___current_3)); }
	inline NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  get_current_3() const { return ___current_3; }
	inline NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/Iterator`1<Cone>
struct Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E, ___current_2)); }
	inline Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  get_current_2() const { return ___current_2; }
	inline Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002 * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  value)
	{
		___current_2 = value;
	}
};


// System.Threading.Tasks.Task`1<System.Int32Enum>
struct Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58  : public Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	int32_t ___m_result_22;

public:
	inline static int32_t get_offset_of_m_result_22() { return static_cast<int32_t>(offsetof(Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58, ___m_result_22)); }
	inline int32_t get_m_result_22() const { return ___m_result_22; }
	inline int32_t* get_address_of_m_result_22() { return &___m_result_22; }
	inline void set_m_result_22(int32_t value)
	{
		___m_result_22 = value;
	}
};

struct Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_tB258CD0C610C65E36B1AC1470D310CC8290BDA75 * ___s_Factory_23;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_tED7578086478CDE88DBC2FFFDCB01FEA9F167392 * ___TaskWhenAnyCast_24;

public:
	inline static int32_t get_offset_of_s_Factory_23() { return static_cast<int32_t>(offsetof(Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58_StaticFields, ___s_Factory_23)); }
	inline TaskFactory_1_tB258CD0C610C65E36B1AC1470D310CC8290BDA75 * get_s_Factory_23() const { return ___s_Factory_23; }
	inline TaskFactory_1_tB258CD0C610C65E36B1AC1470D310CC8290BDA75 ** get_address_of_s_Factory_23() { return &___s_Factory_23; }
	inline void set_s_Factory_23(TaskFactory_1_tB258CD0C610C65E36B1AC1470D310CC8290BDA75 * value)
	{
		___s_Factory_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_23), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_24() { return static_cast<int32_t>(offsetof(Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58_StaticFields, ___TaskWhenAnyCast_24)); }
	inline Func_2_tED7578086478CDE88DBC2FFFDCB01FEA9F167392 * get_TaskWhenAnyCast_24() const { return ___TaskWhenAnyCast_24; }
	inline Func_2_tED7578086478CDE88DBC2FFFDCB01FEA9F167392 ** get_address_of_TaskWhenAnyCast_24() { return &___TaskWhenAnyCast_24; }
	inline void set_TaskWhenAnyCast_24(Func_2_tED7578086478CDE88DBC2FFFDCB01FEA9F167392 * value)
	{
		___TaskWhenAnyCast_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_24), (void*)value);
	}
};


// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17  : public Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	RuntimeObject * ___m_result_22;

public:
	inline static int32_t get_offset_of_m_result_22() { return static_cast<int32_t>(offsetof(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17, ___m_result_22)); }
	inline RuntimeObject * get_m_result_22() const { return ___m_result_22; }
	inline RuntimeObject ** get_address_of_m_result_22() { return &___m_result_22; }
	inline void set_m_result_22(RuntimeObject * value)
	{
		___m_result_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_22), (void*)value);
	}
};

struct Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55 * ___s_Factory_23;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD * ___TaskWhenAnyCast_24;

public:
	inline static int32_t get_offset_of_s_Factory_23() { return static_cast<int32_t>(offsetof(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17_StaticFields, ___s_Factory_23)); }
	inline TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55 * get_s_Factory_23() const { return ___s_Factory_23; }
	inline TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55 ** get_address_of_s_Factory_23() { return &___s_Factory_23; }
	inline void set_s_Factory_23(TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55 * value)
	{
		___s_Factory_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_23), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_24() { return static_cast<int32_t>(offsetof(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17_StaticFields, ___TaskWhenAnyCast_24)); }
	inline Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD * get_TaskWhenAnyCast_24() const { return ___TaskWhenAnyCast_24; }
	inline Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD ** get_address_of_TaskWhenAnyCast_24() { return &___TaskWhenAnyCast_24; }
	inline void set_TaskWhenAnyCast_24(Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD * value)
	{
		___TaskWhenAnyCast_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_24), (void*)value);
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D, ___predicate_4)); }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80, ___predicate_4)); }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80, ___selector_5)); }
	inline Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * get_selector_5() const { return ___selector_5; }
	inline Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109, ___predicate_4)); }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t578287205B88E28024F821D907D73582B53344F1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109, ___selector_5)); }
	inline Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F, ___predicate_4)); }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F, ___selector_5)); }
	inline Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E, ___selector_5)); }
	inline Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955, ___predicate_4)); }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955, ___selector_5)); }
	inline Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB, ___predicate_4)); }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB, ___selector_5)); }
	inline Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1, ___source_3)); }
	inline List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * get_source_3() const { return ___source_3; }
	inline List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1, ___predicate_4)); }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1, ___selector_5)); }
	inline Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * get_selector_5() const { return ___selector_5; }
	inline Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1, ___enumerator_6)); }
	inline Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F, ___source_3)); }
	inline List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * get_source_3() const { return ___source_3; }
	inline List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F, ___predicate_4)); }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F, ___selector_5)); }
	inline Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F, ___enumerator_6)); }
	inline Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B, ___selector_5)); }
	inline Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106, ___source_3)); }
	inline List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * get_source_3() const { return ___source_3; }
	inline List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106, ___predicate_4)); }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106, ___selector_5)); }
	inline Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106, ___enumerator_6)); }
	inline Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78, ___source_3)); }
	inline List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * get_source_3() const { return ___source_3; }
	inline List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78, ___predicate_4)); }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78, ___selector_5)); }
	inline Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78, ___enumerator_6)); }
	inline Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// WinRT.IObjectReference
struct IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444  : public RuntimeObject
{
public:
	// System.IntPtr WinRT.IObjectReference::ThisPtr
	intptr_t ___ThisPtr_0;
	// WinRT.ModuleReference WinRT.IObjectReference::Module
	ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1  ___Module_1;
	// WinRT.Interop.IUnknownVftbl WinRT.IObjectReference::<VftblIUnknown>k__BackingField
	IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  ___U3CVftblIUnknownU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_ThisPtr_0() { return static_cast<int32_t>(offsetof(IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444, ___ThisPtr_0)); }
	inline intptr_t get_ThisPtr_0() const { return ___ThisPtr_0; }
	inline intptr_t* get_address_of_ThisPtr_0() { return &___ThisPtr_0; }
	inline void set_ThisPtr_0(intptr_t value)
	{
		___ThisPtr_0 = value;
	}

	inline static int32_t get_offset_of_Module_1() { return static_cast<int32_t>(offsetof(IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444, ___Module_1)); }
	inline ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1  get_Module_1() const { return ___Module_1; }
	inline ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1 * get_address_of_Module_1() { return &___Module_1; }
	inline void set_Module_1(ModuleReference_tF80DAAFAF8035E9963647193E347618FE323FFF1  value)
	{
		___Module_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___Module_1))->____module_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CVftblIUnknownU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444, ___U3CVftblIUnknownU3Ek__BackingField_2)); }
	inline IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  get_U3CVftblIUnknownU3Ek__BackingField_2() const { return ___U3CVftblIUnknownU3Ek__BackingField_2; }
	inline IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486 * get_address_of_U3CVftblIUnknownU3Ek__BackingField_2() { return &___U3CVftblIUnknownU3Ek__BackingField_2; }
	inline void set_U3CVftblIUnknownU3Ek__BackingField_2(IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  value)
	{
		___U3CVftblIUnknownU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CVftblIUnknownU3Ek__BackingField_2))->___QueryInterface_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CVftblIUnknownU3Ek__BackingField_2))->___AddRef_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CVftblIUnknownU3Ek__BackingField_2))->___Release_2), (void*)NULL);
		#endif
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.TypeCode UnityEngine.InputSystem.Utilities.PrimitiveValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			// System.Boolean UnityEngine.InputSystem.Utilities.PrimitiveValue::m_BoolValue
			bool ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			bool ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			// System.Char UnityEngine.InputSystem.Utilities.PrimitiveValue::m_CharValue
			Il2CppChar ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			Il2CppChar ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			// System.Byte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ByteValue
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			// System.SByte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_SByteValue
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			// System.Int16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ShortValue
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			// System.UInt16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UShortValue
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			// System.Int32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_IntValue
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			// System.UInt32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UIntValue
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			// System.Int64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_LongValue
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			// System.UInt64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ULongValue
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			// System.Single UnityEngine.InputSystem.Utilities.PrimitiveValue::m_FloatValue
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			// System.Double UnityEngine.InputSystem.Utilities.PrimitiveValue::m_DoubleValue
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BoolValue_1() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_BoolValue_1)); }
	inline bool get_m_BoolValue_1() const { return ___m_BoolValue_1; }
	inline bool* get_address_of_m_BoolValue_1() { return &___m_BoolValue_1; }
	inline void set_m_BoolValue_1(bool value)
	{
		___m_BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_m_CharValue_2() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_CharValue_2)); }
	inline Il2CppChar get_m_CharValue_2() const { return ___m_CharValue_2; }
	inline Il2CppChar* get_address_of_m_CharValue_2() { return &___m_CharValue_2; }
	inline void set_m_CharValue_2(Il2CppChar value)
	{
		___m_CharValue_2 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_3() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_ByteValue_3)); }
	inline uint8_t get_m_ByteValue_3() const { return ___m_ByteValue_3; }
	inline uint8_t* get_address_of_m_ByteValue_3() { return &___m_ByteValue_3; }
	inline void set_m_ByteValue_3(uint8_t value)
	{
		___m_ByteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_SByteValue_4() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_SByteValue_4)); }
	inline int8_t get_m_SByteValue_4() const { return ___m_SByteValue_4; }
	inline int8_t* get_address_of_m_SByteValue_4() { return &___m_SByteValue_4; }
	inline void set_m_SByteValue_4(int8_t value)
	{
		___m_SByteValue_4 = value;
	}

	inline static int32_t get_offset_of_m_ShortValue_5() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_ShortValue_5)); }
	inline int16_t get_m_ShortValue_5() const { return ___m_ShortValue_5; }
	inline int16_t* get_address_of_m_ShortValue_5() { return &___m_ShortValue_5; }
	inline void set_m_ShortValue_5(int16_t value)
	{
		___m_ShortValue_5 = value;
	}

	inline static int32_t get_offset_of_m_UShortValue_6() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_UShortValue_6)); }
	inline uint16_t get_m_UShortValue_6() const { return ___m_UShortValue_6; }
	inline uint16_t* get_address_of_m_UShortValue_6() { return &___m_UShortValue_6; }
	inline void set_m_UShortValue_6(uint16_t value)
	{
		___m_UShortValue_6 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_7() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_IntValue_7)); }
	inline int32_t get_m_IntValue_7() const { return ___m_IntValue_7; }
	inline int32_t* get_address_of_m_IntValue_7() { return &___m_IntValue_7; }
	inline void set_m_IntValue_7(int32_t value)
	{
		___m_IntValue_7 = value;
	}

	inline static int32_t get_offset_of_m_UIntValue_8() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_UIntValue_8)); }
	inline uint32_t get_m_UIntValue_8() const { return ___m_UIntValue_8; }
	inline uint32_t* get_address_of_m_UIntValue_8() { return &___m_UIntValue_8; }
	inline void set_m_UIntValue_8(uint32_t value)
	{
		___m_UIntValue_8 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_9() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_LongValue_9)); }
	inline int64_t get_m_LongValue_9() const { return ___m_LongValue_9; }
	inline int64_t* get_address_of_m_LongValue_9() { return &___m_LongValue_9; }
	inline void set_m_LongValue_9(int64_t value)
	{
		___m_LongValue_9 = value;
	}

	inline static int32_t get_offset_of_m_ULongValue_10() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_ULongValue_10)); }
	inline uint64_t get_m_ULongValue_10() const { return ___m_ULongValue_10; }
	inline uint64_t* get_address_of_m_ULongValue_10() { return &___m_ULongValue_10; }
	inline void set_m_ULongValue_10(uint64_t value)
	{
		___m_ULongValue_10 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_11() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_FloatValue_11)); }
	inline float get_m_FloatValue_11() const { return ___m_FloatValue_11; }
	inline float* get_address_of_m_FloatValue_11() { return &___m_FloatValue_11; }
	inline void set_m_FloatValue_11(float value)
	{
		___m_FloatValue_11 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_12() { return static_cast<int32_t>(offsetof(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D, ___m_DoubleValue_12)); }
	inline double get_m_DoubleValue_12() const { return ___m_DoubleValue_12; }
	inline double* get_address_of_m_DoubleValue_12() { return &___m_DoubleValue_12; }
	inline void set_m_DoubleValue_12(double value)
	{
		___m_DoubleValue_12 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};

// WinRT.Interop.IAsyncOperation/Vftbl
struct Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328 
{
public:
	// WinRT.Interop.IInspectableVftbl WinRT.Interop.IAsyncOperation/Vftbl::IInspectableVftbl
	IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D  ___IInspectableVftbl_0;
	// WinRT.Interop._put_PropertyAsObject WinRT.Interop.IAsyncOperation/Vftbl::put_Completed
	_put_PropertyAsObject_t65BE7D6576FA6A84A0205925C7420D5CF3239980 * ___put_Completed_1;
	// WinRT.Interop._get_PropertyAsObject WinRT.Interop.IAsyncOperation/Vftbl::get_Completed
	_get_PropertyAsObject_tB6DABCBEE76A0489F547E29DCD7309085C4B32D3 * ___get_Completed_2;
	// System.IntPtr WinRT.Interop.IAsyncOperation/Vftbl::GetResults
	intptr_t ___GetResults_3;

public:
	inline static int32_t get_offset_of_IInspectableVftbl_0() { return static_cast<int32_t>(offsetof(Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328, ___IInspectableVftbl_0)); }
	inline IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D  get_IInspectableVftbl_0() const { return ___IInspectableVftbl_0; }
	inline IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D * get_address_of_IInspectableVftbl_0() { return &___IInspectableVftbl_0; }
	inline void set_IInspectableVftbl_0(IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D  value)
	{
		___IInspectableVftbl_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___IInspectableVftbl_0))->___IUnknownVftbl_0))->___QueryInterface_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___IInspectableVftbl_0))->___IUnknownVftbl_0))->___AddRef_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___IInspectableVftbl_0))->___IUnknownVftbl_0))->___Release_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___IInspectableVftbl_0))->___GetIids_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___IInspectableVftbl_0))->___GetRuntimeClassName_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___IInspectableVftbl_0))->___GetTrustLevel_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_put_Completed_1() { return static_cast<int32_t>(offsetof(Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328, ___put_Completed_1)); }
	inline _put_PropertyAsObject_t65BE7D6576FA6A84A0205925C7420D5CF3239980 * get_put_Completed_1() const { return ___put_Completed_1; }
	inline _put_PropertyAsObject_t65BE7D6576FA6A84A0205925C7420D5CF3239980 ** get_address_of_put_Completed_1() { return &___put_Completed_1; }
	inline void set_put_Completed_1(_put_PropertyAsObject_t65BE7D6576FA6A84A0205925C7420D5CF3239980 * value)
	{
		___put_Completed_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___put_Completed_1), (void*)value);
	}

	inline static int32_t get_offset_of_get_Completed_2() { return static_cast<int32_t>(offsetof(Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328, ___get_Completed_2)); }
	inline _get_PropertyAsObject_tB6DABCBEE76A0489F547E29DCD7309085C4B32D3 * get_get_Completed_2() const { return ___get_Completed_2; }
	inline _get_PropertyAsObject_tB6DABCBEE76A0489F547E29DCD7309085C4B32D3 ** get_address_of_get_Completed_2() { return &___get_Completed_2; }
	inline void set_get_Completed_2(_get_PropertyAsObject_tB6DABCBEE76A0489F547E29DCD7309085C4B32D3 * value)
	{
		___get_Completed_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___get_Completed_2), (void*)value);
	}

	inline static int32_t get_offset_of_GetResults_3() { return static_cast<int32_t>(offsetof(Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328, ___GetResults_3)); }
	inline intptr_t get_GetResults_3() const { return ___GetResults_3; }
	inline intptr_t* get_address_of_GetResults_3() { return &___GetResults_3; }
	inline void set_GetResults_3(intptr_t value)
	{
		___GetResults_3 = value;
	}
};

// Native definition for P/Invoke marshalling of WinRT.Interop.IAsyncOperation/Vftbl
struct Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328_marshaled_pinvoke
{
	IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D_marshaled_pinvoke ___IInspectableVftbl_0;
	Il2CppMethodPointer ___put_Completed_1;
	Il2CppMethodPointer ___get_Completed_2;
	intptr_t ___GetResults_3;
};
// Native definition for COM marshalling of WinRT.Interop.IAsyncOperation/Vftbl
struct Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328_marshaled_com
{
	IInspectableVftbl_t62F0855A38876299E7DA35C07316AD15DFEF6F8D_marshaled_com ___IInspectableVftbl_0;
	Il2CppMethodPointer ___put_Completed_1;
	Il2CppMethodPointer ___get_Completed_2;
	intptr_t ___GetResults_3;
};

// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D 
{
public:
	// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::type
	int32_t ___type_0;
	// System.Boolean UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::boolValue
	bool ___boolValue_1;
	// System.Double UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::realValue
	double ___realValue_2;
	// System.Int64 UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::integerValue
	int64_t ___integerValue_3;
	// UnityEngine.InputSystem.Utilities.JsonParser/JsonString UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::stringValue
	JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E  ___stringValue_4;
	// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue> UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::arrayValue
	List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___arrayValue_5;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue> UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::objectValue
	Dictionary_2_tDE3993BD3BBC5AA17021AEB6C4928B55CF0AA730 * ___objectValue_6;
	// System.Object UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::anyValue
	RuntimeObject * ___anyValue_7;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_boolValue_1() { return static_cast<int32_t>(offsetof(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D, ___boolValue_1)); }
	inline bool get_boolValue_1() const { return ___boolValue_1; }
	inline bool* get_address_of_boolValue_1() { return &___boolValue_1; }
	inline void set_boolValue_1(bool value)
	{
		___boolValue_1 = value;
	}

	inline static int32_t get_offset_of_realValue_2() { return static_cast<int32_t>(offsetof(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D, ___realValue_2)); }
	inline double get_realValue_2() const { return ___realValue_2; }
	inline double* get_address_of_realValue_2() { return &___realValue_2; }
	inline void set_realValue_2(double value)
	{
		___realValue_2 = value;
	}

	inline static int32_t get_offset_of_integerValue_3() { return static_cast<int32_t>(offsetof(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D, ___integerValue_3)); }
	inline int64_t get_integerValue_3() const { return ___integerValue_3; }
	inline int64_t* get_address_of_integerValue_3() { return &___integerValue_3; }
	inline void set_integerValue_3(int64_t value)
	{
		___integerValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D, ___stringValue_4)); }
	inline JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E  get_stringValue_4() const { return ___stringValue_4; }
	inline JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E * get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E  value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_arrayValue_5() { return static_cast<int32_t>(offsetof(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D, ___arrayValue_5)); }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * get_arrayValue_5() const { return ___arrayValue_5; }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 ** get_address_of_arrayValue_5() { return &___arrayValue_5; }
	inline void set_arrayValue_5(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * value)
	{
		___arrayValue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrayValue_5), (void*)value);
	}

	inline static int32_t get_offset_of_objectValue_6() { return static_cast<int32_t>(offsetof(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D, ___objectValue_6)); }
	inline Dictionary_2_tDE3993BD3BBC5AA17021AEB6C4928B55CF0AA730 * get_objectValue_6() const { return ___objectValue_6; }
	inline Dictionary_2_tDE3993BD3BBC5AA17021AEB6C4928B55CF0AA730 ** get_address_of_objectValue_6() { return &___objectValue_6; }
	inline void set_objectValue_6(Dictionary_2_tDE3993BD3BBC5AA17021AEB6C4928B55CF0AA730 * value)
	{
		___objectValue_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectValue_6), (void*)value);
	}

	inline static int32_t get_offset_of_anyValue_7() { return static_cast<int32_t>(offsetof(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D, ___anyValue_7)); }
	inline RuntimeObject * get_anyValue_7() const { return ___anyValue_7; }
	inline RuntimeObject ** get_address_of_anyValue_7() { return &___anyValue_7; }
	inline void set_anyValue_7(RuntimeObject * value)
	{
		___anyValue_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anyValue_7), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D_marshaled_pinvoke
{
	int32_t ___type_0;
	int32_t ___boolValue_1;
	double ___realValue_2;
	int64_t ___integerValue_3;
	JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E_marshaled_pinvoke ___stringValue_4;
	List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___arrayValue_5;
	Dictionary_2_tDE3993BD3BBC5AA17021AEB6C4928B55CF0AA730 * ___objectValue_6;
	Il2CppIUnknown* ___anyValue_7;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D_marshaled_com
{
	int32_t ___type_0;
	int32_t ___boolValue_1;
	double ___realValue_2;
	int64_t ___integerValue_3;
	JsonString_t55D5C48513353DB3D913A748984CF0687B9F365E_marshaled_com ___stringValue_4;
	List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___arrayValue_5;
	Dictionary_2_tDE3993BD3BBC5AA17021AEB6C4928B55CF0AA730 * ___objectValue_6;
	Il2CppIUnknown* ___anyValue_7;
};

// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57, ___list_0)); }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * get_list_0() const { return ___list_0; }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57, ___current_3)); }
	inline JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  get_current_3() const { return ___current_3; }
	inline JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Func`2<Cone,System.Boolean>
struct Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>
struct Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>
struct Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>
struct Func_2_t578287205B88E28024F821D907D73582B53344F1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>
struct Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct Func_2_tDEA980C7282507293B34B1074F07AFA084A38186  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,Cone>
struct Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>
struct Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,Cone>
struct Func_2_tE0F030B2CA607826B25983C128370C60E0480620  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>
struct Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>
struct Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C  : public MulticastDelegate_t
{
public:

public:
};


// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29, ___value_1)); }
	inline JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  get_value_1() const { return ___value_1; }
	inline JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>
struct ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A  : public IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444
{
public:
	// WinRT.Interop.IUnknownVftbl WinRT.ObjectReference`1::_vftblIUnknown
	IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  ____vftblIUnknown_3;
	// T WinRT.ObjectReference`1::Vftbl
	Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328  ___Vftbl_4;
	// System.Boolean WinRT.ObjectReference`1::_owned
	bool ____owned_5;
	// System.Boolean WinRT.ObjectReference`1::_isLive
	bool ____isLive_6;

public:
	inline static int32_t get_offset_of__vftblIUnknown_3() { return static_cast<int32_t>(offsetof(ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A, ____vftblIUnknown_3)); }
	inline IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  get__vftblIUnknown_3() const { return ____vftblIUnknown_3; }
	inline IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486 * get_address_of__vftblIUnknown_3() { return &____vftblIUnknown_3; }
	inline void set__vftblIUnknown_3(IUnknownVftbl_t9028975D9DFAC20CE3118B442C51984B8BC09486  value)
	{
		____vftblIUnknown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____vftblIUnknown_3))->___QueryInterface_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&____vftblIUnknown_3))->___AddRef_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&____vftblIUnknown_3))->___Release_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_Vftbl_4() { return static_cast<int32_t>(offsetof(ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A, ___Vftbl_4)); }
	inline Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328  get_Vftbl_4() const { return ___Vftbl_4; }
	inline Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328 * get_address_of_Vftbl_4() { return &___Vftbl_4; }
	inline void set_Vftbl_4(Vftbl_tEF6AE07AA78A6300EFE40EE5113A3133D0BD8328  value)
	{
		___Vftbl_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___Vftbl_4))->___IInspectableVftbl_0))->___IUnknownVftbl_0))->___QueryInterface_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___Vftbl_4))->___IInspectableVftbl_0))->___IUnknownVftbl_0))->___AddRef_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___Vftbl_4))->___IInspectableVftbl_0))->___IUnknownVftbl_0))->___Release_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___Vftbl_4))->___IInspectableVftbl_0))->___GetIids_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___Vftbl_4))->___IInspectableVftbl_0))->___GetRuntimeClassName_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___Vftbl_4))->___IInspectableVftbl_0))->___GetTrustLevel_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___Vftbl_4))->___put_Completed_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___Vftbl_4))->___get_Completed_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of__owned_5() { return static_cast<int32_t>(offsetof(ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A, ____owned_5)); }
	inline bool get__owned_5() const { return ____owned_5; }
	inline bool* get_address_of__owned_5() { return &____owned_5; }
	inline void set__owned_5(bool value)
	{
		____owned_5 = value;
	}

	inline static int32_t get_offset_of__isLive_6() { return static_cast<int32_t>(offsetof(ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A, ____isLive_6)); }
	inline bool get__isLive_6() const { return ____isLive_6; }
	inline bool* get_address_of__isLive_6() { return &____isLive_6; }
	inline void set__isLive_6(bool value)
	{
		____isLive_6 = value;
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<Cone>
struct WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F, ___predicate_4)); }
	inline Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>
struct WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6, ___predicate_4)); }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6, ___selector_5)); }
	inline Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>
struct WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0, ___predicate_4)); }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t578287205B88E28024F821D907D73582B53344F1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0, ___selector_5)); }
	inline Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>
struct WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F, ___predicate_4)); }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F, ___selector_5)); }
	inline Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,Cone>
struct WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC, ___selector_5)); }
	inline Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>
struct WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C, ___predicate_4)); }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C, ___selector_5)); }
	inline Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE0F030B2CA607826B25983C128370C60E0480620 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>
struct WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17, ___predicate_4)); }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17, ___selector_5)); }
	inline Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>
struct WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21, ___source_3)); }
	inline List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * get_source_3() const { return ___source_3; }
	inline List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21, ___predicate_4)); }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21, ___selector_5)); }
	inline Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21, ___enumerator_6)); }
	inline Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>
struct WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E, ___source_3)); }
	inline List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * get_source_3() const { return ___source_3; }
	inline List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E, ___predicate_4)); }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t578287205B88E28024F821D907D73582B53344F1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E, ___selector_5)); }
	inline Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E, ___enumerator_6)); }
	inline Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0, ___source_3)); }
	inline List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * get_source_3() const { return ___source_3; }
	inline List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0, ___predicate_4)); }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t578287205B88E28024F821D907D73582B53344F1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0, ___selector_5)); }
	inline Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0, ___enumerator_6)); }
	inline Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9, ___source_3)); }
	inline List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * get_source_3() const { return ___source_3; }
	inline List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9, ___predicate_4)); }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t578287205B88E28024F821D907D73582B53344F1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t578287205B88E28024F821D907D73582B53344F1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9, ___selector_5)); }
	inline Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9, ___enumerator_6)); }
	inline Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,Cone>
struct WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A, ___selector_5)); }
	inline Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>
struct WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B, ___source_3)); }
	inline List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * get_source_3() const { return ___source_3; }
	inline List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B, ___predicate_4)); }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B, ___selector_5)); }
	inline Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE0F030B2CA607826B25983C128370C60E0480620 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B, ___enumerator_6)); }
	inline Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NamedValue::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Utilities.NamedValue::<value>k__BackingField
	PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D  ___U3CvalueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB, ___U3CvalueU3Ek__BackingField_2)); }
	inline PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D  get_U3CvalueU3Ek__BackingField_2() const { return ___U3CvalueU3Ek__BackingField_2; }
	inline PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D * get_address_of_U3CvalueU3Ek__BackingField_2() { return &___U3CvalueU3Ek__BackingField_2; }
	inline void set_U3CvalueU3Ek__BackingField_2(PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D  value)
	{
		___U3CvalueU3Ek__BackingField_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D_marshaled_pinvoke ___U3CvalueU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_t0C949A6884CDF3283D904383FDFC7AA6DF5CD82D_marshaled_com ___U3CvalueU3Ek__BackingField_2;
};

// WinRT.Interop.IAsyncOperation/CompletedHandler
struct CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E  : public MulticastDelegate_t
{
public:

public:
};


// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC, ___list_0)); }
	inline List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * get_list_0() const { return ___list_0; }
	inline List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC, ___current_3)); }
	inline KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>
struct Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE, ___list_0)); }
	inline List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * get_list_0() const { return ___list_0; }
	inline List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE, ___current_3)); }
	inline NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  get_current_3() const { return ___current_3; }
	inline NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
	}
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Boolean>
struct Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,Cone>
struct Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>
struct Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>
struct Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502  : public MulticastDelegate_t
{
public:

public:
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>
struct WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57, ___source_3)); }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * get_source_3() const { return ___source_3; }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57, ___predicate_4)); }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57, ___selector_5)); }
	inline Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57, ___enumerator_6)); }
	inline Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC, ___source_3)); }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * get_source_3() const { return ___source_3; }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC, ___predicate_4)); }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC, ___selector_5)); }
	inline Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC, ___enumerator_6)); }
	inline Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC, ___source_3)); }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * get_source_3() const { return ___source_3; }
	inline List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC, ___predicate_4)); }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC, ___selector_5)); }
	inline Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC, ___enumerator_6)); }
	inline Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,Cone>
struct WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA, ___source_3)); }
	inline List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * get_source_3() const { return ___source_3; }
	inline List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA, ___predicate_4)); }
	inline Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA, ___selector_5)); }
	inline Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F * get_selector_5() const { return ___selector_5; }
	inline Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA, ___enumerator_6)); }
	inline Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5, ___source_3)); }
	inline List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * get_source_3() const { return ___source_3; }
	inline List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5, ___predicate_4)); }
	inline Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5, ___selector_5)); }
	inline Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5, ___enumerator_6)); }
	inline Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF, ___source_3)); }
	inline List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * get_source_3() const { return ___source_3; }
	inline List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF, ___predicate_4)); }
	inline Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF, ___selector_5)); }
	inline Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF, ___enumerator_6)); }
	inline Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>
struct WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57  : public Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57, ___source_3)); }
	inline List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * get_source_3() const { return ___source_3; }
	inline List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57, ___predicate_4)); }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57, ___selector_5)); }
	inline Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57, ___enumerator_6)); }
	inline Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB  : public Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB, ___source_3)); }
	inline List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * get_source_3() const { return ___source_3; }
	inline List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB, ___predicate_4)); }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB, ___selector_5)); }
	inline Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB, ___enumerator_6)); }
	inline Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A, ___source_3)); }
	inline List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * get_source_3() const { return ___source_3; }
	inline List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A, ___predicate_4)); }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A, ___selector_5)); }
	inline Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A, ___enumerator_6)); }
	inline Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  Enumerator_get_Current_m69138BD9C0AD36EE1BBAD1ADADD51E73B18701F8_gshared_inline (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mC335C512D1A02BDEA3E8D0B2C11C3D7CF3F69B83_gshared (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  Enumerator_get_Current_mDB640EBE69963C260FD4F0B3E6DCED931E25E9B8_gshared_inline (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m9C195A867D976DFED3DE6364EFAC2C83824C2327_gshared (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  Enumerator_get_Current_mBB0E30FA944F0649C529C687487757B90F3A5437_gshared_inline (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m0DB713D0210119C4AAD11D25A79A5C56718397EC_gshared (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  Enumerator_get_Current_mF73E9D073C9D97870D5A4B35B59403A11AF084C3_gshared_inline (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mC74390E33EC5E808557C54E1237FEAEE0CE4FF31_gshared (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  Enumerator_get_Current_mD465945F85FAA1C7540E90EE3CE3B7420887F4AD_gshared_inline (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mAE84FC20EE931246715AC403E217A6D8027C32CA_gshared (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  Enumerator_get_Current_mE46574E114EEEBE327B950F7810EFB7F8E6C1576_gshared_inline (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m1BB806A0DEBAAC238B444A4A7C450EDAEB278E5B_gshared (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * __this, const RuntimeMethod* method);

// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current()
inline KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  Enumerator_get_Current_m69138BD9C0AD36EE1BBAD1ADADD51E73B18701F8_inline (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  (*) (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *, const RuntimeMethod*))Enumerator_get_Current_m69138BD9C0AD36EE1BBAD1ADADD51E73B18701F8_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::MoveNext()
inline bool Enumerator_MoveNext_mC335C512D1A02BDEA3E8D0B2C11C3D7CF3F69B83 (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *, const RuntimeMethod*))Enumerator_MoveNext_mC335C512D1A02BDEA3E8D0B2C11C3D7CF3F69B83_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::get_Current()
inline InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  Enumerator_get_Current_mDB640EBE69963C260FD4F0B3E6DCED931E25E9B8_inline (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * __this, const RuntimeMethod* method)
{
	return ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *, const RuntimeMethod*))Enumerator_get_Current_mDB640EBE69963C260FD4F0B3E6DCED931E25E9B8_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
inline bool Enumerator_MoveNext_m9C195A867D976DFED3DE6364EFAC2C83824C2327 (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *, const RuntimeMethod*))Enumerator_MoveNext_m9C195A867D976DFED3DE6364EFAC2C83824C2327_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current()
inline NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  Enumerator_get_Current_mBB0E30FA944F0649C529C687487757B90F3A5437_inline (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * __this, const RuntimeMethod* method)
{
	return ((  NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  (*) (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *, const RuntimeMethod*))Enumerator_get_Current_mBB0E30FA944F0649C529C687487757B90F3A5437_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::MoveNext()
inline bool Enumerator_MoveNext_m0DB713D0210119C4AAD11D25A79A5C56718397EC (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *, const RuntimeMethod*))Enumerator_MoveNext_m0DB713D0210119C4AAD11D25A79A5C56718397EC_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current()
inline NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  Enumerator_get_Current_mF73E9D073C9D97870D5A4B35B59403A11AF084C3_inline (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * __this, const RuntimeMethod* method)
{
	return ((  NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  (*) (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *, const RuntimeMethod*))Enumerator_get_Current_mF73E9D073C9D97870D5A4B35B59403A11AF084C3_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::MoveNext()
inline bool Enumerator_MoveNext_mC74390E33EC5E808557C54E1237FEAEE0CE4FF31 (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *, const RuntimeMethod*))Enumerator_MoveNext_mC74390E33EC5E808557C54E1237FEAEE0CE4FF31_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
inline RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
inline bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0 (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::get_Current()
inline Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  Enumerator_get_Current_mD465945F85FAA1C7540E90EE3CE3B7420887F4AD_inline (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * __this, const RuntimeMethod* method)
{
	return ((  Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  (*) (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *, const RuntimeMethod*))Enumerator_get_Current_mD465945F85FAA1C7540E90EE3CE3B7420887F4AD_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::MoveNext()
inline bool Enumerator_MoveNext_mAE84FC20EE931246715AC403E217A6D8027C32CA (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *, const RuntimeMethod*))Enumerator_MoveNext_mAE84FC20EE931246715AC403E217A6D8027C32CA_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current()
inline JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  Enumerator_get_Current_mE46574E114EEEBE327B950F7810EFB7F8E6C1576_inline (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * __this, const RuntimeMethod* method)
{
	return ((  JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  (*) (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *, const RuntimeMethod*))Enumerator_get_Current_mE46574E114EEEBE327B950F7810EFB7F8E6C1576_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::MoveNext()
inline bool Enumerator_MoveNext_m1BB806A0DEBAAC238B444A4A7C450EDAEB278E5B (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *, const RuntimeMethod*))Enumerator_MoveNext_m1BB806A0DEBAAC238B444A4A7C450EDAEB278E5B_gshared)(__this, method);
}
// System.Void WinRT.Interop.IAsyncOperation::.ctor(System.Guid,WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IAsyncOperation__ctor_m92848571124F568682F25CF6C573D87ACE7C6B7F (IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 * __this, Guid_t  ___iid0, ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * ___obj1, const RuntimeMethod* method);
// WinRT.Interop.IAsyncInfo WinRT.Interop.IAsyncInfo::op_Implicit(WinRT.IObjectReference)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * IAsyncInfo_op_Implicit_m75C7504DFE28C5B0635BE1144584F5A435019238 (IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444 * ___obj0, const RuntimeMethod* method);
// System.Void WinRT.Interop.IAsyncOperation/CompletedHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompletedHandler__ctor_m9711EB106AA6D15428773BE1F7DF029C2E5D563B (CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void WinRT.Interop.IAsyncOperation::set_Completed(WinRT.Interop.IAsyncOperation/CompletedHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IAsyncOperation_set_Completed_mD8ECD437EA2B6D88D9CEA3872A6EA89D45D06AE2 (IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 * __this, CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E * ___value0, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Threading.CancellationTokenRegistration System.Threading.CancellationToken::Register(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CancellationTokenRegistration_t407059AA0E00ABE74F43C533E7D035C4BA451F6A  CancellationToken_Register_m6C186260806A5918D17E0B3A5AF2520D8AFF0787 (CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___callback0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m03C2D673E16DB9CEDF4E26DA2F10A65316196051_gshared (WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B * __this, RuntimeObject* ___source0, Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * ___predicate1, Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m08D0FBFF3D9B82377C9B6B44C2EC55EDF8703AE8_gshared (WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_1 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * L_2 = (Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B * L_3 = (WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B *, RuntimeObject*, Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *, Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_1, (Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m3757223F20F504373FE3EE4DF7A5968551E22FB8_gshared (WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m30C8617D4B5B21A27F4087674494DD67BD41747F_gshared (WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_6;
		L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_6;
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_7 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_8 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_9 = V_1;
		NullCheck((Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *, KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_8, (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * L_11 = (Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)__this->get_selector_5();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_12 = V_1;
		NullCheck((Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *, KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)L_11, (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m7E26827D662A1C4BD80E5FB8E460622920319AEF_gshared (WhereSelectEnumerableIterator_2_t73525ED90147B6C414F287C8363715FCF86AD26B * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m9DC678171D779FCAED47DBF85B2ABEECCEE86E41_gshared (WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6 * __this, RuntimeObject* ___source0, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate1, Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectEnumerableIterator_2_Clone_m0AAEB574B1FD2D1875FCA26DED1A77CF4967D6CC_gshared (WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * L_2 = (Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6 * L_3 = (WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6 *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_1, (Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE655A2B6036D2ACEA2AF6E33C62C4EC56C8A77C8_gshared (WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m167E5D9AB2A4BA1C4C51D06C1A4608CB890C79D2_gshared (WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_6;
		L_6 = InterfaceFuncInvoker0< InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_6;
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_7 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_8 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_9 = V_1;
		NullCheck((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * L_11 = (Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)__this->get_selector_5();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_12 = V_1;
		NullCheck((Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)L_11, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m67765BCF392E82759A07C3989901A4C530ECFD7A_gshared (WhereSelectEnumerableIterator_2_t1F5D61AB0CAA1C8F6214AB0214F3FABCED5101C6 * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m04138520014CD474AD3B75A005088FD84A515284_gshared (WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80 * __this, RuntimeObject* ___source0, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate1, Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectEnumerableIterator_2_Clone_m826FA290251D0716647E2838128B9578472B92FB_gshared (WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * L_2 = (Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80 * L_3 = (WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80 *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_1, (Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mFFC500ADF3EF478DE9463E8434F89E12A067F351_gshared (WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m710D68DCCA1BFBDA888281482CA30A4BB1C2BAC4_gshared (WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_6;
		L_6 = InterfaceFuncInvoker0< InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_6;
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_7 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_8 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_9 = V_1;
		NullCheck((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * L_11 = (Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)__this->get_selector_5();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_12 = V_1;
		NullCheck((Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)L_11, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m3CE04C99DFA723860C53BE66D8A22EDBA8692C3A_gshared (WhereSelectEnumerableIterator_2_tFE1762C651E2888BD3F9E126545CAA843C8F8D80 * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m8B9E834BA687A8595B5BB8A484498EF55AC90D1C_gshared (WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C * __this, RuntimeObject* ___source0, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate1, Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m4EEEAA67A2895AFB3F8C25F56648F557D3DC653E_gshared (WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * L_2 = (Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C * L_3 = (WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_1, (Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m1E9851C3D501C6E617110F85D5BDAA2F90A08F92_gshared (WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m733C6D1BF71FD969A7E4F2925DF089EFDB46DA9F_gshared (WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_6;
		L_6 = InterfaceFuncInvoker0< InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_6;
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_7 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_8 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_9 = V_1;
		NullCheck((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * L_11 = (Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)__this->get_selector_5();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_12 = V_1;
		NullCheck((Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)L_11, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mE0253D2701924DB94373C02B19BB60F8ACF677E0_gshared (WhereSelectEnumerableIterator_2_t19DFCCC2919BC0639E2A3AF63D8558AB8714441C * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mDA72E5EF0DD17572BF84571452248B84A623416E_gshared (WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0 * __this, RuntimeObject* ___source0, Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate1, Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectEnumerableIterator_2_Clone_mE78A32C7E0759DDB083BD83AFE56393980D6540C_gshared (WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * L_2 = (Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0 * L_3 = (WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0 *, RuntimeObject*, Func_2_t578287205B88E28024F821D907D73582B53344F1 *, Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_1, (Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE9286B20B060E3DFA607D1A3137F67E50DEDCBF8_gshared (WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m11B25F11417395A3458893F68F3857BB1942935F_gshared (WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_6;
		L_6 = InterfaceFuncInvoker0< NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_6;
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_7 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_8 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_9 = V_1;
		NullCheck((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t578287205B88E28024F821D907D73582B53344F1 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * L_11 = (Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)__this->get_selector_5();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_12 = V_1;
		NullCheck((Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)L_11, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mFC5B63734F0D827D0C21F0608BD6B77B11A0EBEA_gshared (WhereSelectEnumerableIterator_2_tED82D9A21D6F79E78CD8EDC72BB022D6E6BC8FD0 * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mD165E796303ABC48CA869E8E209A4BBD5EE304BF_gshared (WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109 * __this, RuntimeObject* ___source0, Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate1, Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectEnumerableIterator_2_Clone_m40C9CAC39BB17ACDF4DDFB9E67276CEEBC8F19AA_gshared (WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * L_2 = (Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109 * L_3 = (WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109 *, RuntimeObject*, Func_2_t578287205B88E28024F821D907D73582B53344F1 *, Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_1, (Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m78752D6873F8A0D88BE122B11EABDA0409750FC5_gshared (WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m375D4779341F1A41CBDCEBD189F4A648BC2DE6E7_gshared (WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_6;
		L_6 = InterfaceFuncInvoker0< NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_6;
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_7 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_8 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_9 = V_1;
		NullCheck((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t578287205B88E28024F821D907D73582B53344F1 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * L_11 = (Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)__this->get_selector_5();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_12 = V_1;
		NullCheck((Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)L_11, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m0B8C339F2E70CF6D736D0B96D4BC784E2A2A7EA2_gshared (WhereSelectEnumerableIterator_2_tDD7552E39FE913A75589FA430C9A2F54F4F2C109 * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m46E090BD184DA51AF20D284FA16A5757433BE9E9_gshared (WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1 * __this, RuntimeObject* ___source0, Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate1, Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m7F81617B19B3E7BF0255BBA92CC9607D5BA9177C_gshared (WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * L_2 = (Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1 * L_3 = (WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1 *, RuntimeObject*, Func_2_t578287205B88E28024F821D907D73582B53344F1 *, Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_1, (Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m0F5E1C3C6B009E34109DF623EAAC8524650367E1_gshared (WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m8CC78CAC783F55E46A0186B10611C29E809FC396_gshared (WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_6;
		L_6 = InterfaceFuncInvoker0< NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_6;
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_7 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_8 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_9 = V_1;
		NullCheck((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t578287205B88E28024F821D907D73582B53344F1 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * L_11 = (Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)__this->get_selector_5();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_12 = V_1;
		NullCheck((Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)L_11, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m29E9333047FB72FC52317466E05EFC2D8FAFE8A4_gshared (WhereSelectEnumerableIterator_2_tCF13B8AC261ABD8D6D49D27EF3E893F8E66004A1 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m4B4819CEC223BBEF1E958CC29980E2FA8FB4B974_gshared (WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F * __this, RuntimeObject* ___source0, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate1, Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectEnumerableIterator_2_Clone_mC2D93D3F48B82B1D9FEDE633E896E4506B60F4EC_gshared (WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * L_2 = (Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F * L_3 = (WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F *, RuntimeObject*, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_1, (Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mA12A2FE53B4E7E763B667EE41F62F68F1CE3AA49_gshared (WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m700A61DB5B528C5EA806C584221810D1AD25FD3B_gshared (WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_6;
		L_6 = InterfaceFuncInvoker0< NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_6;
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_7 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_8 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_9 = V_1;
		NullCheck((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * L_11 = (Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)__this->get_selector_5();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_12 = V_1;
		NullCheck((Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)L_11, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mF5BE7E004A0669A2811879327C1039BD6989B4E6_gshared (WhereSelectEnumerableIterator_2_t971DB822E0141B5A253DD72D133B7FB0BE165C4F * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mF9487F43DFB992DA31EB53295904EE216B71A5CB_gshared (WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F * __this, RuntimeObject* ___source0, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate1, Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectEnumerableIterator_2_Clone_mDD336228C2E9AD3627E26BCA1DE6F2996B4FF4F6_gshared (WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * L_2 = (Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F * L_3 = (WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F *, RuntimeObject*, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_1, (Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m58D3E28BFC3054E372270E34BD81805855333F0C_gshared (WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mBDB6CEE0C01C5AFA0B708257A0CB6A7D395F87E3_gshared (WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_6;
		L_6 = InterfaceFuncInvoker0< NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_6;
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_7 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_8 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_9 = V_1;
		NullCheck((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * L_11 = (Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)__this->get_selector_5();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_12 = V_1;
		NullCheck((Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)L_11, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m74F1E3DD5E4D2B6C6DEC6EEE39843BF0D719D68F_gshared (WhereSelectEnumerableIterator_2_t36218ECF1AD6ECAC01BA6353994BDEB2FAF0FB5F * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m6CAEAD8E154C7B6724543F3944318C980F2472C9_gshared (WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208 * __this, RuntimeObject* ___source0, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate1, Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m46534E0C266D97AFE5B8A6E868EFE14D1657E4CC_gshared (WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * L_2 = (Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208 * L_3 = (WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208 *, RuntimeObject*, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_1, (Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m7AF96CFF1884448EBE044A1D044D59E60B6D4FC4_gshared (WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mEDC19219B299EE919B98405F4CCA6F8AAB9E9358_gshared (WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_6;
		L_6 = InterfaceFuncInvoker0< NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_6;
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_7 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_8 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_9 = V_1;
		NullCheck((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * L_11 = (Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)__this->get_selector_5();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_12 = V_1;
		NullCheck((Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)L_11, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mFDB9E6825722E0D5877A50736124EF26C6BDFDCC_gshared (WhereSelectEnumerableIterator_2_t94A058D92F95DF7D4A23FF2B063CD927D50B0208 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,Cone>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m7A31063553EBF3E7CEE82342ED168F0B6366EB7D_gshared (WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectEnumerableIterator_2_Clone_m7BC4B804DEE7AF0EEEE60B98477C2D524D9C23BF_gshared (WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * L_2 = (Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC * L_3 = (WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,Cone>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m6A69E6FCC6DF8A7EEB827C15584E7A810796EB94_gshared (WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m49D78801A51105CBD3BC3F08E386B922EF55FB3F_gshared (WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * L_11 = (Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m2A93782BF594FFA22B331F22837A11CBDA6B3FB0_gshared (WhereSelectEnumerableIterator_2_t4BE0C1D5CA21527F1615A7D9E05C4D4C5C2F37FC * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mBC05915E6B0E2156B7778129546A64C45DFA5E20_gshared (WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectEnumerableIterator_2_Clone_m62774E42334D5606C7E391BBFB0D0161C600E8D0_gshared (WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * L_2 = (Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E * L_3 = (WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m418D5D0278D077BF1DC38F2B592897B39FA28536_gshared (WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m304AF68A147D99F21DC84493B0B22B3491009756_gshared (WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * L_11 = (Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m43326C825AD8DC248F94EFD0585DEED865EB7E37_gshared (WhereSelectEnumerableIterator_2_t98D1397F1FCD77C8ED3DC78794E11B91B901B04E * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m6DFD3E949A8FA5121F520D501B78C17E84EBDFAC_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mD5F17A02281E6D1529D117CFF2E0F8C347D1B13F_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * L_3 = (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mAA70577DEF67CEC98FE677984AE2175B7D4E4D00_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m95AEE737A22EFFFE6557F448BF5AFCC6241D0BD7_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m72A38A8170E8B837F5C090642BE08E3845A8EB37_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mBC7AC628915B48530244F4351E0E94DB3B4FF977_gshared (WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C * __this, RuntimeObject* ___source0, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate1, Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectEnumerableIterator_2_Clone_m1F40A845C82244EC33CD9B85531D65EBEFB4B35D_gshared (WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * L_2 = (Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C * L_3 = (WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C *, RuntimeObject*, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_1, (Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m86511E3F7CA13E3A56D71AE66B3FC4DCE9CB17F4_gshared (WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mE2D19FC4EF2C7859D8BDDBB7DB3308364A699DC9_gshared (WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_6;
		L_6 = InterfaceFuncInvoker0< Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_6;
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_7 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_8 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_9 = V_1;
		NullCheck((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * L_11 = (Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)__this->get_selector_5();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_12 = V_1;
		NullCheck((Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)L_11, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m65734716F1E743A5931EE3039FD8952433E21B92_gshared (WhereSelectEnumerableIterator_2_tF7647D7B52BB14FC38E3ED3BE2CC0AA558241E6C * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mB995C73FAF5C36C9EE954E9F050E607720AFF1C9_gshared (WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955 * __this, RuntimeObject* ___source0, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate1, Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectEnumerableIterator_2_Clone_mCEA50C2C7FB42E8001A4E2322A66943618717C80_gshared (WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * L_2 = (Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955 * L_3 = (WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955 *, RuntimeObject*, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_1, (Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m151A24F2BFCF552841B1A869FE8B3CDBCDE62F67_gshared (WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mBE7CAC934CF565CF713EFED4E8118DE40FD92FCE_gshared (WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_6;
		L_6 = InterfaceFuncInvoker0< Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_6;
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_7 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_8 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_9 = V_1;
		NullCheck((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * L_11 = (Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)__this->get_selector_5();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_12 = V_1;
		NullCheck((Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)L_11, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m6E006C28184594953A7013E717BD04918490BC1A_gshared (WhereSelectEnumerableIterator_2_tAEAEC1471D2F8D7ED902984382CC791821EC5955 * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mA1F243154F484A4803D05754D3187332F219E702_gshared (WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA * __this, RuntimeObject* ___source0, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate1, Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mD07DC1B110035B183B925AF2E5ABE885161F4A85_gshared (WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * L_2 = (Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA * L_3 = (WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA *, RuntimeObject*, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_1, (Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m5A15FC55F044FA7D351518F4A15357BEC44AFD5C_gshared (WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mE0C5B75E5D1FE4DA38B46C5283596A56431CA846_gshared (WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_6;
		L_6 = InterfaceFuncInvoker0< Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_6;
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_7 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_8 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_9 = V_1;
		NullCheck((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * L_11 = (Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)__this->get_selector_5();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_12 = V_1;
		NullCheck((Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)L_11, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m9E8328F3966D62F307B3BF1C4AF5DDDBC03DEF4A_gshared (WhereSelectEnumerableIterator_2_t146CF6459DC0607D4CE448F1003A61D42B1FDFAA * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m5D612643165A3322081DD1A20088E8D2020E7ECD_gshared (WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17 * __this, RuntimeObject* ___source0, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate1, Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectEnumerableIterator_2_Clone_m2DE0A17622386A18F547FAB2EC5B1E77CDF6118F_gshared (WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * L_2 = (Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17 * L_3 = (WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17 *, RuntimeObject*, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_1, (Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m3413BB7E18A2C6EB9F7BA4C118509D03B6B66769_gshared (WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m46E97BDF8853B7F7B54B398823A848D6B4FB15B4_gshared (WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_6;
		L_6 = InterfaceFuncInvoker0< JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_6;
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_7 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_8 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_9 = V_1;
		NullCheck((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * L_11 = (Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)__this->get_selector_5();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_12 = V_1;
		NullCheck((Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)L_11, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mFDE3B63477248C04057CDB9B6E393B783EB2CCF6_gshared (WhereSelectEnumerableIterator_2_tF00F2B5CBFF61107FD53E93CE9023A87BB5A3D17 * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mCBEA3B3A44A627F0FAECBBCE0EF6440A0451A888_gshared (WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB * __this, RuntimeObject* ___source0, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate1, Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectEnumerableIterator_2_Clone_m076CF0CD4CDB691F0AA97D10403E033797949C4F_gshared (WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * L_2 = (Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB * L_3 = (WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB *, RuntimeObject*, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_1, (Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m72DAD83F664B0A95AA261C51BD4038030938222E_gshared (WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m111DC058CDE3FDD7C655269D16E3CDE2CA7F9AC7_gshared (WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_6;
		L_6 = InterfaceFuncInvoker0< JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_6;
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_7 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_8 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_9 = V_1;
		NullCheck((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * L_11 = (Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)__this->get_selector_5();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_12 = V_1;
		NullCheck((Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)L_11, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mF3A8EBAEE5DE8D2FDA81F364A977C129B564D7CA_gshared (WhereSelectEnumerableIterator_2_t3C7584DD75CAE3CDFAFBE432B4029FA0A00EB3CB * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mBB60A1C7B5FA4A35F2497831DA0B795274B7DFF8_gshared (WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A * __this, RuntimeObject* ___source0, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate1, Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m14A24DAF39C3940D18348AFC091DBFF7BD6CE031_gshared (WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * L_2 = (Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A * L_3 = (WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A *, RuntimeObject*, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_1, (Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE2A3238655816629EC5DFE1C0B62A31AA99653D4_gshared (WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m5677D6F34816086861729FCCEA69FCA0D8C19DCC_gshared (WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_6;
		L_6 = InterfaceFuncInvoker0< JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_6;
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_7 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_8 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_9 = V_1;
		NullCheck((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * L_11 = (Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)__this->get_selector_5();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_12 = V_1;
		NullCheck((Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)L_11, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m69231F32319A50D50A047C7CA767D02EB053E55B_gshared (WhereSelectEnumerableIterator_2_tC92F13086234CB145093299565BB0B7FFBACBD7A * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,Cone>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mF8B8DA4F1AFE43636A9FA8E2BDF33D410C7FEF5A_gshared (WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA * __this, List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * ___source0, Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * ___predicate1, Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectListIterator_2_Clone_m8024C47A6FF821B7DD81A7EF8EB9E2F3463040A8_gshared (WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA * __this, const RuntimeMethod* method)
{
	{
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_0 = (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)__this->get_source_3();
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_1 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F * L_2 = (Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F *)__this->get_selector_5();
		WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA * L_3 = (WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA *, List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *, Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *, Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_0, (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_1, (Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m20CCF7DE99B93194B9AE26CB052D44AFBE2DA050_gshared (WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_3 = (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)__this->get_source_3();
		NullCheck((List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_3);
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  L_4;
		L_4 = ((  Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  (*) (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * L_5 = (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_6;
		L_6 = Enumerator_get_Current_m69138BD9C0AD36EE1BBAD1ADADD51E73B18701F8_inline((Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_6;
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_7 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_8 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_9 = V_1;
		NullCheck((Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *, KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_8, (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F * L_11 = (Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F *)__this->get_selector_5();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_12 = V_1;
		NullCheck((Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F *, KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tDD6226D1270FDFC3DAE49D7B800102CD0569AC0F *)L_11, (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * L_14 = (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC335C512D1A02BDEA3E8D0B2C11C3D7CF3F69B83((Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mB8773D454DCF5EE0EBD5075B6B1EDE5210278045_gshared (WhereSelectListIterator_2_tFB3B0C585D3C7FF21A69F4ECE8FD8A9B890DBBCA * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m9642B269689DD94A975DF9CBC1B8A9CE81AE5195_gshared (WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5 * __this, List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * ___source0, Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * ___predicate1, Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectListIterator_2_Clone_m6E4E2AC35FE93462A7470DD847D829A051F46C83_gshared (WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5 * __this, const RuntimeMethod* method)
{
	{
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_0 = (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)__this->get_source_3();
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_1 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B * L_2 = (Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B *)__this->get_selector_5();
		WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5 * L_3 = (WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5 *, List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *, Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *, Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_0, (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_1, (Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m5AA6246B2B3F9912B7472E62C33B0528C9019424_gshared (WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_3 = (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)__this->get_source_3();
		NullCheck((List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_3);
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  L_4;
		L_4 = ((  Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  (*) (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * L_5 = (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_6;
		L_6 = Enumerator_get_Current_m69138BD9C0AD36EE1BBAD1ADADD51E73B18701F8_inline((Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_6;
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_7 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_8 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_9 = V_1;
		NullCheck((Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *, KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_8, (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B * L_11 = (Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B *)__this->get_selector_5();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_12 = V_1;
		NullCheck((Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B *, KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t16B8F143A561454890F470B787B0C2901E3D0D2B *)L_11, (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * L_14 = (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC335C512D1A02BDEA3E8D0B2C11C3D7CF3F69B83((Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m58556882A13DE8D1F2B287CA4D5C2E23EC05549A_gshared (WhereSelectListIterator_2_tBFD0FFC5ACB93F38B9D2FEFF738F0BCF7A2562C5 * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m8071F9CAA8D8D32839E16720616728C6D4616EC4_gshared (WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF * __this, List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * ___source0, Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * ___predicate1, Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mE08B21248F56EB8CC839B97D5CD1152779CEF0E1_gshared (WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF * __this, const RuntimeMethod* method)
{
	{
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_0 = (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)__this->get_source_3();
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_1 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * L_2 = (Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)__this->get_selector_5();
		WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF * L_3 = (WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF *, List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *, Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *, Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_0, (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_1, (Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m73CDD3294FE19C35B62ECD4B6682411F9E88F733_gshared (WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 * L_3 = (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)__this->get_source_3();
		NullCheck((List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_3);
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  L_4;
		L_4 = ((  Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC  (*) (List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t289DAAA36466F75E7E542E7AF18470A8C1F582A6 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * L_5 = (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_6;
		L_6 = Enumerator_get_Current_m69138BD9C0AD36EE1BBAD1ADADD51E73B18701F8_inline((Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_6;
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_7 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 * L_8 = (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)__this->get_predicate_4();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_9 = V_1;
		NullCheck((Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *, KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t920B117889188AA2BD1ADCF101E1CBF3EBEB49C9 *)L_8, (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D * L_11 = (Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)__this->get_selector_5();
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_12 = V_1;
		NullCheck((Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *, KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5917D8C3C6F7766226282957DA7245C92EFF092D *)L_11, (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * L_14 = (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC335C512D1A02BDEA3E8D0B2C11C3D7CF3F69B83((Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)(Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m00B5E78B9CCEEBCC6D86DC0E6D44B1F6C917D907_gshared (WhereSelectListIterator_2_t9E43CB69381F906AFA347F2923F5F1DA530D85EF * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m36EECB29708CB50BD6A73751710548E625105CC9_gshared (WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21 * __this, List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * ___source0, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate1, Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectListIterator_2_Clone_mEFF21B0A7EA2CABE6F317F2EA9B358DD2F79D17C_gshared (WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21 * __this, const RuntimeMethod* method)
{
	{
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_0 = (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)__this->get_source_3();
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * L_2 = (Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)__this->get_selector_5();
		WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21 * L_3 = (WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21 *, List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_0, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_1, (Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m65C218423499E9D717EE21ACCFC67A1D4002DB76_gshared (WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_3 = (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)__this->get_source_3();
		NullCheck((List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_3);
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  L_4;
		L_4 = ((  Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  (*) (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * L_5 = (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)__this->get_address_of_enumerator_6();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_6;
		L_6 = Enumerator_get_Current_mDB640EBE69963C260FD4F0B3E6DCED931E25E9B8_inline((Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_6;
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_7 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_8 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_9 = V_1;
		NullCheck((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D * L_11 = (Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)__this->get_selector_5();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_12 = V_1;
		NullCheck((Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA42AF3658E13E92CBF88F970CB70A5732A33317D *)L_11, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * L_14 = (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m9C195A867D976DFED3DE6364EFAC2C83824C2327((Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m83BC2DF1802928E6B719FF0E8027E8BA7CEC520B_gshared (WhereSelectListIterator_2_t388E60BFC92086410A241D7957D268998AC2FB21 * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m3D6387356FE3660BA3545A822C3D5E117126C088_gshared (WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1 * __this, List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * ___source0, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate1, Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectListIterator_2_Clone_mDD82237D4C797E14B4F234CF4DE3886EEC21CE7A_gshared (WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1 * __this, const RuntimeMethod* method)
{
	{
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_0 = (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)__this->get_source_3();
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * L_2 = (Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)__this->get_selector_5();
		WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1 * L_3 = (WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1 *, List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_0, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_1, (Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mA0BEB08E0CCD5BD621C1AF1276A712341FFE2D72_gshared (WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_3 = (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)__this->get_source_3();
		NullCheck((List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_3);
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  L_4;
		L_4 = ((  Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  (*) (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * L_5 = (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)__this->get_address_of_enumerator_6();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_6;
		L_6 = Enumerator_get_Current_mDB640EBE69963C260FD4F0B3E6DCED931E25E9B8_inline((Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_6;
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_7 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_8 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_9 = V_1;
		NullCheck((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D * L_11 = (Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)__this->get_selector_5();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_12 = V_1;
		NullCheck((Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t97CAC3103C0F1F2BEBBF43BB84DFB6087B4D465D *)L_11, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * L_14 = (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m9C195A867D976DFED3DE6364EFAC2C83824C2327((Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m0C30F57B8E3D9542C59190C86E7EA243686B49AD_gshared (WhereSelectListIterator_2_tD906A77A768297842CEF1E3AD60708627B91F4B1 * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m0073460543D67E0FE0497130F7EF243590AC953D_gshared (WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F * __this, List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * ___source0, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate1, Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m940AE5F0B2718FB232BCA96BB4F9971031276248_gshared (WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F * __this, const RuntimeMethod* method)
{
	{
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_0 = (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)__this->get_source_3();
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_1 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * L_2 = (Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)__this->get_selector_5();
		WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F * L_3 = (WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F *, List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_0, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_1, (Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m5B2340A5AC485374292D723389B8F1B5C379F534_gshared (WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B * L_3 = (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)__this->get_source_3();
		NullCheck((List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_3);
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  L_4;
		L_4 = ((  Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163  (*) (List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tCA56E968F29D4AD9770E37DE970B41C22C56834B *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * L_5 = (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)__this->get_address_of_enumerator_6();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_6;
		L_6 = Enumerator_get_Current_mDB640EBE69963C260FD4F0B3E6DCED931E25E9B8_inline((Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_6;
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_7 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_8 = (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)__this->get_predicate_4();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_9 = V_1;
		NullCheck((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_8, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E * L_11 = (Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)__this->get_selector_5();
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_12 = V_1;
		NullCheck((Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *, InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1EE5405BD0AFCFB8292B48765D3F7E991BB8DF4E *)L_11, (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * L_14 = (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m9C195A867D976DFED3DE6364EFAC2C83824C2327((Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)(Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m328CE6FBEBE213530573FFE04290BCA6AC6AFAF0_gshared (WhereSelectListIterator_2_tBBA60631D8FC87D7E37A1275C89736EFD5D2938F * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCC1EC9587E01B111BE67ED2D22C1F1B9F08371DA_gshared (WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E * __this, List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * ___source0, Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate1, Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectListIterator_2_Clone_m101E96115C90E37079B784C311F8E3FFE99F74CA_gshared (WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E * __this, const RuntimeMethod* method)
{
	{
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_0 = (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)__this->get_source_3();
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * L_2 = (Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)__this->get_selector_5();
		WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E * L_3 = (WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E *, List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *, Func_2_t578287205B88E28024F821D907D73582B53344F1 *, Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_0, (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_1, (Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mF21464987D52C7EEA162293DA43E0AECCB3BE6C5_gshared (WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_3 = (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)__this->get_source_3();
		NullCheck((List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_3);
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  L_4;
		L_4 = ((  Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  (*) (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * L_5 = (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)__this->get_address_of_enumerator_6();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_6;
		L_6 = Enumerator_get_Current_mBB0E30FA944F0649C529C687487757B90F3A5437_inline((Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_6;
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_7 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_8 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_9 = V_1;
		NullCheck((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t578287205B88E28024F821D907D73582B53344F1 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 * L_11 = (Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)__this->get_selector_5();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_12 = V_1;
		NullCheck((Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t40D89EA6AB2EEEF5CA4E1155F25CEF6F962880D6 *)L_11, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * L_14 = (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m0DB713D0210119C4AAD11D25A79A5C56718397EC((Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m4F052B46AF8FF45FBC18974B2C50CD731F25BA39_gshared (WhereSelectListIterator_2_tE27FA3E3D1B2304B4C0B1044CBEB685B6B00A40E * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m04C7CBE688B406CAE45262B2CC2876C9629FC4A3_gshared (WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0 * __this, List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * ___source0, Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate1, Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectListIterator_2_Clone_m5BBF7864D8947DB6C053F6E8E1B8C34FD72F71D1_gshared (WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_0 = (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)__this->get_source_3();
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * L_2 = (Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)__this->get_selector_5();
		WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0 * L_3 = (WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0 *, List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *, Func_2_t578287205B88E28024F821D907D73582B53344F1 *, Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_0, (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_1, (Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m1967D9AFFD06829B60D0FFB60A973C3B7E409DA6_gshared (WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_3 = (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)__this->get_source_3();
		NullCheck((List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_3);
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  L_4;
		L_4 = ((  Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  (*) (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * L_5 = (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)__this->get_address_of_enumerator_6();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_6;
		L_6 = Enumerator_get_Current_mBB0E30FA944F0649C529C687487757B90F3A5437_inline((Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_6;
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_7 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_8 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_9 = V_1;
		NullCheck((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t578287205B88E28024F821D907D73582B53344F1 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 * L_11 = (Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)__this->get_selector_5();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_12 = V_1;
		NullCheck((Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tF35527F93F3A89F0580E7F9FE30170543A2BD2E0 *)L_11, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * L_14 = (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m0DB713D0210119C4AAD11D25A79A5C56718397EC((Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mD20847D5188E8EE6E34E6CB88B5D31FD581A1958_gshared (WhereSelectListIterator_2_t8E63E56C5A84E375F4B39E163C41BD6E542A7AE0 * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m3E63CC471AD95725815D5A50F661FC6F575F15CE_gshared (WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9 * __this, List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * ___source0, Func_2_t578287205B88E28024F821D907D73582B53344F1 * ___predicate1, Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mC5A4E78A3B3958C6607FF70D643F58A252B1A214_gshared (WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_0 = (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)__this->get_source_3();
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_1 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * L_2 = (Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)__this->get_selector_5();
		WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9 * L_3 = (WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9 *, List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *, Func_2_t578287205B88E28024F821D907D73582B53344F1 *, Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_0, (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_1, (Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m9F882C962085C8B26118BD9EE97C25F085060781_gshared (WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 * L_3 = (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)__this->get_source_3();
		NullCheck((List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_3);
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  L_4;
		L_4 = ((  Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695  (*) (List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0E05D621B20543765F13D96D19E9EAB81885B8E1 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * L_5 = (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)__this->get_address_of_enumerator_6();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_6;
		L_6 = Enumerator_get_Current_mBB0E30FA944F0649C529C687487757B90F3A5437_inline((Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_6;
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_7 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t578287205B88E28024F821D907D73582B53344F1 * L_8 = (Func_2_t578287205B88E28024F821D907D73582B53344F1 *)__this->get_predicate_4();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_9 = V_1;
		NullCheck((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t578287205B88E28024F821D907D73582B53344F1 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t578287205B88E28024F821D907D73582B53344F1 *)L_8, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 * L_11 = (Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)__this->get_selector_5();
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_12 = V_1;
		NullCheck((Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *, NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tDEA980C7282507293B34B1074F07AFA084A38186 *)L_11, (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * L_14 = (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m0DB713D0210119C4AAD11D25A79A5C56718397EC((Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)(Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m19937F6C6AEFC52A777C2B96F806BAA3154AFCC8_gshared (WhereSelectListIterator_2_tBE22CA095B03FB469A8B76F340CA934EA4A7F6B9 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m28FF3EAC66BADA2098EF37A153E67384A7CEE311_gshared (WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57 * __this, List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * ___source0, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate1, Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectListIterator_2_Clone_m73B3E4016857EE8015FEFDB167F6E7C7E2E932BA_gshared (WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57 * __this, const RuntimeMethod* method)
{
	{
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_0 = (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)__this->get_source_3();
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * L_2 = (Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)__this->get_selector_5();
		WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57 * L_3 = (WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57 *, List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_0, (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_1, (Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m528A3F800F9C3AC87ED1477E2F39968277107823_gshared (WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_3 = (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)__this->get_source_3();
		NullCheck((List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_3);
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  L_4;
		L_4 = ((  Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  (*) (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * L_5 = (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)__this->get_address_of_enumerator_6();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_6;
		L_6 = Enumerator_get_Current_mF73E9D073C9D97870D5A4B35B59403A11AF084C3_inline((Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_6;
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_7 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_8 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_9 = V_1;
		NullCheck((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 * L_11 = (Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)__this->get_selector_5();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_12 = V_1;
		NullCheck((Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFDC071A931E3EC196EA54553731C737E22D76C84 *)L_11, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * L_14 = (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC74390E33EC5E808557C54E1237FEAEE0CE4FF31((Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m3F3F3DDDF2E6977CC45D8FE0BC4B331F84E0FC47_gshared (WhereSelectListIterator_2_t563019C89DC665EADFF37B4DBC2E75D1D8A87F57 * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m7DB2E89C66CF97CE026FD80FBE8D8967535AB263_gshared (WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB * __this, List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * ___source0, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate1, Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectListIterator_2_Clone_m2DFBAE7D4E23F595A46A77BE24D9A9B7DF748015_gshared (WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB * __this, const RuntimeMethod* method)
{
	{
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_0 = (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)__this->get_source_3();
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * L_2 = (Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)__this->get_selector_5();
		WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB * L_3 = (WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB *, List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_0, (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_1, (Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m8C83340018666BAC4733B98050A9A51D04CAE19C_gshared (WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_3 = (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)__this->get_source_3();
		NullCheck((List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_3);
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  L_4;
		L_4 = ((  Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  (*) (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * L_5 = (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)__this->get_address_of_enumerator_6();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_6;
		L_6 = Enumerator_get_Current_mF73E9D073C9D97870D5A4B35B59403A11AF084C3_inline((Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_6;
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_7 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_8 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_9 = V_1;
		NullCheck((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 * L_11 = (Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)__this->get_selector_5();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_12 = V_1;
		NullCheck((Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0BC12F96A36DB4812DCFB7F1861BA323E83D0D75 *)L_11, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * L_14 = (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC74390E33EC5E808557C54E1237FEAEE0CE4FF31((Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m4F5507CF3F92B2A19946912600CDDA60F3806242_gshared (WhereSelectListIterator_2_tE7B7A76C41301158C64C9547793AFDC7200E79AB * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mEDB9A6B7A94BB995A3C000335E0EC502E5111F60_gshared (WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A * __this, List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * ___source0, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * ___predicate1, Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m9C83967591A3AF812A17E404215491432F113F2F_gshared (WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A * __this, const RuntimeMethod* method)
{
	{
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_0 = (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)__this->get_source_3();
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_1 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * L_2 = (Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)__this->get_selector_5();
		WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A * L_3 = (WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A *, List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *, Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_0, (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_1, (Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m583DD5574FE5B78E27312CCEC709CDCC3403171A_gshared (WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 * L_3 = (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)__this->get_source_3();
		NullCheck((List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_3);
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  L_4;
		L_4 = ((  Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE  (*) (List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t70917E86BA36D1353723A6A0F90C56DD734E8937 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * L_5 = (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)__this->get_address_of_enumerator_6();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_6;
		L_6 = Enumerator_get_Current_mF73E9D073C9D97870D5A4B35B59403A11AF084C3_inline((Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_6;
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_7 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 * L_8 = (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)__this->get_predicate_4();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_9 = V_1;
		NullCheck((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tE3A94A0ABEF9C1BD1E6F087A8EDCD8261F969EC2 *)L_8, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 * L_11 = (Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)__this->get_selector_5();
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_12 = V_1;
		NullCheck((Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *, NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t4545DE9C23C37916AA3634FBE978FD83815B3502 *)L_11, (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * L_14 = (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC74390E33EC5E808557C54E1237FEAEE0CE4FF31((Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)(Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m9FDC633F4357AC1164B92A3CB2C3E8CE812E1328_gshared (WhereSelectListIterator_2_t3A7F612901B1B4B80250E56C86F00F85A17DED3A * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,Cone>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mE82849DEB89C46F7369719EFE259210896C15E50_gshared (WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectListIterator_2_Clone_m569C7EB0588B5ABD5866855B06FC65CA78D47DF8_gshared (WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * L_2 = (Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)__this->get_selector_5();
		WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A * L_3 = (WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m2F1D572B024E1E44756DD4E224A3CBD39AF1574A_gshared (WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F * L_11 = (Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t4F47E736DFB680BAAC2C706048481AC14AADEF4F *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m33FF867FB062A64F6F404967EE440E7500C0F849_gshared (WhereSelectListIterator_2_t64FDB198BBB32A0CD5514B9C5EE0980E508DC98A * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m681BB1A337A47B69F6F8AF096B7343E63FEEBFC6_gshared (WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectListIterator_2_Clone_mBC7DB7EC7EF21E9F346B3966D02B896DF10D5674_gshared (WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * L_2 = (Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)__this->get_selector_5();
		WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B * L_3 = (WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m16817A0C61FC9B219031C70AEC9673D9CA6A3EAB_gshared (WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 * L_11 = (Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t2877CFC3633474B6274EEA7482A7A4E70D025CC7 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m84B74AA0133C89FC3F376643BD5642FD56B6FCBD_gshared (WhereSelectListIterator_2_tE76B3C05529FC66D34631E0DF1EC63130353A94B * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCF313A191371C8CCC2E79D89A3BF21714EFDB20E_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m667BCD94E83BB3A02AF2D66E07B089FA86971342_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * L_3 = (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mEE0E8B173345B059100E0736D106FFAE0C2D29CA_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mAC87184664F73DD7F3EC4AB4CE2BDE71BE76249D_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mAE002FCDFC3C9C58D4CA2E4AA6FA099080831FB8_gshared (WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B * __this, List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * ___source0, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate1, Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectListIterator_2_Clone_m2C1276F05F50830CD5730B6B28D53DF9BD126B33_gshared (WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B * __this, const RuntimeMethod* method)
{
	{
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_0 = (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)__this->get_source_3();
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * L_2 = (Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)__this->get_selector_5();
		WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B * L_3 = (WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B *, List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_0, (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_1, (Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m9FF1FDFC4705FA9D9EE494C9E96729214482A384_gshared (WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_3 = (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)__this->get_source_3();
		NullCheck((List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_3);
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  L_4;
		L_4 = ((  Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  (*) (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * L_5 = (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)__this->get_address_of_enumerator_6();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_6;
		L_6 = Enumerator_get_Current_mD465945F85FAA1C7540E90EE3CE3B7420887F4AD_inline((Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_6;
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_7 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_8 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_9 = V_1;
		NullCheck((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE0F030B2CA607826B25983C128370C60E0480620 * L_11 = (Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)__this->get_selector_5();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_12 = V_1;
		NullCheck((Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE0F030B2CA607826B25983C128370C60E0480620 *)L_11, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * L_14 = (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mAE84FC20EE931246715AC403E217A6D8027C32CA((Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m6E4BC8E742526C7EDD78EE2DED86BAAD4EFC3172_gshared (WhereSelectListIterator_2_tB5E3511A9FBCDA9F3262BC1E65D7823E7BFA487B * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m3921E0428E571AA7637C2D30EEF831207DA89BB3_gshared (WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106 * __this, List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * ___source0, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate1, Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectListIterator_2_Clone_m57F91F8F1EA8065A0723D12AF0C4967DD9D1D630_gshared (WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_0 = (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)__this->get_source_3();
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * L_2 = (Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)__this->get_selector_5();
		WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106 * L_3 = (WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106 *, List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_0, (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_1, (Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mCD5CF8213E20662EA0B1E404500E2DF381AA1A19_gshared (WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_3 = (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)__this->get_source_3();
		NullCheck((List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_3);
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  L_4;
		L_4 = ((  Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  (*) (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * L_5 = (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)__this->get_address_of_enumerator_6();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_6;
		L_6 = Enumerator_get_Current_mD465945F85FAA1C7540E90EE3CE3B7420887F4AD_inline((Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_6;
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_7 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_8 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_9 = V_1;
		NullCheck((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 * L_11 = (Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)__this->get_selector_5();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_12 = V_1;
		NullCheck((Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t2BE6EA28D893AEA0F5EBBE9DE341DF3AA2B347A8 *)L_11, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * L_14 = (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mAE84FC20EE931246715AC403E217A6D8027C32CA((Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m46A20068ECBFACF1A6CDDA1B60CCCE63CF2EA687_gshared (WhereSelectListIterator_2_t21162492B84F9CB69E410A70E01642D910E82106 * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mDFF11ADD1FB4BE5CE10047A98F8FC0F5C2A2D770_gshared (WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78 * __this, List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * ___source0, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * ___predicate1, Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mE6D5CB3A2DB7ABA906B4A84B05FEA2952C1002B7_gshared (WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_0 = (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)__this->get_source_3();
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_1 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * L_2 = (Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)__this->get_selector_5();
		WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78 * L_3 = (WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78 *, List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *, Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_0, (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_1, (Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m486B965DCEEE07094D30400719F1AC4EAC6D64BD_gshared (WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 * L_3 = (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)__this->get_source_3();
		NullCheck((List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_3);
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  L_4;
		L_4 = ((  Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3  (*) (List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0535D8DB59E2E1B0673345AD2DFD22348E7CB776 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * L_5 = (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)__this->get_address_of_enumerator_6();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_6;
		L_6 = Enumerator_get_Current_mD465945F85FAA1C7540E90EE3CE3B7420887F4AD_inline((Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_6;
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_7 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 * L_8 = (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)__this->get_predicate_4();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_9 = V_1;
		NullCheck((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t1CC7A8DDC38CCDAB036102F7603F8BD7CB3DB4E4 *)L_8, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF * L_11 = (Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)__this->get_selector_5();
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_12 = V_1;
		NullCheck((Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *, Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5C4AC37AB66B3160BADFC3A9E6DB3742261F1ACF *)L_11, (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * L_14 = (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mAE84FC20EE931246715AC403E217A6D8027C32CA((Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)(Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m8845C73C05B22FDED27565FBD06B7E776FCFDF5B_gshared (WhereSelectListIterator_2_tB517255FF9F77BC50D58C9E6D3575A29F9C73C78 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCFF74CEF47CD0D711FA3797A2C0BB568816A6626_gshared (WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57 * __this, List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___source0, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate1, Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		((  void (*) (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E * WhereSelectListIterator_2_Clone_m3E15DBA706DD75F927AA50EAAB9873836DE28C17_gshared (WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57 * __this, const RuntimeMethod* method)
{
	{
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_0 = (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)__this->get_source_3();
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * L_2 = (Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)__this->get_selector_5();
		WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57 * L_3 = (WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57 *, List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_0, (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_1, (Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mFBD1ACCD8E4DD79B453E6A86A88F786969B7FA52_gshared (WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_3 = (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)__this->get_source_3();
		NullCheck((List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_3);
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  L_4;
		L_4 = ((  Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  (*) (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * L_5 = (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)__this->get_address_of_enumerator_6();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_6;
		L_6 = Enumerator_get_Current_mE46574E114EEEBE327B950F7810EFB7F8E6C1576_inline((Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_6;
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_7 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_8 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_9 = V_1;
		NullCheck((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 * L_11 = (Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)__this->get_selector_5();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_12 = V_1;
		NullCheck((Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)L_11);
		Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  L_13;
		L_13 = ((  Cone_t2D89E976FA9F77ABBF56F2801591CF163F450002  (*) (Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tF2DB8A6E89612D1C03B9E868858604BADBE32F97 *)L_11, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * L_14 = (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m1BB806A0DEBAAC238B444A4A7C450EDAEB278E5B((Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Cone>::Dispose() */, (Iterator_1_t98DBD02EB31CE0846BA0C5AF54BFEE4479810C1E *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,Cone>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m01C9E9DDC57FD50B4FBAE0BE84F660A8738A1CC2_gshared (WhereSelectListIterator_2_tBDA9A95DEB12A16D5918C4CEFFCD35BE23A8AA57 * __this, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F * L_1 = (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9C337FE6A8C6A0F5383A1B21FA9A0DB54BB75E7F *, RuntimeObject*, Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t756AAACCBC92EFA9061295A9A501C1732E1F61B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m34848D5ED38001A5105A2B7C3DD22B75F80F3278_gshared (WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC * __this, List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___source0, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate1, Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		((  void (*) (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 * WhereSelectListIterator_2_Clone_m00ECDC06AA1667D853BEA5026DDA0A8E9E18BD4B_gshared (WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC * __this, const RuntimeMethod* method)
{
	{
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_0 = (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)__this->get_source_3();
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * L_2 = (Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)__this->get_selector_5();
		WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC * L_3 = (WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC *, List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_0, (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_1, (Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m2D469F2969E93F7008B8D6C9D3234B38069BFF6E_gshared (WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_3 = (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)__this->get_source_3();
		NullCheck((List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_3);
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  L_4;
		L_4 = ((  Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  (*) (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * L_5 = (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)__this->get_address_of_enumerator_6();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_6;
		L_6 = Enumerator_get_Current_mE46574E114EEEBE327B950F7810EFB7F8E6C1576_inline((Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_6;
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_7 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_8 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_9 = V_1;
		NullCheck((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 * L_11 = (Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)__this->get_selector_5();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_12 = V_1;
		NullCheck((Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)L_11);
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_13;
		L_13 = ((  InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  (*) (Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tCD0788DEA4A5D3AE722A6A07E134E5E1058CCB26 *)L_11, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * L_14 = (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m1BB806A0DEBAAC238B444A4A7C450EDAEB278E5B((Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_tFE4FBEF60F24745340A1C6BBD86AB8F4A0943507 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m7D4F71B5D12BF28A39B86B19C864A4E68F024C98_gshared (WhereSelectListIterator_2_t6707B2361A2BC1A61335CFB541C0A6566EE645AC * __this, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D * L_1 = (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t3F8D65C4B3C620B4D337C1A76F0E91B56C49DD5D *, RuntimeObject*, Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1CA8E8AB2E1EE887F85B366FCE698EA5148490BF *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m4FC5731B168F88AA4C411E2B3D6AD52E8D3E76A2_gshared (WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC * __this, List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * ___source0, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * ___predicate1, Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m12430E45911BE60EE5132014A326508159D87D51_gshared (WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC * __this, const RuntimeMethod* method)
{
	{
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_0 = (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)__this->get_source_3();
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_1 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * L_2 = (Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)__this->get_selector_5();
		WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC * L_3 = (WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC *, List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *, Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_0, (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_1, (Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mEB6618563BFDD862C2B165F07E882CF54EA8FAB1_gshared (WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 * L_3 = (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)__this->get_source_3();
		NullCheck((List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_3);
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  L_4;
		L_4 = ((  Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57  (*) (List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE4CCA309AAABE93C1BF3806E4C42E0B50C5EA5A2 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * L_5 = (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)__this->get_address_of_enumerator_6();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_6;
		L_6 = Enumerator_get_Current_mE46574E114EEEBE327B950F7810EFB7F8E6C1576_inline((Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_6;
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_7 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 * L_8 = (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)__this->get_predicate_4();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_9 = V_1;
		NullCheck((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t5395D7FBAA2BE603980C097AAEDA325F0CCA4645 *)L_8, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C * L_11 = (Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)__this->get_selector_5();
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_12 = V_1;
		NullCheck((Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *, JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFF549C53C60098E0ABC10D3C1FE76426B52F651C *)L_11, (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * L_14 = (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m1BB806A0DEBAAC238B444A4A7C450EDAEB278E5B((Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)(Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m5BB9F89FD9154200FB71F886C77602CDB381011F_gshared (WhereSelectListIterator_2_tB36C57C0390A4F3C82C30B0BED645E0795A233CC * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WinRT.Interop._IAsyncOperation`2<System.Object,System.Int32Enum>::.ctor(System.Guid,WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void _IAsyncOperation_2__ctor_m8179D12200CAB36C36E0D8FA7023951A7079AEFA_gshared (_IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 * __this, Guid_t  ___iid0, ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * ___obj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Guid_t  L_0 = ___iid0;
		ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * L_1 = ___obj1;
		NullCheck((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85_il2cpp_TypeInfo_var);
		IAsyncOperation__ctor_m92848571124F568682F25CF6C573D87ACE7C6B7F((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this, (Guid_t )L_0, (ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// TTask WinRT.Interop._IAsyncOperation`2<System.Object,System.Int32Enum>::AsTask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * _IAsyncOperation_2_AsTask_mF1B68F71FED15EA2AAB2D69F861970EFC8804B8C_gshared (_IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 * __this, const RuntimeMethod* method)
{
	CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD ));
		CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  L_0 = V_0;
		NullCheck((_IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 *)__this);
		RuntimeObject * L_1;
		L_1 = ((  RuntimeObject * (*) (_IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 *, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((_IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 *)__this, (CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return (RuntimeObject *)L_1;
	}
}
// TTask WinRT.Interop._IAsyncOperation`2<System.Object,System.Int32Enum>::AsTask(System.Threading.CancellationToken)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * _IAsyncOperation_2_AsTask_mE8027B2A1191F8BE1EE23B194B8BADBE3F24A824_gshared (_IAsyncOperation_2_tB87A6D4A1EC2897C93F97775E6331A314CF3AC42 * __this, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  ___cancellationToken0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 * L_0 = (U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1));
		((  void (*) (U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		V_0 = (U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 *)L_0;
		U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 * L_2 = V_0;
		ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * L_3 = (ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A *)((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this)->get__obj_0();
		IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * L_4;
		L_4 = IAsyncInfo_op_Implicit_m75C7504DFE28C5B0635BE1144584F5A435019238((IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444 *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_info_2(L_4);
		U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 * L_5 = V_0;
		TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 * L_6 = (TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_6, (RuntimeObject *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		NullCheck(L_5);
		L_5->set_source_1(L_6);
		U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 * L_7 = V_0;
		CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E * L_8 = (CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E *)il2cpp_codegen_object_new(CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E_il2cpp_TypeInfo_var);
		CompletedHandler__ctor_m9711EB106AA6D15428773BE1F7DF029C2E5D563B(L_8, (RuntimeObject *)L_7, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this);
		IAsyncOperation_set_Completed_mD8ECD437EA2B6D88D9CEA3872A6EA89D45D06AE2((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this, (CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E *)L_8, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 * L_9 = V_0;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_10 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_10, (RuntimeObject *)L_9, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)), /*hidden argument*/NULL);
		CancellationTokenRegistration_t407059AA0E00ABE74F43C533E7D035C4BA451F6A  L_11;
		L_11 = CancellationToken_Register_m6C186260806A5918D17E0B3A5AF2520D8AFF0787((CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD *)(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD *)(&___cancellationToken0), (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t9C6C0AC85F8EAF1B8E61DE9D3C0B009EE786D124 * L_12 = V_0;
		NullCheck(L_12);
		TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 * L_13 = (TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 *)L_12->get_source_1();
		NullCheck((TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 *)L_13);
		Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58 * L_14;
		L_14 = ((  Task_1_t86B94DBC8071781438CF50D65B641E433B2E4C58 * (*) (TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((TaskCompletionSource_1_tFEFD2FEC3619B04450B0DCBC20CD7229C2D07354 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		return (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8)));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WinRT.Interop._IAsyncOperation`2<System.Object,System.Object>::.ctor(System.Guid,WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void _IAsyncOperation_2__ctor_m25E9930EC578ACCCD23503F05131CCBF05E5EBFF_gshared (_IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 * __this, Guid_t  ___iid0, ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * ___obj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Guid_t  L_0 = ___iid0;
		ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * L_1 = ___obj1;
		NullCheck((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85_il2cpp_TypeInfo_var);
		IAsyncOperation__ctor_m92848571124F568682F25CF6C573D87ACE7C6B7F((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this, (Guid_t )L_0, (ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// TTask WinRT.Interop._IAsyncOperation`2<System.Object,System.Object>::AsTask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * _IAsyncOperation_2_AsTask_mB477A8F5B9C39D023518509DFE19BE93CC1F4259_gshared (_IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 * __this, const RuntimeMethod* method)
{
	CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD ));
		CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  L_0 = V_0;
		NullCheck((_IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 *)__this);
		RuntimeObject * L_1;
		L_1 = ((  RuntimeObject * (*) (_IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 *, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((_IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 *)__this, (CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return (RuntimeObject *)L_1;
	}
}
// TTask WinRT.Interop._IAsyncOperation`2<System.Object,System.Object>::AsTask(System.Threading.CancellationToken)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * _IAsyncOperation_2_AsTask_mDBBEE29AACBC65237CF53EA3FEE81EB8C2566522_gshared (_IAsyncOperation_2_tFCD97D6EC0B2EFDA188B9BBC632E39B33EE298D0 * __this, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  ___cancellationToken0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC * L_0 = (U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1));
		((  void (*) (U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		V_0 = (U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC *)L_0;
		U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC * L_2 = V_0;
		ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A * L_3 = (ObjectReference_1_t9BB15B4AC4A60497A8C5149E575D03ED44A82D1A *)((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this)->get__obj_0();
		IAsyncInfo_t3E0C2558C801802AB18ABA624E9C0FFE58A4C3F9 * L_4;
		L_4 = IAsyncInfo_op_Implicit_m75C7504DFE28C5B0635BE1144584F5A435019238((IObjectReference_tED3DABF84842524EBCF295C6DAEAFDA6A8907444 *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_info_2(L_4);
		U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC * L_5 = V_0;
		TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 * L_6 = (TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_6, (RuntimeObject *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		NullCheck(L_5);
		L_5->set_source_1(L_6);
		U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC * L_7 = V_0;
		CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E * L_8 = (CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E *)il2cpp_codegen_object_new(CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E_il2cpp_TypeInfo_var);
		CompletedHandler__ctor_m9711EB106AA6D15428773BE1F7DF029C2E5D563B(L_8, (RuntimeObject *)L_7, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this);
		IAsyncOperation_set_Completed_mD8ECD437EA2B6D88D9CEA3872A6EA89D45D06AE2((IAsyncOperation_tA80414290354748068361438EEDA1D9095173C85 *)__this, (CompletedHandler_t779051CC9CC4774B284CB361025E333974D6614E *)L_8, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC * L_9 = V_0;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_10 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_10, (RuntimeObject *)L_9, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)), /*hidden argument*/NULL);
		CancellationTokenRegistration_t407059AA0E00ABE74F43C533E7D035C4BA451F6A  L_11;
		L_11 = CancellationToken_Register_m6C186260806A5918D17E0B3A5AF2520D8AFF0787((CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD *)(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD *)(&___cancellationToken0), (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t9702B979FBBF0E2A5ED1D1DA55EDDA39B060B6EC * L_12 = V_0;
		NullCheck(L_12);
		TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 * L_13 = (TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 *)L_12->get_source_1();
		NullCheck((TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 *)L_13);
		Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * L_14;
		L_14 = ((  Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * (*) (TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((TaskCompletionSource_1_t5B48A13B0469AA5A5797B645926E284436099903 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		return (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8)));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  Enumerator_get_Current_m69138BD9C0AD36EE1BBAD1ADADD51E73B18701F8_gshared_inline (Enumerator_t8BF2697435D9CF5251CA2FB07B33720AAA6FAACC * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29  L_0 = (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )__this->get_current_3();
		return (KeyValuePair_2_t1003AEB72CC963485254D1DCCDFFBAE8A557AA29 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  Enumerator_get_Current_mDB640EBE69963C260FD4F0B3E6DCED931E25E9B8_gshared_inline (Enumerator_t78081EAEAD9A171D08540877014D7BBF1B131163 * __this, const RuntimeMethod* method)
{
	{
		InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9  L_0 = (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )__this->get_current_3();
		return (InternedString_tCD9FB98956EDF6D07A8ABF907C05C5B5257C08F9 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  Enumerator_get_Current_mBB0E30FA944F0649C529C687487757B90F3A5437_gshared_inline (Enumerator_t8591220E45759115A0AD5CB868ADF66B82BB1695 * __this, const RuntimeMethod* method)
{
	{
		NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97  L_0 = (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )__this->get_current_3();
		return (NameAndParameters_t9D863B90629C415709603F4790ADFCC528D62A97 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  Enumerator_get_Current_mF73E9D073C9D97870D5A4B35B59403A11AF084C3_gshared_inline (Enumerator_t11C30DD862794455DEDF2E50AD884F5B0B9CD1DE * __this, const RuntimeMethod* method)
{
	{
		NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB  L_0 = (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )__this->get_current_3();
		return (NamedValue_t8409537CEF1BC4772888197B94E1D9D80A986DDB )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  Enumerator_get_Current_mD465945F85FAA1C7540E90EE3CE3B7420887F4AD_gshared_inline (Enumerator_t1B75F10E1C53031684D5518F60C1F01B97EB6EC3 * __this, const RuntimeMethod* method)
{
	{
		Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02  L_0 = (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )__this->get_current_3();
		return (Substring_t7D9B38B83507DB3B8802FFFB252076BEBE410D02 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  Enumerator_get_Current_mE46574E114EEEBE327B950F7810EFB7F8E6C1576_gshared_inline (Enumerator_t7FD5E664A073438DC196C43A2FFE6E7BB50D0E57 * __this, const RuntimeMethod* method)
{
	{
		JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D  L_0 = (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )__this->get_current_3();
		return (JsonValue_tC28F8BE32DB0989A26463A5C5CB95EEC2A4F1E1D )L_0;
	}
}
