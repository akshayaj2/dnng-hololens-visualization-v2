using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Windows.WebCam;
using System.Linq;
using Microsoft.MixedReality.Toolkit.UI;

public class HandMenuController : MonoBehaviour
{
    //Script handles some toggling features in the hand menu
    public GameObject advancedWindow;
    public GameObject chart;
    public GameObject arrow;

    public void OpenAdvanced() {
        advancedWindow.SetActive(!advancedWindow.activeSelf);
    }

    public void OpenChart() {
        chart.transform.position = transform.TransformPoint(new Vector3(0.2f, 0,0));
        chart.transform.rotation = transform.rotation;

        chart.SetActive(!chart.activeSelf);
        if (chart.activeSelf)
        {
            gameObject.GetComponent<DrawHistogram>().UpdateGraph();
        }
    }

    public void ToggleFastMode()
    {
        arrow.SetActive(!arrow.activeSelf);
    }

}
