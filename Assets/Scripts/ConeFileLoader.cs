using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeFileLoader : MonoBehaviour
{
    public List<TextAsset> data;
    private int idx = 0;

    // Used to pre-load data for demo purposes, currently contains cone data from RL52
    public void Start() {
        LoadNextFile();
    }

    public void LoadNextFile() {
        FindObjectOfType<DrawCones>().ClearCones();
        FindObjectOfType<DrawCones>().LoadJson(data[idx].text);
        idx++;
        if (idx >= data.Count)
            idx = 0;
    }
}
