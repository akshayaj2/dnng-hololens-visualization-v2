using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

using Microsoft.MixedReality.OpenXR;
using Microsoft.MixedReality.QR;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Utilities;

public class QRTracker : MonoBehaviour
{
    QRCodeWatcher watcher;
    QRCode code;

    Vector3 codePos;
    Quaternion codeRot;

    private float zOffset = -0.098f;
    
    async void Start()
    {
        QRCodeWatcherAccessStatus status = QRCodeWatcherAccessStatus.DeniedBySystem;
        status = await QRCodeWatcher.RequestAccessAsync(); // Wait for permission

        watcher = new QRCodeWatcher(); // Create QR code watcher

        watcher.Added += Watcher_Added;
        watcher.Updated += Watcher_Updated;

        if (status == QRCodeWatcherAccessStatus.Allowed)
            watcher.Start(); // Start watching for QR codes
    }

    void Watcher_Added(object sender, QRCodeAddedEventArgs args) {
        try {
            code = args.Code;
            if (code.Data == "https://dnng.engin.umich.edu/" || true) { // Determines what QR codes will work, remove "true" if you want to restrict to a specific one

                Pose pose;
                SpatialGraphNode.FromStaticNodeId(code.SpatialGraphNodeId).TryLocate(FrameTime.OnUpdate, out pose);
                Reposition(pose, code.PhysicalSideLength);
            }
        } catch (Exception e) { }
    }
    void Watcher_Updated(object sender, QRCodeUpdatedEventArgs args) {
        try {
            if (code.Data == "https://dnng.engin.umich.edu/" || true) { // Remove "true" if you want to restrict QR detection to a specific code
                code = args.Code;

                Pose pose;
                SpatialGraphNode.FromStaticNodeId(code.SpatialGraphNodeId).TryLocate(FrameTime.OnUpdate, out pose);
                Reposition(pose, code.PhysicalSideLength);
            }
        } catch (Exception e) { }
    }

    void Reposition(Pose pose, float sideLength) {
        transform.position = pose.position;
        transform.forward = pose.rotation * Vector3.up;

        transform.position += transform.up * zOffset;
        transform.position += transform.forward * (sideLength/2.0f);
        transform.position -= transform.right * (sideLength/2.0f);
    }
}
