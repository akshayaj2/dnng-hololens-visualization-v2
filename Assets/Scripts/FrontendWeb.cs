using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

[System.Serializable]
public class DirectionResults
{
    public float[] results;

    public float maxAngle()
    {
        return (float)Array.IndexOf(results, results.Max());
    }
}

// Reference: https://gist.github.com/define-private-public/d05bc52dd0bed1c4699d49e2737e80e7
// Also see: https://github.com/paulbatum/WebSocket-Samples/blob/master/HttpListenerWebSocketEcho/Server/Server.cs
//
// Note there is a visual studio bug with this:
//  - https://forum.unity.com/threads/web-socket-client-disconnecting-exception-being-thrown-in-uwp-builds.593422/
public class FrontendWeb : MonoBehaviour
{
    [System.NonSerialized]
    public HttpListener listener;
    public string url;

    private string pageData;

    public string json;

    private CancellationTokenSource cancellationTokenSource;

    async Task HandleConnections()
    {
        CancellationToken token = cancellationTokenSource.Token;

        while (true)
        {
            if (token.IsCancellationRequested)
                break;

            HttpListenerContext ctx = await listener.GetContextAsync();

            var req = ctx.Request;
            var resp = ctx.Response;

            // Handle Lines Toggled
            try
            {
                if (req.HttpMethod == "POST")
                {
                    if (req.Url.AbsolutePath == "/toggleLines")
                    {
                        FindObjectOfType<DrawLines>().ToggleLines();
                    }
                    else if (req.Url.AbsolutePath == "/toggleMesh")
                    {
                        FindObjectOfType<ToggleMesh>().Toggle();
                    }
                    else if (req.Url.AbsolutePath == "/uploadCones" && req.HasEntityBody)
                    {
                        System.IO.Stream body = req.InputStream;
                        System.Text.Encoding encoding = req.ContentEncoding;
                        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);

                        string json = reader.ReadToEnd();

                        body.Close();
                        reader.Close();

                        FindObjectOfType<DrawCones>().LoadJson(json);
                    }
                    else if (req.Url.AbsolutePath == "/clearCones")
                    {
                        FindObjectOfType<DrawCones>().ClearCones();
                    }
                    else if (req.Url.AbsolutePath == "/direction" && req.HasEntityBody)
                    {
                        System.IO.Stream body = req.InputStream;
                        System.Text.Encoding encoding = req.ContentEncoding;
                        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);

                        json = reader.ReadToEnd();

                        body.Close();
                        reader.Close();

                        // FIXME: Make own class, handle ring
                        DirectionResults d = JsonUtility.FromJson<DirectionResults>("{\"results\":" + json + "}");

                        float x = (float)(d.results[0]);
                        float y = (float)(d.results[1]);
                        float z = (float)(d.results[2]);

                        GameObject sphereObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        sphereObject.transform.localPosition = new Vector3(x, y, z);
                        sphereObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

                        GameObject arrow = GameObject.Find("arrow");

                        var pointInAir = (new Vector3(x, y, z) - new Vector3(0, 0, 0)).normalized;
                        var rotation_obj = Quaternion.LookRotation(pointInAir);
                        Debug.Log(rotation_obj);

                        arrow.transform.rotation = Quaternion.Slerp(arrow.transform.rotation, rotation_obj, Time.deltaTime);

                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
            }

            // Send webpage
            byte[] data = Encoding.UTF8.GetBytes(pageData);
            resp.ContentType = "text/html";
            resp.ContentEncoding = Encoding.UTF8;
            resp.ContentLength64 = data.LongLength;

            await resp.OutputStream.WriteAsync(data, 0, data.Length);
            resp.Close();
        }
    }

    async void Start()
    {
        GameObject arrow = GameObject.Find("arrow");
        arrow.transform.localEulerAngles = new Vector3(0, 0, 0);

        // Load HTML
        pageData = Resources.Load<TextAsset>("frontend").text;

        // Start listener
        listener = new HttpListener();
        listener.Prefixes.Add(url);
        listener.Start();

        cancellationTokenSource = new CancellationTokenSource();
        await HandleConnections();

        listener.Close();
    }

    void OnApplicationQuit()
    {
        listener.Stop();
        cancellationTokenSource.Cancel();
    }
}
