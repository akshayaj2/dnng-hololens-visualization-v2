using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ArrowPointer : MonoBehaviour
{
    float rotationTime = 1;
    Vector3 currentEulerAngles;
    public float angle = 360.0f;
    float currentAngle = 0;
    float startingAngle;
    float startTime;
    float speed;

    // Constantly points the arrow in the direction of where fast-mode predicts the source is
    void Update()
    {

        float x = -0.76372457f;
        float y = 1.7807254f;
        float z = -0.31621593f;

        // Calculate phi (new altitude angle):
        float phi = 0;

        if (x > 0)
        {
            phi = (float)(Math.Atan(y / x));
        }
        else if (x < 0 && y >= 0)
        {
            phi = (float)(Math.Atan(y / x) + Math.PI);
        }
        else
        {
            phi = (float)(Math.Atan(y / x) - Math.PI);
        }

        phi = (float)((180 * phi) / (Math.PI));


        float set_altitude = phi;

        float norm = (float)(Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2)));
        float rho = (float)(Math.Acos(z / norm));
        rho = (float)((180 * rho) / (Math.PI));

        float set_azimuth = rho;

        Debug.Log("set azimuth: " + set_azimuth);
        Debug.Log("set altitude: " + set_altitude);

        // frame of reference: arrow at front of box, straightahead pointed into box is (0,0,0)
        // arrow pointed right is +90.0 azimuth
        // arrow pointed down is +90.0 altitude
        // transform.localEulerAngles = new Vector3(set_altitude, set_azimuth, 0);

        //var pointInAir = (new Vector3(-0.76372457f, 1.7807254f, -0.31621593f) - transform.position).normalized;
        /*var pointInAir = (new Vector3(-0.76372457f, 1.7807254f, -0.31621593f) - new Vector3(0, 0, 0)).normalized;
        var rotation_obj = Quaternion.LookRotation(pointInAir);
        Debug.Log(rotation_obj);

        transform.rotation = Quaternion.Slerp(transform.rotation, rotation_obj, Time.deltaTime);*/

        //transform.localEulerAngles = new Vector3(0, 0, 0);
    }

    /*void OnGUI()
    {
        GUI.Label(new Rect(100, 100, 50, 50), "Hello World!");
    }*/

    void Start()
    {
        float x_init = -0.76372457f;
        float y_init = 1.7807254f;
        float z_init = -0.31621593f;

        transform.localEulerAngles = new Vector3(0, 0, 0);

        /*GameObject sphereObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphereObject.transform.localPosition = new Vector3(x_init, y_init, z_init);
        sphereObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);*/

    }
}
