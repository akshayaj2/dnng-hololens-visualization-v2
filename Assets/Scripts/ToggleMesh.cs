using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleMesh : MonoBehaviour
{
    public Material meshMat;
    bool toggled = true;
    
    public void Toggle() {
        toggled = !toggled;
        meshMat.SetColor("_WireColor", toggled ? Color.white : Color.black);
    }
}
