using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using System.Linq;

public class DrawLines : MonoBehaviour
{
    public Material debugLineMat;
    public Mesh lineMesh;
    
    const float MIN_SIZE = 0.99f;
    const float LINE_WIDTH = 0.005f;

    float[,] rows;

    bool showLines = true;

    public void LoadData(NativeArray<Color> data, float max) {
        rows = new float[360, 360];
        for (int x = 0; x < 360; x++) {
            for (int y = 0; y < 360; y++) {
                rows[x, y] = data[x + y * 360].r/max;
            }
        }
    }

    void Update() {
        if (rows == null)
            return;

        List<Matrix4x4> lines = new List<Matrix4x4>();
        //List<Vector4> colors = new List<Vector4>();
        
        for (int x = 0; x < rows.GetLength(0); x+=2) {
            for (int y = 0; y < rows.GetLength(1); y+=2) {
                float magnitude = Mathf.Pow(rows[x, y], 2.0f);
                //Color magnitudeColor = Color.Lerp(Color.black, Color.white, magnitude);
                if (magnitude < MIN_SIZE || lines.Count >= 1023)
                    continue;

                float yAng = ((float)y/((float)rows.GetLength(0) - 1.0f)) * 180.0f;
                yAng -= 90.0f;
                float xAng = ((float)x/((float)rows.GetLength(1) - 1.0f)) * 360.0f;
                xAng -= 180.0f;

                Vector3 direction = -Vector3.forward;
                direction = Quaternion.AngleAxis(yAng, Vector3.right) * direction; // Rotate around X-Axis
                direction = Quaternion.AngleAxis(xAng, Vector3.up) * direction; // Rotate around Y-Axis
                direction = transform.TransformDirection(direction); // Transform from local space to world space

                RaycastHit hit; // Lines will stop at the hololens's spacial mesh
                if (!Physics.Raycast(transform.position + direction * 0.3f, direction,
                                     out hit, 50.0f, LayerMask.GetMask("Spatial Awareness"))) {
                    // hit.point = transform.position + direction * 100.0f;
                    continue;
                }

                Vector3 startPoint = transform.position + direction * 0.3f;
                Vector3 midpoint = Vector3.Lerp(hit.point, startPoint, 0.5f);
                float length = Vector3.Distance(hit.point, startPoint);

                Vector3 forwards = Vector3.Cross(Vector3.Cross(Camera.main.transform.position - transform.position, direction), direction);
                Quaternion rot = Quaternion.identity;
                if (forwards.sqrMagnitude != 0 && direction.sqrMagnitude != 0)
                    rot = Quaternion.LookRotation(forwards, direction);
                float widthMultiplier = 0.5f + magnitude;
                Matrix4x4 m = Matrix4x4.TRS(midpoint, rot,
                                            new Vector3(LINE_WIDTH * widthMultiplier, length, LINE_WIDTH * widthMultiplier));
                lines.Add(m);
                //colors.Add(new Vector4(magnitudeColor.r, magnitudeColor.g, magnitudeColor.b, 1.0f));
            }
        }

        if (lines.Count > 0) {
            MaterialPropertyBlock lineProperties = new MaterialPropertyBlock();
            //lineProperties.SetVectorArray("_EndColor", colors);
            if (showLines)
                Graphics.DrawMeshInstanced(lineMesh, 0, debugLineMat, lines, lineProperties);
        }
    }

    public void ToggleLines() {
        showLines = !showLines;
    }
}
