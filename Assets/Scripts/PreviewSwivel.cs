using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewSwivel : MonoBehaviour
{
    public float speed = 1.0f;

    void Update()
    {
        Vector3 cameraDir = transform.position - Camera.main.transform.position;
        cameraDir.y = 0;

        if (cameraDir != Vector3.zero) {
            transform.rotation = Quaternion.Lerp(
                transform.rotation,
                Quaternion.LookRotation(cameraDir),
                Time.deltaTime * speed
            );
        }
    }
}
