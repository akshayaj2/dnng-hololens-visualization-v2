using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorMap : MonoBehaviour
{
    public List<Texture> colorMaps;
    private int index = 0;

    void Start() {
        Apply();
    }

    // The function that is actually used by the cycle button from the hand menue
    public void Cycle() {
        index++;
        if (index >= colorMaps.Count)
            index = 0;
        Apply();
    }

    public void ChangeTo(int value)
    {
        index = value;
        Apply();
    }

    // Changes the texture to the correct colormap
    void Apply() {
        if (index >= colorMaps.Count || index < 0)
            return;

        Shader.SetGlobalTexture("_Colormap", colorMaps[index]);
    }
}
