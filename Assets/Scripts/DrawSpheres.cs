using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using System.Linq;

public class DrawSpheres : MonoBehaviour
{
    public Material sphereMat;
    public float size = 1.0f;
    public int sphereCount = 512;

    bool showSpheres = true;

    Mesh sphereMesh;
    List<Matrix4x4> sphereMatrices;
    void Start() {
        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        this.sphereMesh = Instantiate(obj.GetComponent<MeshFilter>().mesh);
        Destroy(obj);
        RecalculateMatrices();
    }
    void OnValidate() {
        RecalculateMatrices();
    }

    void RecalculateMatrices() {
        sphereMatrices = new List<Matrix4x4>();
        for (int i = 1; i <= sphereCount; i++) {
            float scale = (float)i/(float)sphereCount;
            sphereMatrices.Add(Matrix4x4.TRS(
                                   transform.position,
                                   Quaternion.identity,
                                   Vector3.one * scale * size * (1 - (Time.time % 1))
                               ));
        }
    }


    // Projects a beam into 3D space by drawing many partial speheres
    Vector3 lastPosition = Vector3.zero;
    void Update() {
        if (showSpheres) {
            MaterialPropertyBlock sphereProperties = new MaterialPropertyBlock();
            Graphics.DrawMeshInstanced(sphereMesh, 0,
                                       sphereMat, sphereMatrices,
                                       sphereProperties);
        }
        if (transform.position != lastPosition || true) {
            lastPosition = transform.position;
            RecalculateMatrices();
        }
    }

    public void ToggleSpheres() {
        showSpheres = !showSpheres;
    }
}
