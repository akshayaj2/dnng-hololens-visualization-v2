using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

public struct Cone {
    public Vector3 lever_arm;

    public Vector3 x_1;
    public Vector3 x_2;

    public Vector3 x_1_uncertainty;
    public Vector3 x_2_uncertainty;

    public float alpha;
    public float alpha_uncertainty;

    public float energy;

    public uint gamma; // Not boolean because boolean isn't blittable
}
[System.Serializable]
public class ConeJson {
    public float[] lever_arm;

    public float[] x_1;
    public float[] x_2;

    public float[] x_1_uncertainty;
    public float[] x_2_uncertainty;

    public float alpha;
    public float alpha_uncertainty;

    public float energy;

    public bool gamma;

    public Cone ToCone() {
        Cone c = new Cone();

        c.lever_arm = new Vector3(lever_arm[0], lever_arm[1], lever_arm[2]);

        c.x_1 = new Vector3(x_1[0], x_1[1], x_1[2]);
        c.x_2 = new Vector3(x_2[0], x_2[1], x_2[2]);

        c.x_1_uncertainty = new Vector3(x_1_uncertainty[0], x_1_uncertainty[1], x_1_uncertainty[2]);
        c.x_2_uncertainty = new Vector3(x_2_uncertainty[0], x_2_uncertainty[1], x_2_uncertainty[2]);

        c.alpha = alpha;
        c.alpha_uncertainty = alpha_uncertainty;

        c.energy = energy;

        c.gamma = gamma ? (uint)1 : (uint)0;

        return c;
    }
}
[System.Serializable]
public class ConeCollection {
    public ConeJson[] cones;
}

public class DrawCones : MonoBehaviour
{
    public ComputeShader coneShader;
    // private ComputeBuffer maxBuffer;

    public RenderTexture neutronAccumulator;
    public RenderTexture gammaAccumulator;

    [System.NonSerialized]
    public List<Cone> neutronCones;
    [System.NonSerialized]
    public List<Cone> gammaCones;

    private ComputeBuffer neutronConeBuffer;
    private ComputeBuffer gammaConeBuffer;

    [System.NonSerialized]
    public bool displayGamma = true;

    private bool doAccumulation = true;

    void Start() {
        ClearCones();
    }

    public void ToggleGamma() {
        this.displayGamma = !this.displayGamma;

        Compute(neutronAccumulator, neutronConeBuffer, !displayGamma);
        Compute(gammaAccumulator, gammaConeBuffer, displayGamma);
    }
    public void ToggleAccumulation() {
        this.doAccumulation = !this.doAccumulation;

        Compute(neutronAccumulator, neutronConeBuffer, !displayGamma);
        Compute(gammaAccumulator, gammaConeBuffer, displayGamma);
    }

    public void ClearAccumulators() {
        neutronAccumulator = new RenderTexture(360, 360, 0, RenderTextureFormat.ARGBFloat);
        neutronAccumulator.enableRandomWrite = true;
        neutronAccumulator.Create();

        gammaAccumulator = new RenderTexture(360, 360, 0, RenderTextureFormat.ARGBFloat);
        gammaAccumulator.enableRandomWrite = true;
        gammaAccumulator.Create();
    }
    public void ClearCones() {
        neutronCones = new List<Cone>();
        gammaCones = new List<Cone>();

        ClearAccumulators();
    }
    public void LoadJson(string json)
    {
        Cone dummy = new Cone();
        dummy.lever_arm = new Vector3();
        dummy.x_1 = new Vector3();
        dummy.x_2 = new Vector3();
        dummy.x_1_uncertainty = new Vector3();
        dummy.x_2_uncertainty = new Vector3();
        dummy.alpha = 0;
        dummy.alpha_uncertainty = 0;
        dummy.gamma = 2;

        List<Cone> newNeutronCones = new List<Cone>() {dummy};
        List<Cone> newGammaCones = new List<Cone>() {dummy};

        ConeCollection c = JsonUtility.FromJson<ConeCollection>("{\"cones\":" + json + "}");
        foreach (Cone cone in c.cones.Select(cj => cj.ToCone())) {
            if (cone.gamma == 1) {
                gammaCones.Add(cone);
                newGammaCones.Add(cone);
            } else {
                neutronCones.Add(cone);
                newNeutronCones.Add(cone);
            }
        }
        Debug.Log($"Gamma Cones: {gammaCones.Count}, Neutron Cones: {neutronCones.Count}");

        // maxBuffer = new ComputeBuffer(cones.Count, 4);
        // maxBuffer.SetData(Enumerable.Repeat((UInt32)0, cones.Count).ToList());

        int stride = 12 * 5 + 4 + 4 + 4 + 4; // 12 bytes per float3
        if (gammaConeBuffer != null) gammaConeBuffer.Release();
        if (neutronConeBuffer != null) neutronConeBuffer.Release();
        gammaConeBuffer = new ComputeBuffer(newGammaCones.Count, stride);
        gammaConeBuffer.SetData(newGammaCones);
        neutronConeBuffer = new ComputeBuffer(newNeutronCones.Count, stride); // 12 bytes per float3
        neutronConeBuffer.SetData(newNeutronCones);

        if (!this.doAccumulation)
            ClearAccumulators();

        Compute(neutronAccumulator, neutronConeBuffer, !displayGamma);
        Compute(gammaAccumulator, gammaConeBuffer, displayGamma);
    }

    void Compute(RenderTexture accumulator, ComputeBuffer coneBuffer, bool assign = false) {
        if (coneBuffer == null || accumulator == null)
            return;

        int kernel = coneShader.FindKernel("CSMain");

        coneShader.SetFloat("_Time", Time.time);
        coneShader.SetBuffer(kernel, "_Cones", coneBuffer);
        coneShader.SetTexture(kernel, "_Result", accumulator);
        // coneShader.SetBuffer(kernel, "_MaxBuffer", maxBuffer);

        coneShader.Dispatch(kernel, 45, 45, 1); // 360/8 = 45

        if (assign) {
            AsyncGPUReadback.Request(accumulator, 0, (AsyncGPUReadbackRequest r) => {
                float max = 0.0f;
                float min = float.MaxValue;

                NativeArray<Color> data = r.GetData<Color>(0);
                for (int i = 0; i < data.Length; i++) {
                    max = Mathf.Max(max, data[i].r);
                    min = Mathf.Min(min, data[i].r);
                }

                Shader.SetGlobalFloat("_Max", max);
                Shader.SetGlobalFloat("_Min", min);

                FindObjectOfType<DrawLines>().LoadData(data, max);
            });

            AssignTextures(accumulator);
        }
    }

    void AssignTextures(RenderTexture accumulator) {
        Shader.SetGlobalTexture("_Heatmap", accumulator);
    }
}
