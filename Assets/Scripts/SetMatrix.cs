using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMatrix : MonoBehaviour
{
    public string property;
    private int propertyID;

    void Start() {
        propertyID = Shader.PropertyToID(property);
    }

    void Update()
    {
        //Matrix4x4 m = Matrix4x4.TRS(transform.position, transform.rotation, transform.localScale);
        Shader.SetGlobalMatrix(propertyID, transform.worldToLocalMatrix);
    }
}
