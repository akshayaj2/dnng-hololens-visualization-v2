using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using TMPro;
public class DrawHistogram : MonoBehaviour
{
    public Texture2D histTex;
    List<Cone> cones;
    public DrawCones coneHandler;
    public List<TextMesh> x_labels;
    public List<TextMesh> y_labels;
    public TextMesh title;

    void Start() {
        UpdateGraph();
    }

    // Updates the histogram every time the chart or neutron/gamma mod is toggled
    public void UpdateGraph() {
        int binCount = 100;
        float maxMev;

        if (coneHandler.displayGamma){
            cones = coneHandler.gammaCones;
            maxMev = 1.5f;
            title.text = "Gamma Energy Distribution";
        }else{
            cones = coneHandler.neutronCones;
            maxMev = 5.0f;
            title.text = "Neutron Energy Distribution";
        }

        List<float> energyData = new List<float>();
        for (int i = 0; i < cones.Count; i++) {
            energyData.Add(cones[i].energy);
        }

        List<int> bins = ProcessBins(energyData, binCount, maxMev);
        Draw(bins, maxMev);
    }
    /*List<float> GenerateRandom(float mean, float stdDev, int count) {
        List<float> output = new List<float>();
        var rand = new System.Random(); //reuse this if you are generating many

        for (int i = 0; i < count; i++) {
            float u1 = 1.0 - rand.NextFloat(); //uniform(0,1] random floats
            float u2 = 1.0 - rand.NextFloat();
            float randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
            output.Add(Math.Abs(mean + stdDev * randStdNormal)); //random normal(mean,stdDev^2)
        }

        return output;
    }*/
    public List<int> ProcessBins(List<float> data, int binCount, float maxMev) {
        if (data.Count == 0) {
            return new List<int>() {};
        }

        List<int> result = new List<int>(binCount);
        List<float> binCutOffs = new List<float>(binCount);

        // No statistics are required, bins are hard coded now
        /*data.Sort();
        float q1 = data[data.Count / 4]; 
        float q2 = data[data.Count / 2];
        float q3 = data[data.Count * 3 / 4];
        float IQR = q3 - q1;
        for (int i = data.Count - 1; data[i] > q2 + 1.5 * IQR; i--) // remove all outliers
        {
            data.RemoveAt(i);
        }
        max = data[data.Count - 1];*/

        for (int i = 0; i < binCount; i++) {
            binCutOffs.Add(i * maxMev / binCount);
            result.Add(0);
        }

        for (int i = 0; i < data.Count; i++) {
            if (data[i] < maxMev) {
                result[(int)((data[i] * binCount) / maxMev)]++;
            }
        }
        return result;
    }
    public void Draw(List<int> histogram, float maxMev) {
        if (histogram.Count == 0)
        {
            return;
        }
        //Doing some rounding to make the y-axis look nice
        int maxCount = (int)Math.Log10(histogram.Max());
        maxCount = (int)Math.Pow(10, maxCount);
        maxCount *= (int)(2 * histogram.Max() / maxCount) + 1;
        maxCount /= 2;
        //maxCount = histogram.Max();
        Clear();
        DrawHorizontalRect(50, 70, 490, Color.black);
        DrawHorizontalRect(50, 460, 490, Color.black);
        DrawVerticalRect(50, 70, 460, Color.black);
        DrawVerticalRect(490, 70, 460, Color.black);

        // Draw vertical and horizontal axis
        for (int i = 0; i < 5; i++) {
            DrawVerticalRect(50 + (i+1) * 88, 70, 65, Color.black);
            float x_num = maxMev * (i + 1) / 5;
            string x_lable = Math.Round(x_num, 2).ToString();
            x_labels[i].text = x_lable;

            DrawHorizontalRect(50, 70 + (i + 1) * 78, 45, Color.black);
            float y_num = maxCount * (i + 1) / 5;
            string y_lable = Math.Round(y_num, 3).ToString();
            y_labels[i].text = y_lable;
        }

        // Draw bars
        for (int i = 0; i < histogram.Count; i++) {
            float unit = (float)histogram[i] / (float)maxCount;
            int width = 440 / histogram.Count;
            int height = (int)(390 * unit);
            DrawVerticalRect(i * width + 50, 70, height + 70, Color.blue, width);
        }

        histTex.Apply();
    }
    void Clear() {
        for (int i = 0; i < 512; i++) {
            for (int j = 0; j < 512; j++) {
                histTex.SetPixel(i,j, Color.white);
            }
        }
    }
    void DrawHorizontalRect(int x1, int y1, int x2, Color color, int width = 1) {
        if (x1 > x2) {
            int temp = x1;
            x1 = x2;
            x2 = temp;
        }
        for (int i = x1; i <= x2; i++) {
            for (int j = 0; j < width; j++) {
                histTex.SetPixel(i, y1 + j, color);
            }
        }
    }
    void DrawVerticalRect(int x1, int y1, int y2, Color color, int width = 1) {
        if (y1 > y2) {
            int temp = y1;
            y1 = y2;
            y2 = temp;
        }
        for (int i = y1; i <= y2; i++) {
            for (int j = 0; j < width; j++) {
                histTex.SetPixel(x1 + j, i, color);
            }
        }
    }

}
