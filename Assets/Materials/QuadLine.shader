Shader "Unlit/QuadLine"
{
    Properties
    {
        _StartColor("Start Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _EndColor("End Color", Color) = (1.0, 1.0, 1.0, 1.0)
    }

    SubShader
    {
        Pass
        {
            Name "Main"
            Tags{ "RenderType" = "Opaque" }

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata_t
            {
                fixed4 vertex : POSITION;
                fixed4 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                fixed4 vertex : SV_POSITION;
                fixed4 color : COLOR0;
                fixed4 uv : TEXCOORD0;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            sampler2D _Colormap;

            UNITY_INSTANCING_BUFFER_START(Props)
            UNITY_DEFINE_INSTANCED_PROP(float4, _StartColor)
            UNITY_DEFINE_INSTANCED_PROP(float4, _EndColor)
            UNITY_INSTANCING_BUFFER_END(Props)

            v2f vert(appdata_t v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);//mul(UNITY_MATRIX_VP, mul(_ParentLocalToWorldMatrix, mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1.0))));

                if (v.uv.y < 0.5) {
                    o.color = tex2Dlod(_Colormap, float4(0.01, 0.5, 0, 0));
                } else {
                    o.color = tex2Dlod(_Colormap, float4(0.99, 0.5, 0, 0));
                }

                o.uv = v.uv;

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                /* if (i.uv.y < 0.1) { */
                /*     if (frac(i.uv.y * 100) < 0.5) { */
                /*         clip(-1); */
                /*     } */
                /* } */
                return i.color;
            }

            ENDCG
        }
    }
}
