Shader "Unlit/SpherePreview"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        /* Tags { "Queue"="Transparent" "RenderType"="Transparent" } */
        LOD 100
        Cull Off

        /* ZWrite Off */
        /* Blend SrcAlpha OneMinusSrcAlpha */

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 worldPos : TEXCOORD1;
                float4 screenPos : TEXCOORD2;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            #define PI 3.14159

            sampler2D _Heatmap;
            sampler2D _Colormap;

            float4x4 _SensorRootMatrix;

            float _Max;
            float _Min;

            float isDithered(float2 pos, float alpha) {
                pos *= _ScreenParams.xy;
                
                // Define a dither threshold matrix which can
                // be used to define how a 4x4 set of pixels
                // will be dithered
                float DITHER_THRESHOLDS[16] =
                    {
                        1.0 / 17.0,  9.0 / 17.0,  3.0 / 17.0, 11.0 / 17.0,
                        13.0 / 17.0,  5.0 / 17.0, 15.0 / 17.0,  7.0 / 17.0,
                        4.0 / 17.0, 12.0 / 17.0,  2.0 / 17.0, 10.0 / 17.0,
                        16.0 / 17.0,  8.0 / 17.0, 14.0 / 17.0,  6.0 / 17.0
                    };
                
                int index = (int(pos.x) % 4) * 4 + int(pos.y) % 4;
                return alpha - DITHER_THRESHOLDS[index];
            }

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.screenPos = ComputeScreenPos(o.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
                // Heatmap
                float4 pos = mul(_SensorRootMatrix, i.worldPos);
                float3 dir = normalize(pos.xyz);
                float azimuth = atan2(dir.x, dir.z) + PI;
                azimuth += PI;
                azimuth = (azimuth % (2 * PI))/(2 * PI);
                float altitude = acos(dir.y/sqrt(pow(dir.x,2) + pow(dir.y,2) + pow(dir.z,2)));
                altitude = (altitude/PI);
                altitude = -(altitude - 0.5) + 0.5; // Flip around 0.5
                
                // Intensity
                float rawIntensity = tex2D(_Heatmap, float2(azimuth, altitude)).r;
                float intensity = clamp((rawIntensity - _Min)/(_Max - _Min), 0, 1);
                intensity = pow(intensity, 2);
                intensity = smoothstep(0.99, 1, intensity);

                // Color
                intensity = clamp(intensity, 0, 0.99);
                float4 col = tex2D(_Colormap, float2(intensity, 0.5));
                clip(isDithered(i.screenPos.xy/i.screenPos.w, intensity));
                /* clip(intensity - 0.1); */
                /* return lerp(float4(0,0,0,1), col, intensity); */
                return float4(col.rgb, smoothstep(0.1, 1, intensity));
            }
            ENDCG
        }
    }
}
