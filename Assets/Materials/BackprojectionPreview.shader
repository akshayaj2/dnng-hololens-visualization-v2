Shader "Unlit/BackprojectionPreview"
{
    Properties
    {
        _Mask ("Mask", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            sampler2D _Heatmap;
            sampler2D _Mask;
            sampler2D _Colormap;

            float _Max;
            float _Min;

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
                float rawIntensity = tex2D(_Heatmap, i.uv).r;
                float intensity = clamp((rawIntensity - _Min)/(_Max - _Min), 0, 1);
                intensity = pow(intensity, 2);

                intensity = clamp(intensity, 0.05, 0.99);
                float4 col = tex2D(_Colormap, float2(intensity, 0.5));
                clip(tex2D(_Mask, i.uv).a - 0.5);
                return col;
            }
            ENDCG
        }
    }
}
