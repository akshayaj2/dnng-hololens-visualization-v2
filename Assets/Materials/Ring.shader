Shader "Unlit/Ring"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        /* Tags { "Queue"="Transparent" "RenderType"="Transparent" } */
        LOD 100
        Cull Off

        /* ZWrite Off */
        /* Blend SrcAlpha OneMinusSrcAlpha */

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 worldPos : TEXCOORD1;
                float4 screenPos : TEXCOORD2;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            #define PI 3.14159

            sampler2D _Direction;
            sampler2D _Colormap;

            float4x4 _SensorRootMatrix;

            float _Max;
            float _Min;

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.screenPos = ComputeScreenPos(o.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
                // Heatmap
                float4 pos = mul(_SensorRootMatrix, i.worldPos);
                float3 dir = normalize(pos.xyz);
                float azimuth = atan2(dir.x, dir.z);
                azimuth += 2 * PI;
                azimuth = (azimuth % (2 * PI))/(2 * PI);

                float intensity =  tex2D(_Direction, float2(azimuth * 360 / 512, 0.5)).r;

                // Color
                intensity = clamp(intensity, 0, 0.99);
                float4 col = tex2D(_Colormap, float2(intensity, 0.5));
                if(intensity == 0){
                    return (0,0,0, 0.3);
                }
                //clip(intensity - 0.0001);
                /* return lerp(float4(0,0,0,1), col, intensity); */
                return float4(col.rgb, 1);
            }
            ENDCG
        }
    }
}
