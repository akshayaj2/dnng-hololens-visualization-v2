﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationSystem_Threading;
extern const CustomAttributesCacheGenerator g_System_Threading_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Threading_CodeGenModule;
const Il2CppCodeGenModule g_System_Threading_CodeGenModule = 
{
	"System.Threading.dll",
	0,
	NULL,
	0,
	NULL,
	NULL,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	&g_DebuggerMetadataRegistrationSystem_Threading,
	g_System_Threading_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
