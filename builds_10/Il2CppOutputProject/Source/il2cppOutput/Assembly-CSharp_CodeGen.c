﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ArrowPointer::Update()
extern void ArrowPointer_Update_m4B8433A000A4E0ABA28A54CB75C9A512843FFA40 (void);
// 0x00000002 System.Void ArrowPointer::Start()
extern void ArrowPointer_Start_m5882041A3FA8C6776DC3CDDDF755A3D6397D7A4F (void);
// 0x00000003 System.Void ArrowPointer::.ctor()
extern void ArrowPointer__ctor_m02339FFF34A26D1AE0B49A41305E0179B0F8FF7B (void);
// 0x00000004 System.Void ChangeColorMap::Start()
extern void ChangeColorMap_Start_m39E825249DCC7C991A599F9C0E9323F538D240ED (void);
// 0x00000005 System.Void ChangeColorMap::Cycle()
extern void ChangeColorMap_Cycle_m790032E00E68073D242B8CA3C19AFEAD1B9F7781 (void);
// 0x00000006 System.Void ChangeColorMap::ChangeTo(System.Int32)
extern void ChangeColorMap_ChangeTo_mB2DEDE4F252DAC2E2867012210C388E9B5E69E30 (void);
// 0x00000007 System.Void ChangeColorMap::Apply()
extern void ChangeColorMap_Apply_m4E02E07249581BB35949B62B0B13522E3AB7D5B9 (void);
// 0x00000008 System.Void ChangeColorMap::.ctor()
extern void ChangeColorMap__ctor_mA869C9D2E8C9A7A36C1DA3DF05404A56AFFEA15A (void);
// 0x00000009 System.Void ConeFileLoader::Start()
extern void ConeFileLoader_Start_m18B0956C90D2CEE2F2AF48937680653500246063 (void);
// 0x0000000A System.Void ConeFileLoader::LoadNextFile()
extern void ConeFileLoader_LoadNextFile_m54154A91D7042E3900DA7F2DC29D56757B74F9B7 (void);
// 0x0000000B System.Void ConeFileLoader::.ctor()
extern void ConeFileLoader__ctor_m725654547797AA337192F179865F836F3D7310C9 (void);
// 0x0000000C Cone ConeJson::ToCone()
extern void ConeJson_ToCone_m43601328C46FF8C384C46B25A59D24AD39FE1B47 (void);
// 0x0000000D System.Void ConeJson::.ctor()
extern void ConeJson__ctor_m0526E2742ABC2D3B024750EC1735ECF7C4B1AB04 (void);
// 0x0000000E System.Void ConeCollection::.ctor()
extern void ConeCollection__ctor_mFC1327722E51DCE46F77F305E2E7C259D3825B80 (void);
// 0x0000000F System.Void DrawCones::Start()
extern void DrawCones_Start_m7E2DDF61306ABF946957F69C633A5F87C244CDFC (void);
// 0x00000010 System.Void DrawCones::ToggleGamma()
extern void DrawCones_ToggleGamma_m42B01E3218515D8D1E7B3B9C6EE42563726FAFB2 (void);
// 0x00000011 System.Void DrawCones::ToggleAccumulation()
extern void DrawCones_ToggleAccumulation_m956679D3694927793504AF1C211F3E3963A21466 (void);
// 0x00000012 System.Void DrawCones::ClearAccumulators()
extern void DrawCones_ClearAccumulators_mCF56F05F15BD727F0469D4E38492A3D528528527 (void);
// 0x00000013 System.Void DrawCones::ClearCones()
extern void DrawCones_ClearCones_mFEA126478048115AF6657398AD46C249FB19D35F (void);
// 0x00000014 System.Void DrawCones::LoadJson(System.String)
extern void DrawCones_LoadJson_m9F87AEB7C8AC31F7BE9F01FD7AC15B78E429B0C8 (void);
// 0x00000015 System.Void DrawCones::Compute(UnityEngine.RenderTexture,UnityEngine.ComputeBuffer,System.Boolean)
extern void DrawCones_Compute_mC0087695FF1A1E972BE51FBACBA00FDBA7889FB7 (void);
// 0x00000016 System.Void DrawCones::AssignTextures(UnityEngine.RenderTexture)
extern void DrawCones_AssignTextures_m43266D753B24D4031116D0370E6692B17E4A7986 (void);
// 0x00000017 System.Void DrawCones::.ctor()
extern void DrawCones__ctor_m63796DB983F3525DC9B12E52BC6C4EDBE3DA4782 (void);
// 0x00000018 System.Void DrawCones/<>c::.cctor()
extern void U3CU3Ec__cctor_mB9C27FC747DB577EFAFDE939695F5AA0231B6175 (void);
// 0x00000019 System.Void DrawCones/<>c::.ctor()
extern void U3CU3Ec__ctor_mF74A9B1E2AD67103CC976428AF26941565A29409 (void);
// 0x0000001A Cone DrawCones/<>c::<LoadJson>b__14_0(ConeJson)
extern void U3CU3Ec_U3CLoadJsonU3Eb__14_0_m5BA78C0FB420767163F658561A1200FE091BED98 (void);
// 0x0000001B System.Void DrawCones/<>c::<Compute>b__15_0(UnityEngine.Rendering.AsyncGPUReadbackRequest)
extern void U3CU3Ec_U3CComputeU3Eb__15_0_m7270695B150B56C6A97E15B06457FED656F83616 (void);
// 0x0000001C System.Void DrawHistogram::Start()
extern void DrawHistogram_Start_m199E71FBB634D5E00A6FDE32B6B6C5EA9D0216C1 (void);
// 0x0000001D System.Void DrawHistogram::UpdateGraph()
extern void DrawHistogram_UpdateGraph_m860F1FDB8EC71E27CD9964A9F07A0C7E47A5E6B1 (void);
// 0x0000001E System.Collections.Generic.List`1<System.Int32> DrawHistogram::ProcessBins(System.Collections.Generic.List`1<System.Single>,System.Int32,System.Single)
extern void DrawHistogram_ProcessBins_mB9C112CDA8FA670480B7AA768F719031AA922243 (void);
// 0x0000001F System.Void DrawHistogram::Draw(System.Collections.Generic.List`1<System.Int32>,System.Single)
extern void DrawHistogram_Draw_m042542BB935E322B0B384AA5954AD31B1CB1D4BC (void);
// 0x00000020 System.Void DrawHistogram::Clear()
extern void DrawHistogram_Clear_mFA7191197D7D18C985E9607F1F2F0E4102CB1D1D (void);
// 0x00000021 System.Void DrawHistogram::DrawHorizontalRect(System.Int32,System.Int32,System.Int32,UnityEngine.Color,System.Int32)
extern void DrawHistogram_DrawHorizontalRect_m0E38128C1ACC14065CCC93C8F5B39C495E403158 (void);
// 0x00000022 System.Void DrawHistogram::DrawVerticalRect(System.Int32,System.Int32,System.Int32,UnityEngine.Color,System.Int32)
extern void DrawHistogram_DrawVerticalRect_mFB834A99462525D35059B3ED5AC5A77F7EB6992D (void);
// 0x00000023 System.Void DrawHistogram::.ctor()
extern void DrawHistogram__ctor_mC9A2F5C4395B41487ABB7D69E36699EE3611D972 (void);
// 0x00000024 System.Void DrawLines::LoadData(Unity.Collections.NativeArray`1<UnityEngine.Color>,System.Single)
extern void DrawLines_LoadData_m7C94C2C6AA6AD5C06F0B306131089E1E78085FDB (void);
// 0x00000025 System.Void DrawLines::Update()
extern void DrawLines_Update_m7C525F387D0B5A7FFA1C865C038D87594F5EB2CC (void);
// 0x00000026 System.Void DrawLines::ToggleLines()
extern void DrawLines_ToggleLines_m6D3F5C8F60448E4ADD6614202737B62A94906571 (void);
// 0x00000027 System.Void DrawLines::.ctor()
extern void DrawLines__ctor_m6532DC16647FFD580BF696B16180F7899A9AF2D7 (void);
// 0x00000028 System.Void DrawSpheres::Start()
extern void DrawSpheres_Start_mE473CA720DFE804DF1F26B3D6B20D38CEDC55EA6 (void);
// 0x00000029 System.Void DrawSpheres::OnValidate()
extern void DrawSpheres_OnValidate_m59DEC1DEE4C4B4CC2224B6497570FFBA5BF728F7 (void);
// 0x0000002A System.Void DrawSpheres::RecalculateMatrices()
extern void DrawSpheres_RecalculateMatrices_mDA28C195035871317191E3DA207588C1091B33CF (void);
// 0x0000002B System.Void DrawSpheres::Update()
extern void DrawSpheres_Update_m1B4F6CD5C753926E808D6EA50660EED95E401A30 (void);
// 0x0000002C System.Void DrawSpheres::ToggleSpheres()
extern void DrawSpheres_ToggleSpheres_mE93018EB8F1E185ADE8F3D6A6CDBE23FAE57513C (void);
// 0x0000002D System.Void DrawSpheres::.ctor()
extern void DrawSpheres__ctor_m9AE33692B33415391686C57291F43C661AA99C10 (void);
// 0x0000002E System.Single DirectionResults::maxAngle()
extern void DirectionResults_maxAngle_m11347E4DC3E20A66A708CFFA38C08AF7ED41CEB4 (void);
// 0x0000002F System.Void DirectionResults::.ctor()
extern void DirectionResults__ctor_m9FDCBB03CE2DCD70117ABAA66BECCD4AC26C51FB (void);
// 0x00000030 System.Threading.Tasks.Task FrontendWeb::HandleConnections()
extern void FrontendWeb_HandleConnections_m4DC2CF09FD8422EE798D227C6E5812C45642BF5C (void);
// 0x00000031 System.Void FrontendWeb::Start()
extern void FrontendWeb_Start_m49E28273474AA7230BC946BE838A2503C23F7B58 (void);
// 0x00000032 System.Void FrontendWeb::OnApplicationQuit()
extern void FrontendWeb_OnApplicationQuit_m53A4FFC3E51D1EE006E7A994DCA045F7644E958A (void);
// 0x00000033 System.Void FrontendWeb::.ctor()
extern void FrontendWeb__ctor_mC36B79CB4C11986E424EB40149F6C9249AD0283E (void);
// 0x00000034 System.Void FrontendWeb/<HandleConnections>d__5::.ctor()
extern void U3CHandleConnectionsU3Ed__5__ctor_m44BC907BD508827496A7410DA9F253D512323DCC (void);
// 0x00000035 System.Void FrontendWeb/<HandleConnections>d__5::MoveNext()
extern void U3CHandleConnectionsU3Ed__5_MoveNext_m9608431C957EED5DDC8BA6C06DDCF8E71C1D8219 (void);
// 0x00000036 System.Void FrontendWeb/<HandleConnections>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHandleConnectionsU3Ed__5_SetStateMachine_m8C24FE0B1607E3B9F2D1A2B01B7C09E7CB04A123 (void);
// 0x00000037 System.Void FrontendWeb/<Start>d__6::.ctor()
extern void U3CStartU3Ed__6__ctor_mCBF26EF7D8389D93DAC6FDDF409E79BC82D29365 (void);
// 0x00000038 System.Void FrontendWeb/<Start>d__6::MoveNext()
extern void U3CStartU3Ed__6_MoveNext_m0E4154AA96FD2EC4C5DC52D0BC0B4072F64A6096 (void);
// 0x00000039 System.Void FrontendWeb/<Start>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__6_SetStateMachine_m1F901FA3E96E86E54D0D772331AD5ADF3D576E34 (void);
// 0x0000003A System.Void HandMenuController::OpenAdvanced()
extern void HandMenuController_OpenAdvanced_m8F26252F435723174F81ABCDF4C17C2A4E7919C6 (void);
// 0x0000003B System.Void HandMenuController::OpenChart()
extern void HandMenuController_OpenChart_m6FB691AFCBA0933F74B42F074EB1FBDB78A5B4CB (void);
// 0x0000003C System.Void HandMenuController::ToggleFastMode()
extern void HandMenuController_ToggleFastMode_m317DA4EEF563CA292ECCCE71D4721EA244C0229C (void);
// 0x0000003D System.Void HandMenuController::.ctor()
extern void HandMenuController__ctor_m86A93C73A4D82B8EBE2E2480847B547F8D6CEB5A (void);
// 0x0000003E System.Void PreviewSwivel::Update()
extern void PreviewSwivel_Update_m1863468D99585CCC549D2FB8FBE4993287CB630D (void);
// 0x0000003F System.Void PreviewSwivel::.ctor()
extern void PreviewSwivel__ctor_m316F2133DFA833FF88DD59F7BEE3CCC0EE79703B (void);
// 0x00000040 System.Void QRTracker::Start()
extern void QRTracker_Start_m8A257AAB41D8433AB47C35DD0936E35C7AA65A50 (void);
// 0x00000041 System.Void QRTracker::Watcher_Added(System.Object,Microsoft.MixedReality.QR.QRCodeAddedEventArgs)
extern void QRTracker_Watcher_Added_m3AACAB649299CE8EB502F834315508BA697E0F40 (void);
// 0x00000042 System.Void QRTracker::Watcher_Updated(System.Object,Microsoft.MixedReality.QR.QRCodeUpdatedEventArgs)
extern void QRTracker_Watcher_Updated_m03FE71CCFC6C482EE7471B32133E24070BDE36A9 (void);
// 0x00000043 System.Void QRTracker::Reposition(UnityEngine.Pose,System.Single)
extern void QRTracker_Reposition_m878184EEDBFAA1DF2D38C7A0FE8B9D5BDCC5B7A8 (void);
// 0x00000044 System.Void QRTracker::.ctor()
extern void QRTracker__ctor_mEE1D8B1127532173E29174906AE99BB89D9B72F1 (void);
// 0x00000045 System.Void QRTracker/<Start>d__5::.ctor()
extern void U3CStartU3Ed__5__ctor_m5EF8088ADA8B4FBD1AEDA597CD32BC8F2A7FF641 (void);
// 0x00000046 System.Void QRTracker/<Start>d__5::MoveNext()
extern void U3CStartU3Ed__5_MoveNext_m77D26AB15CD79194137A6336DBF8DDF143A4D37D (void);
// 0x00000047 System.Void QRTracker/<Start>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__5_SetStateMachine_m21285B810AFED0C066C47A1BBF0C925E81077951 (void);
// 0x00000048 System.Void SetMatrix::Start()
extern void SetMatrix_Start_mCC3D2E4D8B42CD98DF35F3A87C1D97FF9244566E (void);
// 0x00000049 System.Void SetMatrix::Update()
extern void SetMatrix_Update_m3399F29E6648ED3FEE0B1594345E7C121B2F8CCD (void);
// 0x0000004A System.Void SetMatrix::.ctor()
extern void SetMatrix__ctor_m325E3E14109B0DE71B5B79D679FA7323E406A8A3 (void);
// 0x0000004B System.Void ToggleGameobject::Toggle()
extern void ToggleGameobject_Toggle_m588DF74600CC205B181E3992E3E31A85FE28A455 (void);
// 0x0000004C System.Void ToggleGameobject::.ctor()
extern void ToggleGameobject__ctor_mAC153B1F1B9F340DC54D3211F82A367DACBEED31 (void);
// 0x0000004D System.Void ToggleMesh::Toggle()
extern void ToggleMesh_Toggle_mC311D8C796A058519F404455BCD68CF9BAC97764 (void);
// 0x0000004E System.Void ToggleMesh::.ctor()
extern void ToggleMesh__ctor_mD6E591FBEC631CA6F898C9E9D8DE6680DD5721FD (void);
static Il2CppMethodPointer s_methodPointers[78] = 
{
	ArrowPointer_Update_m4B8433A000A4E0ABA28A54CB75C9A512843FFA40,
	ArrowPointer_Start_m5882041A3FA8C6776DC3CDDDF755A3D6397D7A4F,
	ArrowPointer__ctor_m02339FFF34A26D1AE0B49A41305E0179B0F8FF7B,
	ChangeColorMap_Start_m39E825249DCC7C991A599F9C0E9323F538D240ED,
	ChangeColorMap_Cycle_m790032E00E68073D242B8CA3C19AFEAD1B9F7781,
	ChangeColorMap_ChangeTo_mB2DEDE4F252DAC2E2867012210C388E9B5E69E30,
	ChangeColorMap_Apply_m4E02E07249581BB35949B62B0B13522E3AB7D5B9,
	ChangeColorMap__ctor_mA869C9D2E8C9A7A36C1DA3DF05404A56AFFEA15A,
	ConeFileLoader_Start_m18B0956C90D2CEE2F2AF48937680653500246063,
	ConeFileLoader_LoadNextFile_m54154A91D7042E3900DA7F2DC29D56757B74F9B7,
	ConeFileLoader__ctor_m725654547797AA337192F179865F836F3D7310C9,
	ConeJson_ToCone_m43601328C46FF8C384C46B25A59D24AD39FE1B47,
	ConeJson__ctor_m0526E2742ABC2D3B024750EC1735ECF7C4B1AB04,
	ConeCollection__ctor_mFC1327722E51DCE46F77F305E2E7C259D3825B80,
	DrawCones_Start_m7E2DDF61306ABF946957F69C633A5F87C244CDFC,
	DrawCones_ToggleGamma_m42B01E3218515D8D1E7B3B9C6EE42563726FAFB2,
	DrawCones_ToggleAccumulation_m956679D3694927793504AF1C211F3E3963A21466,
	DrawCones_ClearAccumulators_mCF56F05F15BD727F0469D4E38492A3D528528527,
	DrawCones_ClearCones_mFEA126478048115AF6657398AD46C249FB19D35F,
	DrawCones_LoadJson_m9F87AEB7C8AC31F7BE9F01FD7AC15B78E429B0C8,
	DrawCones_Compute_mC0087695FF1A1E972BE51FBACBA00FDBA7889FB7,
	DrawCones_AssignTextures_m43266D753B24D4031116D0370E6692B17E4A7986,
	DrawCones__ctor_m63796DB983F3525DC9B12E52BC6C4EDBE3DA4782,
	U3CU3Ec__cctor_mB9C27FC747DB577EFAFDE939695F5AA0231B6175,
	U3CU3Ec__ctor_mF74A9B1E2AD67103CC976428AF26941565A29409,
	U3CU3Ec_U3CLoadJsonU3Eb__14_0_m5BA78C0FB420767163F658561A1200FE091BED98,
	U3CU3Ec_U3CComputeU3Eb__15_0_m7270695B150B56C6A97E15B06457FED656F83616,
	DrawHistogram_Start_m199E71FBB634D5E00A6FDE32B6B6C5EA9D0216C1,
	DrawHistogram_UpdateGraph_m860F1FDB8EC71E27CD9964A9F07A0C7E47A5E6B1,
	DrawHistogram_ProcessBins_mB9C112CDA8FA670480B7AA768F719031AA922243,
	DrawHistogram_Draw_m042542BB935E322B0B384AA5954AD31B1CB1D4BC,
	DrawHistogram_Clear_mFA7191197D7D18C985E9607F1F2F0E4102CB1D1D,
	DrawHistogram_DrawHorizontalRect_m0E38128C1ACC14065CCC93C8F5B39C495E403158,
	DrawHistogram_DrawVerticalRect_mFB834A99462525D35059B3ED5AC5A77F7EB6992D,
	DrawHistogram__ctor_mC9A2F5C4395B41487ABB7D69E36699EE3611D972,
	DrawLines_LoadData_m7C94C2C6AA6AD5C06F0B306131089E1E78085FDB,
	DrawLines_Update_m7C525F387D0B5A7FFA1C865C038D87594F5EB2CC,
	DrawLines_ToggleLines_m6D3F5C8F60448E4ADD6614202737B62A94906571,
	DrawLines__ctor_m6532DC16647FFD580BF696B16180F7899A9AF2D7,
	DrawSpheres_Start_mE473CA720DFE804DF1F26B3D6B20D38CEDC55EA6,
	DrawSpheres_OnValidate_m59DEC1DEE4C4B4CC2224B6497570FFBA5BF728F7,
	DrawSpheres_RecalculateMatrices_mDA28C195035871317191E3DA207588C1091B33CF,
	DrawSpheres_Update_m1B4F6CD5C753926E808D6EA50660EED95E401A30,
	DrawSpheres_ToggleSpheres_mE93018EB8F1E185ADE8F3D6A6CDBE23FAE57513C,
	DrawSpheres__ctor_m9AE33692B33415391686C57291F43C661AA99C10,
	DirectionResults_maxAngle_m11347E4DC3E20A66A708CFFA38C08AF7ED41CEB4,
	DirectionResults__ctor_m9FDCBB03CE2DCD70117ABAA66BECCD4AC26C51FB,
	FrontendWeb_HandleConnections_m4DC2CF09FD8422EE798D227C6E5812C45642BF5C,
	FrontendWeb_Start_m49E28273474AA7230BC946BE838A2503C23F7B58,
	FrontendWeb_OnApplicationQuit_m53A4FFC3E51D1EE006E7A994DCA045F7644E958A,
	FrontendWeb__ctor_mC36B79CB4C11986E424EB40149F6C9249AD0283E,
	U3CHandleConnectionsU3Ed__5__ctor_m44BC907BD508827496A7410DA9F253D512323DCC,
	U3CHandleConnectionsU3Ed__5_MoveNext_m9608431C957EED5DDC8BA6C06DDCF8E71C1D8219,
	U3CHandleConnectionsU3Ed__5_SetStateMachine_m8C24FE0B1607E3B9F2D1A2B01B7C09E7CB04A123,
	U3CStartU3Ed__6__ctor_mCBF26EF7D8389D93DAC6FDDF409E79BC82D29365,
	U3CStartU3Ed__6_MoveNext_m0E4154AA96FD2EC4C5DC52D0BC0B4072F64A6096,
	U3CStartU3Ed__6_SetStateMachine_m1F901FA3E96E86E54D0D772331AD5ADF3D576E34,
	HandMenuController_OpenAdvanced_m8F26252F435723174F81ABCDF4C17C2A4E7919C6,
	HandMenuController_OpenChart_m6FB691AFCBA0933F74B42F074EB1FBDB78A5B4CB,
	HandMenuController_ToggleFastMode_m317DA4EEF563CA292ECCCE71D4721EA244C0229C,
	HandMenuController__ctor_m86A93C73A4D82B8EBE2E2480847B547F8D6CEB5A,
	PreviewSwivel_Update_m1863468D99585CCC549D2FB8FBE4993287CB630D,
	PreviewSwivel__ctor_m316F2133DFA833FF88DD59F7BEE3CCC0EE79703B,
	QRTracker_Start_m8A257AAB41D8433AB47C35DD0936E35C7AA65A50,
	QRTracker_Watcher_Added_m3AACAB649299CE8EB502F834315508BA697E0F40,
	QRTracker_Watcher_Updated_m03FE71CCFC6C482EE7471B32133E24070BDE36A9,
	QRTracker_Reposition_m878184EEDBFAA1DF2D38C7A0FE8B9D5BDCC5B7A8,
	QRTracker__ctor_mEE1D8B1127532173E29174906AE99BB89D9B72F1,
	U3CStartU3Ed__5__ctor_m5EF8088ADA8B4FBD1AEDA597CD32BC8F2A7FF641,
	U3CStartU3Ed__5_MoveNext_m77D26AB15CD79194137A6336DBF8DDF143A4D37D,
	U3CStartU3Ed__5_SetStateMachine_m21285B810AFED0C066C47A1BBF0C925E81077951,
	SetMatrix_Start_mCC3D2E4D8B42CD98DF35F3A87C1D97FF9244566E,
	SetMatrix_Update_m3399F29E6648ED3FEE0B1594345E7C121B2F8CCD,
	SetMatrix__ctor_m325E3E14109B0DE71B5B79D679FA7323E406A8A3,
	ToggleGameobject_Toggle_m588DF74600CC205B181E3992E3E31A85FE28A455,
	ToggleGameobject__ctor_mAC153B1F1B9F340DC54D3211F82A367DACBEED31,
	ToggleMesh_Toggle_mC311D8C796A058519F404455BCD68CF9BAC97764,
	ToggleMesh__ctor_mD6E591FBEC631CA6F898C9E9D8DE6680DD5721FD,
};
static const int32_t s_InvokerIndices[78] = 
{
	5264,
	5264,
	5264,
	5264,
	5264,
	4260,
	5264,
	5264,
	5264,
	5264,
	5264,
	5070,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	4296,
	1364,
	4296,
	5264,
	7770,
	5264,
	3224,
	4178,
	5264,
	5264,
	1165,
	2427,
	5264,
	300,
	300,
	5264,
	2008,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	5218,
	5264,
	5174,
	5264,
	5264,
	5264,
	5264,
	5264,
	4296,
	5264,
	5264,
	4296,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	2423,
	2423,
	2445,
	5264,
	5264,
	5264,
	4296,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
	5264,
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationAssemblyU2DCSharp;
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	78,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	&g_DebuggerMetadataRegistrationAssemblyU2DCSharp,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
