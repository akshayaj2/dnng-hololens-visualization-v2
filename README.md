# dnng-hololens-visualization-v2

## Project Goal

Upgrade existing code to receive xyz coordinate output from neural network that estimates radiation source, and display as an arrow on the HoloLens pointing towards the predicted source location while including both the altitude and azimuth angles.

## File Organization

sendcones.py runs neural network on RL_52 data and sends its output through HTTP request to the HoloLens. FrontendWeb.cs script (Assets->Scripts) receives this data and updates arrow based on ML output. ArrowPointer.cs was just used to test initial implementations locally in Unity.

## HoloLens Build Instructions

Refer to README in: https://gitlab.eecs.umich.edu/umich-dnng/dnng-hololens-visualization


